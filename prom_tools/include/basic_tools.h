/*
Copyright  ETIS ��� ENSEA, Universit�� de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouz��ne,
M. Lagarde, S. Lepr��tre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengerv��, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * prom_tools.h
 *
 *  Created on: Apr 28, 2011
 *      Author: Arnaud Blanchard
 */

#ifndef BASIC_TOOLS_H
#define BASIC_TOOLS_H
#include <stdlib.h> /* size_t */
#include <stdarg.h>
#include <math.h>
#include <locale.h>
#include <blc_tools.h>
#include "pthread.h"

#define  MOD(a, b) ((((a)%(b))+(abs(b)))%(b))
#define PARAM_MAX           256

/**************************************************************/
/* transforme un entier en chaine de caracteres */
/* attention si l'on ne recopie pas le resultat renvoye par un 1er
 appel avant le 2eme appel il sera remplace par le resultat du 2eme appel!
 Une seule zone de memoire allouee... */
extern char *Int2Str(int entier);
extern char *Float2Str(float x);

/** defined in iostrem_over_network.c */
extern void vkprints(const char *fmt, va_list ap);
extern void kprints(const char *fmt, ...);
extern void kscans(const char *fmt, ...);
extern void cscans(const char *fmt, ...);
extern void cprints(const char *fmt, ...);
extern double frand_a_b(double a, double b);

typedef struct basic_buffer
{
  char *data;
  int size, position;
  pthread_mutex_t mutex; /* ATTENTION you have to take care of lock or unlock the mutex yourself */
}type_buffer;

#define BUFFER_INIT(buffer) { memset(buffer, 0, sizeof(type_buffer)); pthread_mutex_init(&(buffer)->mutex, 0);}
#define BUFFER_LOCK(buffer) pthread_mutex_lock(&(buffer)->mutex);
#define BUFFER_UNLOCK(buffer) pthread_mutex_unlock(&(buffer)->mutex);

#define EXIT_ON_GROUP_ERROR(group_id, ...) group_fatal_error(group_id, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

void buffer_append(type_buffer *buffer, const char *data, int size);
void buffer_replace(type_buffer *buffer, const char *data, int size);
void buffer_allocate_min(type_buffer *buffer, int minimum_size);
void group_fatal_error(int group_id, const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);
extern int prom_getopt(const char *args, const char *opt, char *retour);
extern int prom_getopt_int(const char *args, const char *opt, int *value);
extern int prom_getopt_float(const char *args, const char *opt, float *value);
#endif /* PROM_TOOLS_H */

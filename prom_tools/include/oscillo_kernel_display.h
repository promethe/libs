/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef OSCILLO_KERNEL_DISPLAY_H
#define OSCILLO_KERNEL_DISPLAY_H

#include <sys/time.h>
#include <unistd.h>
#include <graphic_Tx.h>
#include <libx.h>
#include <xml_tools.h>

#define SIZE_OF_HISTORIC_OF_GROUP_PROFILE 256
#define SIZE_OF_DISPLAY_INFO 256

struct group_profiler;

extern GtkWidget *window;


typedef struct profiler
{
  char logical_name[SIZE_NO_NAME];
  int number_of_groups;
  type_com_groupe *groups;
  struct group_profiler *profiler_groups[nb_max_groupes];
  glong period, time_offset, reset_time, time_gain;
  void *data;
  GtkWidget *widget;
  GtkWidget *button_box;

}type_profiler;

typedef struct event_of_group_profiler
{
  int phase;
  long time_stamp;
}Event_of_group_profiler;

typedef struct group_profiler
{
  char name[SIZE_NO_NAME];
  char text_of_period[SIZE_OF_DISPLAY_INFO];

  type_profiler *profiler;
  cairo_t *cr;

  float size_of_sample, period;

  Event_of_group_profiler historic[SIZE_OF_HISTORIC_OF_GROUP_PROFILE];
  int number_of_historical_events;
  int type;

  glong time_of_last_received_token;
  TxDonneesFenetre *fenetre_group;

  GtkWidget *widget;
  GtkWidget *check_button;
  GtkWidget *drawing_area;
  GtkWidget *graph_widget;
  GdkPixmap *graph_pixmap;
  GtkWidget *widget_of_period;

  long last_reset_oscillo_kernel;
}type_group_profiler;

void group_profiler_update_info(type_profiler *profiler, int gpe, int phase, glong temps);
void display_oscillo_kernel(type_group_profiler *group_profiler,  int gpe, int phase, unsigned long temps);

extern TxDonneesFenetre fenetre_oscillo_kernel;
extern int oscillo_kernel_activated;
extern int recent_reset_affichage;
extern unsigned long periode_reset_oscillo_kernel;
/* extern long last_pos_oscillo_kernel[];*/
extern glong temps_old;
extern int hauteur_display_gpe;
extern struct timeval InitTimeTrace;


void reset_affichage_oscillo_kernel();
void create_fenetre_oscillo_kernel();
void oscillo_kernel_create_window();
void affiche_oscillo_distant();

void show_oscillo_kernel(void *data);

void arrete_oscillo_kernel(GtkWidget *widget);

void oscillo_dist_status();

void init_oscillo_window();

void oscillo_kernel_stop(void *profiler);

void create_window_of_profiler(const char *title);

void create_fenetre_oscillo_kernel();

type_profiler *oscillo_kernel_add_promethe(type_com_groupe *d_groupe, int number_of_groups, const char *name_promethe, char *file_preferences);
void on_check_button_promethe_activate(GtkWidget *check_button, type_profiler *profiler);

void change_periode_oscillo_kernel(GtkWidget * widget, GtkWidget * entry);
void periode_oscillo_kernel_get_value(GtkWidget * widget, int *val);
void oscillo_kernel_pressed();

Node* oscillo_kernel_get_xml_informations(Node *tree);


#endif

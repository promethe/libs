/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**

 \defgroup libpromnet libpromnet.h
 \ingroup generic_tools

 \brief structures for communication between different promethe

 \details
 Description of the structures that are used in the inter-communication between the different scripts.

 \file
 \ingroup libpromnet
 \brief structures for communication between different promethe

 */

#ifndef	_LIBPROMNET_H_
#define	_LIBPROMNET_H_

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "xml_tools.h"
#include "libparserxml_MetaLeto.h"
#include "script_parserxml.h"
#include "read_prt.h"
#include "net_message_debug_dist.h"

#define PROMNET_VERSION 2

#define	COMPUTER_MAX	256
#define	ADDRESS_MAX	256
#define	CPU_MAX		256
#define	INFORMATION_MAX	256
#define	PROM_ID_MAX	64
#define	LOGIN_MAX	128

#define	MAX_ALL_PATH		208 /* A remplacer par PATH_MAX la varaible officielle de Linux  */

#define	EXT_SCRIPT_SCRIPT	".script"
#define	EXT_SCRIPT_NON_SYMB	".script_o"
#define	EXT_SCRIPT_SYMB		".symb"
#define	EXT_SCRIPT_DRAW		".draw"
#define	EXT_SCRIPT_RES		".res"
#define	EXT_SCRIPT_CONFIG	".config"
#define	EXT_SCRIPT_BUS		".bus"
#define	EXT_SCRIPT_VAR		".var"
#define	EXT_SCRIPT_DEV		".dev"
#define	EXT_SCRIPT_GCD		".gcd"
#define	EXT_SCRIPT_PRT		".prt"

#define	PROM_ARGS_LINE_MAX	2048
#define KEYBOARD_INPUT_MAX 256
#define SYNCHRONIZE_PATHS_MAX 4096

#define	LINK_NAME_MAX	256

#define	NET_PATH_MAX	256
#define	CPT_PATH_MAX	256
#define	IVYBUS_ENVVAR_MAX 256

extern const int promnet_version;

/**
 Structure of computer
 */
typedef struct s_computer {
  char name[COMPUTER_MAX];
  char address[ADDRESS_MAX];
  char login[LOGIN_MAX];
  char cpu[CPU_MAX];
  char path[PATH_MAX];
  char information[INFORMATION_MAX];

  computer *data;

  struct s_computer *prev;
  struct s_computer *next;
} t_computer;

/**
 Structure of Promethe Script
 */
typedef struct s_prom_script {
  char logical_name[LOGICAL_NAME_MAX];

  char path_prom_binary[PATH_MAX];
  char path_prom_deploy[PATH_MAX];

  char path_file_script[PATH_MAX];
  char path_file_symb[PATH_MAX];
  char path_file_draw[PATH_MAX];
  char path_file_res[PATH_MAX];
  char path_file_config[PATH_MAX];
  char path_file_bus[PATH_MAX];
  char path_file_var[PATH_MAX];
  char path_file_dev[PATH_MAX];
  char path_file_prt[PATH_MAX];
  char path_file_gcd[PATH_MAX];

  char prom_args_line[PROM_ARGS_LINE_MAX];
  char bprom_args_line[PROM_ARGS_LINE_MAX];

  char computer[COMPUTER_MAX];
  char login[LOGIN_MAX];
  char distant_directory[PATH_MAX];
  char keyboard_input[KEYBOARD_INPUT_MAX];
  char synchronize_files[SYNCHRONIZE_PATHS_MAX];
  char synchronize_directories[SYNCHRONIZE_PATHS_MAX];

  int is_local, overwrite_res;
  int state, debug_port, kernel_port, console_port;
  char path_makefile[PATH_MAX];

  node *data; /* AB:a supprimer a terme */
  prt_t *prt;

  struct s_prom_script *prev, *next;

  int has_global_section;

} t_prom_script;

/**
 Structure of Link
 */
typedef struct s_prom_link {
  char name[LINK_NAME_MAX];
  t_prom_script *in;
  t_prom_script *out;

  link_metaleto *data;

  struct s_prom_link *prev;
  struct s_prom_link *next;
} t_prom_link;


/**
 Main sctructure
 */
typedef struct s_promnet {
  t_computer *computer;
  computer *tree_computer;

  t_prom_script *prom_script;
  node *tree_script;

  t_prom_link *prom_link;
  link_metaleto *tree_link;

  mxml_node_t *net_tree;
  mxml_node_t *cpt_tree;

  char net_path[NET_PATH_MAX];
  char cpt_path[CPT_PATH_MAX];
  char ivybus_envvar[IVYBUS_ENVVAR_MAX];

  global_sec * global;	

} t_promnet;

/**
 Prototypes
 */
t_promnet *promnet_init();

t_computer *promnet_add_new_computer(t_promnet *promnet);
t_computer *promnet_add_computer(t_promnet *promnet);
int promnet_computer_set_name(t_computer *computer, char *name);
int promnet_computer_set_address(t_computer *computer, char *address);
int promnet_computer_set_login(t_computer *computer, char *login);
int promnet_computer_set_cpu(t_computer *computer, char *cpu);
int promnet_computer_set_path(t_computer *computer, char *path);
int promnet_computer_set_information(t_computer *computer, char *information);
char *promnet_computer_get_name(t_computer *computer);
char *promnet_computer_get_address(t_computer *computer);
char *promnet_computer_get_login(t_computer *computer);
char *promnet_computer_get_cpu(t_computer *computer);
char *promnet_computer_get_path(t_computer *computer);
char *promnet_computer_get_information(t_computer *computer);
int promnet_del_computer(t_promnet *promnet, t_computer *computer);
void promnet_computer_copy(t_computer *src, t_computer *dst);
t_computer *promnet_computer_get_next(t_promnet *promnet, t_computer *computer);

t_prom_script *promnet_add_new_prom_script(t_promnet *promnet);
t_prom_script *promnet_add_prom_script(t_promnet *promnet);
int promnet_prom_script_set_logical_name(t_prom_script *prom_script, char *logical_name);
int promnet_prom_script_set_path_prom_binary(t_prom_script *prom_script, char *path_prom_binary);
int promnet_prom_script_set_path_prom_deploy(t_prom_script *prom_script, char *path_prom_binary);
void promnet_prom_script_set_all_path(t_prom_script *prom_script, char *path, char *logical_name);
int promnet_prom_script_set_path_file_script(t_prom_script *prom_script, char *path_file_script);
int promnet_prom_script_set_path_file_symb(t_prom_script *prom_script, char *path_file_symb);
int promnet_prom_script_set_path_file_draw(t_prom_script *prom_script, char *path_file_draw);
int promnet_prom_script_set_path_file_res(t_prom_script *prom_script, char *path_file_res);
int promnet_prom_script_set_path_file_config(t_prom_script *prom_script, char *path_file_config);
int promnet_prom_script_set_path_file_bus(t_prom_script *prom_script, char *path_file_bus);
int promnet_prom_script_set_path_file_var(t_prom_script *prom_script, char *path_file_var);
int promnet_prom_script_set_path_file_dev(t_prom_script *prom_script, char *path_file_dev);
int promnet_prom_script_set_path_file_prt(t_prom_script *prom_script, char *path_file_prt);
int promnet_prom_script_set_path_file_gcd(t_prom_script *prom_script, char *path_file_gcd);
int promnet_prom_script_set_prom_args_line(t_prom_script *prom_script, char *prom_args_line);
int promnet_prom_script_set_computer(t_prom_script *prom_script, char *computer);
int promnet_prom_script_set_field(t_prom_script *prom_script, char *script_field, char **node_field, char *value, int size_of_field);

char *promnet_prom_script_get_argument(t_prom_script *prom_script, void *argument_pointer);

int promnet_del_prom_script(t_promnet *promnet, t_prom_script *prom_script);
void promnet_del_all_prom_script(t_promnet *promnet);
t_prom_script *promnet_prom_script_get_next(t_promnet *promnet, t_prom_script *prom_script);
t_prom_script *promnet_prom_script_find_by_name(t_prom_script *prom_script, char *logical_name);

t_prom_link *promnet_add_new_prom_link(t_promnet *promnet);
t_prom_link *promnet_add_prom_link(t_promnet *promnet);
int promnet_prom_link_set_name(t_prom_link *prom_link, char *name);
int promnet_prom_link_set_input(t_prom_link *prom_link, t_prom_script *input);
int promnet_prom_link_set_output(t_prom_link *prom_link, t_prom_script *output);
char *promnet_prom_link_get_name(t_prom_link *prom_link);
t_prom_script *promnet_prom_link_get_input(t_prom_link *prom_link);
t_prom_script *promnet_prom_link_get_output(t_prom_link *prom_link);
int promnet_del_prom_link(t_promnet *promnet, t_prom_link *prom_link);
t_prom_link *promnet_prom_link_get_next(t_promnet *promnet, t_prom_link *prom_link);

int promnet_load_prom_script(t_promnet *promnet);
int promnet_load_prom_link(t_promnet *promnet);
int promnet_load_file_net(t_promnet *promnet, char *path);

int promnet_load_computer(t_promnet *promnet);
int promnet_load_file_cpt(t_promnet *promnet, char *path);

/** Sauvegarde a utiliser pour remplacer save_file_net */
int promnet_save(t_promnet *promnet, char *filename);

int promnet_save_file_net(t_promnet *promnet, char *path, char * ivybus_envvar);
int promnet_save_file_cpt(t_promnet *promnet, char *path);

int promnet_set_net_path(t_promnet *promnet, char *path);
int promnet_set_cpt_path(t_promnet *promnet, char *path);
int promnet_set_ivybus_envvar(t_promnet *promnet, char *envvar);

#endif /* _LIBPROMNET_H_ */

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _OUTILS_SCRIPT
#define _OUTILS_SCRIPT

#include <net_message_debug_dist.h>
#include <search.h>
#include "reseau.h"

#define READ_LINE_INT(file, variable) if (fscanf(file, "%d\n", variable) != 1) PRINT_WARNING("Wrong format.")
#define READ_LINE_FLOAT(file, variable) if (fscanf(file, "%f\n", variable) != 1) PRINT_WARNING("Wrong format.")



extern int nbre_groupes_lus;
extern void save_comment(FILE * local_f1, type_noeud_comment * comment);
extern type_noeud_comment *read_line_with_comment(FILE * local_f1, type_noeud_comment * first_comment, char *ligne);

void write_one_link(FILE * local_f1, type_liaison * local_liaison, int comment);
void read_one_link(FILE * local_f1, type_liaison * local_liaison, char *macro_name, struct hsearch_data *hashtab);

extern void  write_one_group(FILE * local_f1, type_groupe * local_groupe, int comment, char* xyz );

extern int read_one_group_leto(FILE * local_f1, type_groupe * local_groupe, char *macro_name, struct hsearch_data *hashtab, char** retour_nom_xyz);
extern int read_one_group_promethe(FILE * local_f1, type_groupe local_def_groupe[], int nb_max);

extern const char *stop_if_not_int(const char *chaine);
extern const char *stop_if_not_float(const char *chaine);

extern void Str2Int(int *x, char *chaine);

extern void Str2Float(float *x, char *chaine);

extern char *recherche_champs(char *ligne, const char *chaine);

extern int verify_if_int(const char *chaine);
int find_no_associated_to_symbolic_name(char *name, struct hsearch_data *hashtab);
type_groupe* find_group_associated_to_symbolic_name(char *name, struct hsearch_data *hashtab);
int create_new_entry_in_hash_table(type_groupe *groupe, struct hsearch_data *hash_tab);

/*  traduit le nom symbolique d'un lien en numero pour les manipulations de leto        */
extern int link_name_2_no(type_liaison * liaison, char *macro_name, void **hashtab);

extern char *recherche_champs(char *ligne, const char *chaine);

/* fonctions des hash tables */
 void init_hash_table(struct hsearch_data *hashtab);
 void remove_hash_table(struct hsearch_data *hashtab);
 void *lookup_hash_table(struct hsearch_data *hashtab, char *name);
 void insert_hash_table(struct hsearch_data *hashtab, char *key, void *data);

/* renvoie 1 si la ligne est vide ou ne comporte que des espaces */
/* renvoie 0 sinon */
extern int empty_line(char *ligne);

/* renvoie 1 si la ligne debute par un caractere de commentaire */
/* renvoie 0 sinon */
extern int test_comment(char *ligne);

extern type_noeud_comment *add_comment(type_noeud_comment * comment, char *chaine);

int compte_one_weight(FILE *f, char *statut);
#endif

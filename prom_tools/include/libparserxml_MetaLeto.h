/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef LIBPARSERXML_METALETO_H
#define LIBPARSERXML_METALETO_H

#include <mxml.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


/* ------------------ VARIABLES ------------------ */

/* ------ COMPUTER ------ */
typedef struct computer {
	char * name;
	char * ip;
	char * login;
	char * path;
	char * cpu;
	char * informations;

	struct computer * first;
	struct computer * next;
} computer;


/* ------ LINK ------ */
typedef struct link_metaleto {
	char * name;
	char * input;
	char * output;

	struct link_metaleto * first;
	struct link_metaleto * next;
} link_metaleto;


/* ------ NODE ------ */
typedef struct node {
	char * name;
	char * computer;
	char * login;
	char * cmd;
	char * deploy;
	char * options;
	char * script;
	char * symb;
	char * draw;
	char * res;
	char * config;
	char * bus;
	char * var;
	char * dev;
	char * gcd;
	char * prt;
	char * keyboard_input;
	char * distant_directory;
	int overwrite_res;
	char *synchronize_files;
	char *synchronize_directories;


	struct node * first;
	struct node * next;
} node;

/* Global section */
typedef struct
{
	char * var;
	char * prt;
} global_sec;

/* ------------------ FUNCTIONS ------------------ */

char * getValeur(mxml_node_t * mxml_node, mxml_node_t * tree, const char * arg );

/* ------- Global --------*/
global_sec * getGlobalSec( mxml_node_t *  );

/* ------ COMPUTER ------ */
struct computer * addComputer(computer *);
struct computer * delComputer(struct  computer *);
struct computer * delComputerByName(struct  computer *, char *);
struct computer * getComputers(mxml_node_t *);
struct computer * getComputersNet(mxml_node_t *);
struct computer * getNextComputer(computer *);
struct computer * getComputerByName(computer *, char *);

char * getNameComputer(computer *);
char * getIp(computer *);
char * getLogin(computer *);
char * getPath(computer *);
char * getCpu(computer *);
char * getInformations(computer *);

int setNameComputer(computer *,char *);
int setIp(computer *,char *);
int setLogin(computer *,char *);
int setPath(computer *,char *);
int setCpu(computer *,char *);
int setInformations(computer *,char *);


/* ------ LINK ------ */
struct link_metaleto * addLink(link_metaleto *);
struct link_metaleto * delLink(struct  link_metaleto *);
struct link_metaleto * delLinkByName(struct  link_metaleto *, char *);
struct link_metaleto * getLinks(mxml_node_t *);
struct link_metaleto * getNextLink(link_metaleto *);
struct link_metaleto * getLinkByName(link_metaleto *, char *);

char * getNameLink(link_metaleto *);
char * getInput(link_metaleto *);
char * getOutput(link_metaleto *);

int setNameLink(link_metaleto *,char *);
int setInput(link_metaleto *,char *);
int setOutput(link_metaleto *,char *);


/* ------ NODE ------ */
struct node * addNode(node * );
struct node * delNode(struct  node *);
struct node * delNodeByName(struct  node *, char *);
struct node * getNetwork(mxml_node_t *);
struct node * getNextNode(node *);
struct node * getNodeByName(node * n, char *);

char * getNameNode(node *);
char * getComputer(node *);
char * getCmd(node *);
char * getDeploy(node *);
char * getOptions(node *);
char * getScript(node *);
char * getScriptNonSymb(node *);
char * getDraw(node *);
char * getRes(node *);
char * getConfig(node *);
char * getBus(node *);
char * getVar(node *);
char * getDev(node *);
char * getGcd(node *);
char * getPrt(node *);

/* Generic function to do the same */
char *getNodeField(struct node *node, char *field);
void getIvy(char * dest, mxml_node_t * tree);

int setNameNode(node *,char *);
int setComputer(node *,char *);
int setCmd(node *,char *);
int setDeploy(node *,char *);
int setOptions(node *,char *);
int setScript(node *,char *);
int setScriptNonSymb(node *,char *);
int setDraw(node *,char *);
int setRes(node *,char *);
int setConfig(node *,char *);
int setBus(node *,char *);
int setVar(node *,char *);
int setDev(node *,char *);
int setGcd(node *,char *);
int setPrt(node *,char *);

/* Generic function to do the same */
int setNodeField(node *node, char **field, const char *value);

/* ------ OTHERS ------ */
const char * whitespace_cb(mxml_node_t *, int);
const char * whitespace_cb_computer(mxml_node_t *, int);
/* mxml_node_t * loadFile_MetaLeto(char *); */
int saveNetFile(node *,link_metaleto *,char *, char *);
int saveComputerFile(computer *,char *);

#endif

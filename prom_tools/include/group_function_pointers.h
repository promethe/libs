/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * group_function_pointers.h
 *
 *  Created on: 30 août 2012
 *      Author: souheil
 */


#ifndef GROUP_FUNCTION_POINTERS_H
#define GROUP_FUNCTION_POINTERS_H

typedef struct type_help
{
  char *description;
} type_help;

typedef struct group_function_pointers
{
  const char *name;
  void (*appel_algo) (int gpe);
  void (*new) (int gpe);      /* fonctions appelee a la fin de create_and_manage() */
  void (*destroy) (int gpe);
  void (*save) (int gpe); /* fonction utilisee lors de la sauvegarde du reseau */
  type_help *help;
  int no; /* au cas ou l'on aurait besoin de se balader dans le tableau, on peut numeroter automatiquement. utilisateur met -1 */
  int nb_links_min; /* pour un jour tester si l'on a assez de liens sur certains groupes ? -1 ou 0 par defaut*/
} type_group_function_pointers;




#endif /* GROUP_FUNCTION_POINTERS_H_ */

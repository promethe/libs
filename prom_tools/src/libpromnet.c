/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include "basic_tools.h"
#include "libpromnet.h"

/* AB:Essai pour tester si la version de la lib et du .h correspndent */
const int promnet_version = PROMNET_VERSION;

t_promnet *promnet_init()
{
  t_promnet *promnet = NULL;

  promnet = ALLOCATION(t_promnet);
  memset(promnet, 0, sizeof(t_promnet));

  return promnet;
}

t_computer *promnet_add_new_computer(t_promnet *promnet)
{
  t_computer *computer = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_add_computer : invalid promnet pointer\n");
    return NULL;
  }

  computer = promnet->computer;

  for (; (computer != NULL) && (computer->next != NULL); computer = computer->next)
    ;

  if (computer == NULL)
  {
    if ((promnet->computer = malloc(sizeof(t_computer))) == NULL)
    {
      perror("promnet_add_computer : malloc");
      return NULL;
    }
    memset(promnet->computer, 0, sizeof(t_computer));
    computer = promnet->computer;
  }
  else if (computer->next == NULL)
  {
    if ((computer->next = malloc(sizeof(t_computer))) == NULL)
    {
      perror("promnet_add_computer : malloc");
      return NULL;
    }
    memset(computer->next, 0, sizeof(t_computer));
    computer->next->prev = computer;
    computer = computer->next;
  }
  else
  {
    fprintf(stderr, "promnet_add_computer : unknown error in computers list\n");
    return NULL;
  }
  promnet->tree_computer = addComputer(promnet->tree_computer);
  computer->data = promnet->tree_computer;
  return computer;
}

t_computer *promnet_add_computer(t_promnet *promnet)
{
  t_computer *computer = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_add_computer : invalid promnet pointer\n");
    return NULL;
  }

  computer = promnet->computer;

  for (; (computer != NULL) && (computer->next != NULL); computer = computer->next)
    ;

  if (computer == NULL)
  {
    if ((promnet->computer = malloc(sizeof(t_computer))) == NULL)
    {
      perror("promnet_add_computer : malloc");
      return NULL;
    }
    memset(promnet->computer, 0, sizeof(t_computer));
    computer = promnet->computer;
  }
  else if (computer->next == NULL)
  {
    if ((computer->next = malloc(sizeof(t_computer))) == NULL)
    {
      perror("promnet_add_computer : malloc");
      return NULL;
    }
    memset(computer->next, 0, sizeof(t_computer));
    computer->next->prev = computer;
    computer = computer->next;
  }
  else
  {
    fprintf(stderr, "promnet_add_computer : unknown error in computers list\n");
    return NULL;
  }
  return computer;
}

int promnet_computer_set_name(t_computer *computer, char *name)
{
  int len = -1;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_set_name : invalid computer pointer\n");
    return -1;
  }
  if (name == NULL)
  {
    fprintf(stderr, "promnet_computer_set_name : invalid name pointer\n");
    return -1;
  }
  len = strlen(name);
  if ((len >= 0) && (len < COMPUTER_MAX))
  {
    memcpy(computer->name, name, len + 1);
    setNameComputer(computer->data, name);
    return 0;
  }
  return -1;
}

int promnet_computer_set_address(t_computer *computer, char *address)
{
  int len = -1;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_set_address : invalid computer pointer\n");
    return -1;
  }
  if (address == NULL)
  {
    fprintf(stderr, "promnet_computer_set_address : invalid address pointer\n");
    return -1;
  }
  len = strlen(address);
  if ((len >= 0) && (len < ADDRESS_MAX))
  {
    memcpy(computer->address, address, len + 1);
    setIp(computer->data, address);
    return 0;
  }
  return -1;
}

int promnet_computer_set_login(t_computer *computer, char *login)
{
  int len = -1;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_set_login : invalid computer pointer\n");
    return -1;
  }
  if (login == NULL)
  {
    fprintf(stderr, "promnet_computer_set_login : invalid login pointer\n");
    return -1;
  }
  len = strlen(login);
  if ((len >= 0) && (len < LOGIN_MAX))
  {
    memcpy(computer->login, login, len + 1);
    setLogin(computer->data, login);
    return 0;
  }
  return -1;
}

int promnet_computer_set_cpu(t_computer *computer, char *cpu)
{
  int len = -1;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_set_cpu : invalid computer pointer\n");
    return -1;
  }
  if (cpu == NULL)
  {
    fprintf(stderr, "promnet_computer_set_cpu : invalid cpu pointer\n");
    return -1;
  }
  len = strlen(cpu);
  if ((len >= 0) && (len < CPU_MAX))
  {
    memcpy(computer->cpu, cpu, len + 1);
    setCpu(computer->data, cpu);
    return 0;
  }
  return -1;
}

int promnet_computer_set_path(t_computer *computer, char *path)
{
  int len = -1;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_set_path : invalid computer pointer\n");
    return -1;
  }
  if (path == NULL)
  {
    fprintf(stderr, "promnet_computer_set_path : invalid path pointer\n");
    return -1;
  }
  len = strlen(path);
  if ((len >= 0) && (len < CPU_MAX))
  {
    memcpy(computer->path, path, len + 1);
    setPath(computer->data, path);
    return 0;
  }
  return -1;
}

int promnet_computer_set_information(t_computer *computer, char *information)
{
  int len = -1;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_set_information : invalid computer pointer\n");
    return -1;
  }
  if (information == NULL)
  {
    fprintf(stderr, "promnet_computer_set_information : invalid information pointer\n");
    return -1;
  }
  len = strlen(information);
  if ((len >= 0) && (len < INFORMATION_MAX))
  {
    memcpy(computer->information, information, len + 1);
    setInformations(computer->data, information);
    return 0;
  }
  return -1;
}

char *promnet_computer_get_name(t_computer *computer)
{
  char *name = NULL;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_get_name : invalid computer pointer\n");
    return NULL;
  }
  if ((name = strdup((void *) computer->name)) == NULL)
  {
    perror("promnet_computer_get_name : error");
    return NULL;
  }
  return name;
}

char *promnet_computer_get_address(t_computer *computer)
{
  char *address = NULL;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_get_address : invalid computer pointer\n");
    return NULL;
  }
  if ((address = strdup((void *) computer->address)) == NULL)
  {
    perror("promnet_computer_get_address : error");
    return NULL;
  }
  return address;
}

char *promnet_computer_get_login(t_computer *computer)
{
  char *login = NULL;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_get_login : invalid computer pointer\n");
    return NULL;
  }
  if ((login = strdup((void *) computer->login)) == NULL)
  {
    perror("promnet_computer_get_login : error");
    return NULL;
  }
  return login;
}

char *promnet_computer_get_cpu(t_computer *computer)
{
  char *cpu = NULL;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_get_cpu : invalid computer pointer\n");
    return NULL;
  }
  if ((cpu = strdup((void *) computer->cpu)) == NULL)
  {
    perror("promnet_computer_get_cpu : error");
    return NULL;
  }
  return cpu;
}

char *promnet_computer_get_path(t_computer *computer)
{
  char *path = NULL;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_get_path : invalid computer pointer\n");
    return NULL;
  }
  if ((path = strdup((void *) computer->path)) == NULL)
  {
    perror("promnet_computer_get_path : error");
    return NULL;
  }
  return path;
}

char *promnet_computer_get_information(t_computer *computer)
{
  char *information = NULL;

  if (computer == NULL)
  {
    fprintf(stderr, "promnet_computer_get_information : invalid computer pointer\n");
    return NULL;
  }
  if ((information = strdup((void *) computer->information)) == NULL)
  {
    perror("promnet_computer_get_information : error");
    return NULL;
  }
  return information;
}

int promnet_del_computer(t_promnet *promnet, t_computer *computer)
{
  t_computer *pcomputer = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_del_computer : invalid promnet pointer\n");
    return -1;
  }
  if (computer == NULL)
  {
    fprintf(stderr, "promnet_del_computer : invalid computer pointer\n");
    return -1;
  }

  pcomputer = promnet->computer;

  for (; (pcomputer != NULL) && (pcomputer != computer); pcomputer = pcomputer->next)
    ;

  if (pcomputer == NULL)
  {
    fprintf(stderr, "promnet_del_computer : unknown computer\n");
    return -1;
  }

  /*
   ** Here pcomputer == computer
   */
  if (promnet->computer == pcomputer)
  {
    promnet->computer = pcomputer->next;
    if (promnet->computer) promnet->computer->prev = NULL;
  }
  else
  {
    if (computer->next != NULL) computer->next->prev = pcomputer->prev;
    if (computer->prev != NULL) computer->prev->next = pcomputer->next;
    if ((computer->next == NULL) && (computer->prev == NULL)) promnet->computer = NULL;
  }
  promnet->tree_computer = delComputer(computer->data);
  free(computer);
  computer = pcomputer = NULL;
  return 0;
}

void promnet_computer_copy(t_computer *src, t_computer *dst)
{
  char *val = NULL;

  val = promnet_computer_get_name(src);
  promnet_computer_set_name(dst, val);
  free(val);
  val = promnet_computer_get_address(src);
  promnet_computer_set_address(dst, val);
  free(val);
  val = promnet_computer_get_login(src);
  promnet_computer_set_login(dst, val);
  free(val);
  val = promnet_computer_get_cpu(src);
  promnet_computer_set_cpu(dst, val);
  free(val);
  val = promnet_computer_get_path(src);
  promnet_computer_set_path(dst, val);
  free(val);
  val = promnet_computer_get_information(src);
  promnet_computer_set_information(dst, val);
  free(val);
}

t_computer *promnet_computer_get_next(t_promnet *promnet, t_computer *computer)
{
  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_computer_get_next : invalid promnet pointer\n");
    return NULL;
  }
  if (computer == NULL) return promnet->computer;
  else return computer->next;
}

t_prom_script *promnet_add_new_prom_script(t_promnet *promnet)
{
  t_prom_script *prom_script = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_add_prom_script : invalid promnet pointer\n");
    return NULL;
  }

  prom_script = promnet->prom_script;

  for (; (prom_script != NULL) && (prom_script->next != NULL); prom_script = prom_script->next)
    ;

  if (prom_script == NULL)
  {
    if ((promnet->prom_script = malloc(sizeof(t_prom_script))) == NULL)
    {
      perror("promnet_add_prom_script : malloc");
      return NULL;
    }
    memset(promnet->prom_script, 0, sizeof(t_prom_script));
    prom_script = promnet->prom_script;
  }
  else if (prom_script->next == NULL)
  {
    if ((prom_script->next = malloc(sizeof(t_prom_script))) == NULL)
    {
      perror("promnet_add_prom_script : malloc");
      return NULL;
    }
    memset(prom_script->next, 0, sizeof(t_prom_script));
    prom_script->next->prev = prom_script;
    prom_script = prom_script->next;
  }
  else
  {
    fprintf(stderr, "promnet_add_prom_script : unknown error in prom_scripts list\n");
    return NULL;
  }
  promnet->tree_script = addNode(promnet->tree_script);
  prom_script->data = promnet->tree_script;

  /* permet de gerer une section globale var/prt afin de gerer les makefile dans themis  */  
  if( promnet->global != NULL) prom_script->has_global_section = 1; 

  return prom_script;
}

t_prom_script *promnet_add_prom_script(t_promnet *promnet)
{
  t_prom_script *prom_script = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_add_prom_script : invalid promnet pointer\n");
    return NULL;
  }

  prom_script = promnet->prom_script;

  for (; (prom_script != NULL) && (prom_script->next != NULL); prom_script = prom_script->next)
    ;

  if (prom_script == NULL)
  {
    if ((promnet->prom_script = malloc(sizeof(t_prom_script))) == NULL)
    {
      perror("promnet_add_prom_script : malloc");
      return NULL;
    }
    memset(promnet->prom_script, 0, sizeof(t_prom_script));
    prom_script = promnet->prom_script;
  }
  else if (prom_script->next == NULL)
  {
    if ((prom_script->next = malloc(sizeof(t_prom_script))) == NULL)
    {
      perror("promnet_add_prom_script : malloc");
      return NULL;
    }
    memset(prom_script->next, 0, sizeof(t_prom_script));
    prom_script->next->prev = prom_script;
    prom_script = prom_script->next;
  }
  else
  {
    fprintf(stderr, "promnet_add_prom_script : unknown error in prom_scripts list\n");
    return NULL;
  }
 
  /* permet de gerer une section globale var/prt afin de gerer les makefile dans themis  */  
  if( promnet->global != NULL) prom_script->has_global_section = 1; 


  return prom_script;
}

int promnet_prom_script_set_logical_name(t_prom_script *prom_script, char *logical_name)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_logical_name : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->logical_name[0] = 0;

  if (logical_name == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_logical_name : invalid logical_name pointer\n");
    return -1;
  }

  len = strlen(logical_name);
  if ((len >= 0) && (len < LOGICAL_NAME_MAX))
  {
    memcpy(prom_script->logical_name, logical_name, len + 1);
    setNameNode(prom_script->data, logical_name);
    return 0;
  }

  return -1;
}

int promnet_prom_script_set_path_prom_deploy(t_prom_script *prom_script, char *path_prom_deploy)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_prom_deploy : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_prom_deploy[0] = 0;
  if (path_prom_deploy == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_prom_deploy : invalid path_prom_deploy pointer\n");
    return -1;
  }
  len = strlen(path_prom_deploy);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_prom_deploy, path_prom_deploy, len + 1);
    setDeploy(prom_script->data, path_prom_deploy);
    return 0;
  }

  return -1;
}

int promnet_prom_script_set_path_prom_binary(t_prom_script *prom_script, char *path_prom_binary)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_prom_binary : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_prom_binary[0] = 0;
  if (path_prom_binary == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_prom_binary : invalid path_prom_binary pointer\n");
    return -1;
  }
  len = strlen(path_prom_binary);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_prom_binary, path_prom_binary, len + 1);
    setCmd(prom_script->data, path_prom_binary);
    return 0;
  }

  return -1;
}

void promnet_prom_script_set_all_path(t_prom_script *prom_script, char *path, char *logical_name)
{
  char all_path[MAX_ALL_PATH];
  (void) logical_name;

  if (strlen(prom_script->path_file_script) <= 0)
  {
    memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
    sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_SCRIPT);
    promnet_prom_script_set_path_file_script(prom_script, all_path);
  }

  if (strlen(prom_script->path_file_symb) <= 0)
  {
    PRINT_WARNING("You should use .symb instead of .script");
  }
  /*
   memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
   sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_SYMB);
   promnet_prom_script_set_path_file_symb(prom_script, all_path);
   */

  if (strlen(prom_script->path_file_draw) <= 0)
  {
    memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
    sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_DRAW);
    promnet_prom_script_set_path_file_draw(prom_script, all_path);
  }

  if (strlen(prom_script->path_file_res) <= 0)
  {
    memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
    sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_RES);
    promnet_prom_script_set_path_file_res(prom_script, all_path);
  }

  if (strlen(prom_script->path_file_config) <= 0)
  {
    memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
    sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_CONFIG);
    promnet_prom_script_set_path_file_config(prom_script, all_path);
  }

  /*
   if (strlen(prom_script->path_file_bus) <= 0)
   {
   memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
   sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_BUS);
   promnet_prom_script_set_path_file_bus(prom_script, all_path);
   }


   if (strlen(prom_script->path_file_var) <= 0)
   {
   memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
   sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_VAR);
   promnet_prom_script_set_path_file_var(prom_script, all_path);
   }

   if (strlen(prom_script->path_file_dev) <= 0)
   {
   memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
   sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_DEV);
   promnet_prom_script_set_path_file_dev(prom_script, all_path);
   }
   */

  if (strlen(prom_script->path_file_gcd) <= 0)
  {
    memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
    sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_GCD);
    promnet_prom_script_set_path_file_gcd(prom_script, all_path);
  }

  /*
   if (strlen(prom_script->path_file_prt) <= 0)
   {
   memset(all_path, 0, MAX_ALL_PATH * sizeof(char));
   sprintf(all_path, "%s%s", prom_script->logical_name, EXT_SCRIPT_PRT);
   promnet_prom_script_set_path_file_prt(prom_script, all_path);
   }
   */
  promnet_prom_script_set_path_prom_deploy(prom_script, path);
  promnet_prom_script_set_path_prom_binary(prom_script, (char*) "promethe");

}

int promnet_prom_script_set_path_file_script(t_prom_script *prom_script, char *path_file_script)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_script : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_file_script[0] = 0;
  if (path_file_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_script : invalid path_file_script pointer\n");
    return -1;
  }
  len = strlen(path_file_script);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_script, path_file_script, len + 1);

    setNodeField(prom_script->data, &prom_script->data->script, path_file_script);
    return 0;
  }

  return -1;
}

int promnet_prom_script_set_path_file_symb(t_prom_script *prom_script, char *path_file_symb)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_symb : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_file_symb[0] = 0;
  if (path_file_symb == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_script_symb : invalid path_file_script pointer\n");
    return -1;
  }
  len = strlen(path_file_symb);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_symb, path_file_symb, len + 1);

    setNodeField(prom_script->data, &prom_script->data->symb, path_file_symb);

    return 0;
  }

  return -1;
}

int promnet_prom_script_set_path_file_draw(t_prom_script *prom_script, char *path_file_draw)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_draw : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_file_draw[0] = 0;
  if (path_file_draw == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_draw : invalid path_file_draw pointer\n");
    return -1;
  }
  len = strlen(path_file_draw);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_draw, path_file_draw, len + 1);
    setDraw(prom_script->data, path_file_draw);
    return 0;
  }

  return -1;
}

int promnet_prom_script_set_path_file_res(t_prom_script *prom_script, char *path_file_res)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_res : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_file_res[0] = 0;
  if (path_file_res == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_res : invalid path_file_res pointer\n");
    return -1;
  }
  len = strlen(path_file_res);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_res, path_file_res, len + 1);
    setRes(prom_script->data, path_file_res);
    return 0;
  }

  return -1;
}

int promnet_prom_script_set_path_file_config(t_prom_script *prom_script, char *path_file_config)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_config : invalid prom_script pointer\n");
    return -1;
  }

  prom_script->path_file_config[0] = 0;
  if (path_file_config == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_config : invalid path_file_config pointer\n");
    return -1;
  }
  len = strlen(path_file_config);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_config, path_file_config, len + 1);
    setConfig(prom_script->data, path_file_config);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_path_file_bus(t_prom_script *prom_script, char *path_file_bus)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_bus : invalid prom_script pointer\n");
    return -1;
  }
  if (path_file_bus == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_bus : invalid path_file_bus pointer\n");
    return -1;
  }
  len = strlen(path_file_bus);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_bus, path_file_bus, len + 1);
    setBus(prom_script->data, path_file_bus);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_path_file_var(t_prom_script *prom_script, char *path_file_var)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_var : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_file_var[0] = 0;
  if (path_file_var == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_var : invalid path_file_var pointer\n");
    return -1;
  }
  len = strlen(path_file_var);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_var, path_file_var, len + 1);
    setVar(prom_script->data, path_file_var);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_path_file_dev(t_prom_script *prom_script, char *path_file_dev)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_dev : invalid prom_script pointer\n");
    return -1;
  }

  prom_script->path_file_dev[0] = 0;

  if (path_file_dev == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_dev : invalid path_file_dev pointer\n");
    return -1;
  }
  len = strlen(path_file_dev);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_dev, path_file_dev, len + 1);
    setDev(prom_script->data, path_file_dev);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_path_file_prt(t_prom_script *prom_script, char *path_file_prt)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_prt : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_file_prt[0] = 0;
  if (path_file_prt == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_prt : invalid path_file_prt pointer\n");
    return -1;
  }
  len = strlen(path_file_prt);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_prt, path_file_prt, len + 1);
    setPrt(prom_script->data, path_file_prt);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_path_file_gcd(t_prom_script *prom_script, char *path_file_gcd)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_gcd : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->path_file_gcd[0] = 0;

  if (path_file_gcd == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_path_file_gcd : invalid path_file_gcd pointer\n");
    return -1;
  }
  len = strlen(path_file_gcd);
  if ((len >= 0) && (len < PATH_MAX))
  {
    memcpy(prom_script->path_file_gcd, path_file_gcd, len + 1);
    setGcd(prom_script->data, path_file_gcd);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_prom_args_line(t_prom_script *prom_script, char *prom_args_line)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_prom_args_line : invalid prom_script pointer\n");
    return -1;
  }
  prom_script->prom_args_line[0] = 0;

  if (prom_args_line == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_prom_args_line : invalid prom_args_line pointer\n");
    return -1;
  }
  len = strlen(prom_args_line);
  if ((len >= 0) && (len < PROM_ARGS_LINE_MAX))
  {
    memcpy(prom_script->prom_args_line, prom_args_line, len + 1);
    setOptions(prom_script->data, prom_args_line);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_computer(t_prom_script *prom_script, char *computer)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_computer : invalid prom_script pointer\n");
    return -1;
  }

  prom_script->computer[0] = 0;
  if (computer == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_computer : invalid computer pointer\n");
    return -1;
  }
  len = strlen(computer);
  if ((len >= 0) && (len < COMPUTER_MAX))
  {
    memcpy(prom_script->computer, computer, len + 1);
    setComputer(prom_script->data, computer);
    return 0;
  }
  return -1;
}

int promnet_prom_script_set_field(t_prom_script *prom_script, char *script_field, char **node_field, char *value, int size_of_field)
{
  int len = -1;

  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_field : invalid prom_script pointer\n");
    return -1;
  }

  script_field[0] = 0;
  if (value == NULL)
  {
    fprintf(stderr, "promnet_prom_script_set_field : invalid computer pointer\n");
    return -1;
  }

  len = strlen(value);
  if ((len >= 0) && (len < size_of_field))
  {
    memcpy(script_field, value, len + 1);
    setNodeField(prom_script->data, node_field, value);
    return 0;
  }
  return -1;
}

char *promnet_prom_script_get_argument(t_prom_script *prom_script, void* argument_pointer)
{
  char *argument;
  if (prom_script == NULL)
  {
    PRINT_WARNING("NULL prom_script pointer");
    return NULL;
  }

  if ((argument = strdup(argument_pointer)) == NULL)
  {
    PRINT_SYSTEM_ERROR();
    return NULL;
  }
  return argument;
}

int promnet_del_prom_script(t_promnet *promnet, t_prom_script *prom_script)
{
  t_prom_script *current_script = NULL;

  if (promnet == NULL) EXIT_ON_ERROR("invalid promnet pointer\n");
  if (prom_script == NULL) EXIT_ON_ERROR("invalid prom_script pointer\n");

  for (current_script = promnet->prom_script; current_script != NULL; current_script = current_script->next)
  {
    if (current_script == prom_script) break;
  }

  if (current_script == NULL) EXIT_ON_ERROR("unknown prom_script\n");

  /*
   ** Here current_script == prom_script
   */
  if (promnet->prom_script == current_script)
  {
    promnet->prom_script = current_script->next;
    if (promnet->prom_script) promnet->prom_script->prev = NULL;
  }
  else
  {
    if (prom_script->next != NULL) prom_script->next->prev = current_script->prev;
    if (prom_script->prev != NULL) prom_script->prev->next = current_script->next;
    if ((prom_script->next == NULL) && (prom_script->prev == NULL)) promnet->prom_script = NULL;
  }
  /* promnet->tree_script = delNode(current_script->data); POetite fuite de m��moire mais  a terme node disparatra*/
  free(prom_script);
  prom_script = current_script = NULL;

  return 0;
}

void promnet_del_all_prom_script(t_promnet *promnet)
{
  t_prom_script *current_script;
  t_prom_script *previous_script = NULL;

  if (promnet == NULL) EXIT_ON_ERROR("invalid promnet pointer\n");

  for (current_script = promnet->prom_script; current_script != NULL; current_script = current_script->next)
  {
    if (previous_script != NULL)
    {
      free(previous_script->data);
      free(previous_script);
    }
    previous_script = current_script;
  }
  free(previous_script);
  promnet->prom_script = NULL;
}

t_prom_script *promnet_prom_script_get_next(t_promnet *promnet, t_prom_script *prom_script)
{
  static t_prom_script *last_script = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_prom_script_get_next : invalid promnet pointer\n");
    return NULL;
  }
  if (prom_script == NULL) if (last_script == NULL)
  {
    last_script = promnet->prom_script;
    return promnet->prom_script;
  }
  else
  {
    if (last_script->next != NULL)
    {
      last_script = last_script->next;
      return last_script;
    }
    else return last_script->next;
  }
  else
  {
    if (prom_script->next != NULL)
    {
      last_script = prom_script->next;
    }
    return prom_script->next;
  }
}

t_prom_script *promnet_prom_script_find_by_name(t_prom_script *prom_script, char *logical_name)
{
  if (prom_script == NULL)
  {
    fprintf(stderr, "promnet_prom_script_find_by_name : invalid prom_script pointer\n");
    return NULL;
  }

  for (; prom_script != NULL; prom_script = prom_script->next)
  {
    if (!strncmp(logical_name, prom_script->logical_name, LOGICAL_NAME_MAX)) return prom_script;
  }
  PRINT_WARNING("Pas de correspondance trouvee: logical_name=%s \n", logical_name);
  PRINT_WARNING("Corriger le .net (bug coeos?) \n");
  return NULL;
}

t_prom_link *promnet_add_new_prom_link(t_promnet *promnet)
{
  t_prom_link *prom_link = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_add_prom_link : invalid promnet pointer\n");
    return NULL;
  }

  prom_link = promnet->prom_link;

  for (; (prom_link != NULL) && (prom_link->next != NULL); prom_link = prom_link->next)
    ;

  if (prom_link == NULL)
  {
    if ((promnet->prom_link = malloc(sizeof(t_prom_link))) == NULL)
    {
      perror("promnet_add_prom_link : malloc");
      return NULL;
    }
    memset(promnet->prom_link, 0, sizeof(t_prom_link));
    prom_link = promnet->prom_link;
  }
  else if (prom_link->next == NULL)
  {
    if ((prom_link->next = malloc(sizeof(t_prom_link))) == NULL)
    {
      perror("promnet_add_prom_link : malloc");
      return NULL;
    }
    memset(prom_link->next, 0, sizeof(t_prom_link));
    prom_link->next->prev = prom_link;
    prom_link = prom_link->next;
  }
  else
  {
    fprintf(stderr, "promnet_add_prom_link : unknown error in prom_links list\n");
    return NULL;
  }
  promnet->tree_link = addLink(promnet->tree_link);
  prom_link->data = promnet->tree_link;
  return prom_link;
}

t_prom_link *promnet_add_prom_link(t_promnet *promnet)
{
  t_prom_link *prom_link = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_add_prom_link : invalid promnet pointer\n");
    return NULL;
  }

  prom_link = promnet->prom_link;

  for (; (prom_link != NULL) && (prom_link->next != NULL); prom_link = prom_link->next)
    ;

  if (prom_link == NULL)
  {
    if ((promnet->prom_link = malloc(sizeof(t_prom_link))) == NULL)
    {
      perror("promnet_add_prom_link : malloc");
      return NULL;
    }
    memset(promnet->prom_link, 0, sizeof(t_prom_link));
    prom_link = promnet->prom_link;
  }
  else if (prom_link->next == NULL)
  {
    if ((prom_link->next = malloc(sizeof(t_prom_link))) == NULL)
    {
      perror("promnet_add_prom_link : malloc");
      return NULL;
    }
    memset(prom_link->next, 0, sizeof(t_prom_link));
    prom_link->next->prev = prom_link;
    prom_link = prom_link->next;
  }
  else
  {
    fprintf(stderr, "promnet_add_prom_link : unknown error in prom_links list\n");
    return NULL;
  }
  return prom_link;
}

int promnet_prom_link_set_name(t_prom_link *prom_link, char *name)
{
  int len = -1;

  if (prom_link == NULL)
  {
    fprintf(stderr, "promnet_prom_link_set_name : invalid prom_link pointer\n");
    return -1;
  }
  if (name == NULL)
  {
    fprintf(stderr, "promnet_prom_link_set_name : invalid name pointer\n");
    return -1;
  }
  len = strlen(name);
  if ((len >= 0) && (len < LINK_NAME_MAX))
  {
    memcpy(prom_link->name, name, len + 1);
    setNameLink(prom_link->data, name);
    return 0;
  }
  return -1;
}

int promnet_prom_link_set_input(t_prom_link *prom_link, t_prom_script *input)
{
  if (prom_link == NULL)
  {
    fprintf(stderr, "promnet_prom_link_set_input : invalid prom_link pointer\n");
    return -1;
  }
  if (input == NULL)
  {
    fprintf(stderr, "promnet_prom_link_set_input : invalid input script pointer\n");
    return -1;
  }
  prom_link->in = input;
  setInput(prom_link->data, input->logical_name);
  return 0;
}

int promnet_prom_link_set_output(t_prom_link *prom_link, t_prom_script *output)
{
  if (prom_link == NULL)
  {
    fprintf(stderr, "promnet_prom_link_set_output : invalid prom_link pointer\n");
    return -1;
  }
  if (output == NULL)
  {
    fprintf(stderr, "promnet_prom_link_set_output : invalid output script pointer\n");
    return -1;
  }
  prom_link->out = output;
  setOutput(prom_link->data, output->logical_name);
  return 0;
}

char *promnet_prom_link_get_name(t_prom_link *prom_link)
{
  char *name = NULL;

  if (prom_link == NULL)
  {
    fprintf(stderr, "promnet_prom_link_get_name : invalid prom_link pointer\n");
    return NULL;
  }
  if ((name = strdup((void *) prom_link->name)) == NULL)
  {
    perror("promnet_prom_link_get_name : error");
    return NULL;
  }
  return name;
}

t_prom_script *promnet_prom_link_get_input(t_prom_link *prom_link)
{
  if (prom_link == NULL)
  {
    fprintf(stderr, "promnet_prom_link_get_input : invalid prom_link pointer\n");
    return NULL;
  }
  return prom_link->in;
}

t_prom_script *promnet_prom_link_get_output(t_prom_link *prom_link)
{
  if (prom_link == NULL)
  {
    fprintf(stderr, "promnet_prom_link_get_output : invalid prom_link pointer\n");
    return NULL;
  }
  return prom_link->out;
}

int promnet_del_prom_link(t_promnet *promnet, t_prom_link *prom_link)
{
  t_prom_link *pprom_link = NULL;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_del_prom_link : invalid promnet pointer\n");
    return -1;
  }
  if (prom_link == NULL)
  {
    fprintf(stderr, "promnet_del_prom_link : invalid prom_link pointer\n");
    return -1;
  }

  pprom_link = promnet->prom_link;

  for (; (pprom_link != NULL) && (pprom_link != prom_link); pprom_link = pprom_link->next)
    ;

  if (pprom_link == NULL)
  {
    fprintf(stderr, "promnet_del_prom_link : unknown prom_link\n");
    return -1;
  }

  /*
   ** Here pprom_link == prom_link
   */

  if (promnet->prom_link == pprom_link)
  {
    promnet->prom_link = pprom_link->next;
    if (promnet->prom_link) promnet->prom_link->prev = NULL;
  }
  else
  {
    if (prom_link->next != NULL) prom_link->next->prev = pprom_link->prev;
    if (prom_link->prev != NULL) prom_link->prev->next = pprom_link->next;
    if ((prom_link->next == NULL) && (prom_link->prev == NULL)) promnet->prom_link = NULL;
  }
  promnet->tree_link = delLink(prom_link->data);
  free(prom_link);
  prom_link = pprom_link = NULL;

  return 0;
}

t_prom_link *promnet_prom_link_get_next(t_promnet *promnet, t_prom_link *prom_link)
{
  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_prom_link_get_next : invalid promnet pointer\n");
    return NULL;
  }
  if (prom_link == NULL) return promnet->prom_link;
  else return prom_link->next;
}

/* Remplace tous les appels du type:
 val = getNameNode(prom_script->data);
 promnet_prom_script_set_logical_name(prom_script, val);
 free(val);
 *
 */
void get_node_field_and_set_prom_script(t_prom_script *prom_script, char **data_field_adr, char *prom_script_field, int size_of_field)
{
  char *val;
  val = getNodeField(prom_script->data, *data_field_adr);
  promnet_prom_script_set_field(prom_script, prom_script_field, data_field_adr, val, size_of_field);
  free(val);
}

int promnet_load_prom_script(t_promnet *promnet)
{
  t_prom_script *prom_script = NULL;
  node *data = NULL;

  for (data = promnet->tree_script; data != NULL; data = getNextNode(data))
  {
    char *val = NULL;

    if ((prom_script = promnet_add_prom_script(promnet)) == NULL)
    {
      fprintf(stderr, "promnet_load_prom_script : invalid promnet pointer\n");
      return -1;
    }

    prom_script->data = data;

    val = getNameNode(prom_script->data);
    promnet_prom_script_set_logical_name(prom_script, val);
    free(val);

    get_node_field_and_set_prom_script(prom_script, &prom_script->data->cmd, prom_script->path_prom_binary, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->deploy, prom_script->path_prom_deploy, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->script, prom_script->path_file_script, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->symb, prom_script->path_file_symb, PATH_MAX);

    get_node_field_and_set_prom_script(prom_script, &prom_script->data->draw, prom_script->path_file_draw, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->res, prom_script->path_file_res, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->bus, prom_script->path_file_bus, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->config, prom_script->path_file_config, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->bus, prom_script->path_file_bus, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->var, prom_script->path_file_var, PATH_MAX);

    get_node_field_and_set_prom_script(prom_script, &prom_script->data->dev, prom_script->path_file_dev, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->prt, prom_script->path_file_prt, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->gcd, prom_script->path_file_gcd, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->computer, prom_script->computer, COMPUTER_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->options, prom_script->prom_args_line, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->login, prom_script->login, LOGIN_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->distant_directory, prom_script->distant_directory, PATH_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->keyboard_input, prom_script->keyboard_input, KEYBOARD_INPUT_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->synchronize_files, prom_script->synchronize_files, SYNCHRONIZE_PATHS_MAX);
    get_node_field_and_set_prom_script(prom_script, &prom_script->data->synchronize_directories, prom_script->synchronize_directories, SYNCHRONIZE_PATHS_MAX);

    if (prom_script->path_prom_deploy[0] == 0) strcpy(prom_script->path_prom_deploy, ".");

    prom_script->overwrite_res = prom_script->data->overwrite_res;
  }

  return 0;
}

int promnet_load_prom_link(t_promnet *promnet)
{
  t_prom_link *prom_link = NULL;
  t_prom_script *prom_script = NULL;
  link_metaleto *data = NULL;

  for (data = promnet->tree_link; data != NULL; data = getNextLink(data))
  {
    char *val = NULL;

    if ((prom_link = promnet_add_prom_link(promnet)) == NULL)
    {
      fprintf(stderr, "promnet_load_prom_link : invalid promnet pointer\n");
      return -1;
    }

    prom_link->data = data;

    val = getNameLink(prom_link->data);
    promnet_prom_link_set_name(prom_link, val);
    free(val);

    val = getInput(prom_link->data);
    prom_script = promnet_prom_script_find_by_name(promnet->prom_script, val);
    promnet_prom_link_set_input(prom_link, prom_script);
    free(val);

    val = getOutput(prom_link->data);
    prom_script = promnet_prom_script_find_by_name(promnet->prom_script, val);
    promnet_prom_link_set_output(prom_link, prom_script);
    free(val);
  }

  return 0;
}

int promnet_load_file_net(t_promnet *promnet, char *path)
{
  computer *tmp = NULL;
  if (promnet == NULL) EXIT_ON_ERROR("Invalid promnet pointer\n");
  if ((promnet->net_tree = loadFile(path)) == NULL)
  {
    PRINT_WARNING("Loading script tree failed\n\tThe file %s may not exist.", path);
    return 0;
  }
  
  /************* recupere la section globale **********/	
	
  promnet->global = getGlobalSec( promnet->net_tree  );  

  /****************************************************/

  getIvy(promnet->ivybus_envvar, promnet->net_tree);

  if ((promnet->tree_script = getNetwork(promnet->net_tree)) == NULL) PRINT_WARNING("No scripts in the net file %s\n", path);
  else if (promnet_load_prom_script(promnet) == -1) EXIT_ON_ERROR("Loading scripts failed\n");

  if ((promnet->tree_link = getLinks(promnet->net_tree)) != NULL)
  {
    if (promnet_load_prom_link(promnet) == -1)
    {
      EXIT_ON_ERROR("Loading links failed\n");
    }
  }

  /*recuperer les computers comme si on utilisaits un cpt*/

  tmp = getComputersNet(promnet->net_tree);
  if (tmp == NULL)
  {
    fprintf(stderr, "echec recuperation ordi %s\t Tentative avec le .cpt\n", path);
  }
  else
  {
    promnet->tree_computer = tmp;
    if (promnet_load_computer(promnet) == -1)
    {
      fprintf(stderr, "promnet_load_file_cpt : loading computers failed\n");
      return -1;
    }
  }

  promnet_set_net_path(promnet, path);
  return 0;
}

int promnet_load_computer(t_promnet *promnet)
{
  t_computer *prom_computer = NULL;
  computer *data = NULL;

  for (data = promnet->tree_computer; data != NULL; data = getNextComputer(data))
  {
    char *val = NULL;

    if ((prom_computer = promnet_add_computer(promnet)) == NULL)
    {
      fprintf(stderr, "promnet_load_computer : invalid promnet pointer\n");
      return -1;
    }

    prom_computer->data = data;

    val = getNameComputer(prom_computer->data);
    promnet_computer_set_name(prom_computer, val);
    free(val);

    val = getIp(prom_computer->data);
    promnet_computer_set_address(prom_computer, val);
    free(val);

    val = getLogin(prom_computer->data);
    promnet_computer_set_login(prom_computer, val);
    free(val);

    val = getPath(prom_computer->data);
    promnet_computer_set_path(prom_computer, val);
    free(val);

    val = getCpu(prom_computer->data);
    promnet_computer_set_cpu(prom_computer, val);
    free(val);

    val = getInformations(prom_computer->data);
    promnet_computer_set_information(prom_computer, val);
    free(val);
  }

  return 0;
}

int promnet_load_file_cpt(t_promnet *promnet, char *path)
{
  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_load_file_cpt : invalid promnet pointer\n");
    return -1;
  }

  if ((promnet->cpt_tree = loadFile(path)) == NULL)
  {
    fprintf(stderr, "promnet_load_file_cpt : loading cpt tree failed\n");
    return -1;
  }

  if ((promnet->tree_computer = getComputers(promnet->cpt_tree)) == NULL)
  {
    fprintf(stderr, "promnet_load_file_cpt : no computers in cpt file %s\n", path);
  }
  else
  {
    if (promnet_load_computer(promnet) == -1)
    {
      fprintf(stderr, "promnet_load_file_cpt : loading computers failed\n");
      return -1;
    }
  }
  promnet_set_cpt_path(promnet, path);
  return 0;
}

/* Remplacer a terme par promnet_save */
int promnet_save_file_net(t_promnet *promnet, char *path, char * ivybus_envvar)
{
  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_save_file_net : invalid promnet pointer\n");
    return -1;
  }

  /*saveNetFile(promnet->tree_script, promnet->tree_link, path, ivybus_envvar);*/
  promnet_save(promnet, path);
  promnet_set_net_path(promnet, path);
  promnet_set_ivybus_envvar(promnet, ivybus_envvar);
  return 0;
}

/* Sauvegarde en utilisant t_pronnet plutot que struct note ammene a remplacer le reste */
int promnet_save(t_promnet *promnet, char *filename)
{
  mxml_node_t *tree = NULL, *net = NULL, *node = NULL, *my_links = NULL, *my_link = NULL;
  mxml_node_t * glob;	
  mxml_node_t *tmp = NULL;
  t_prom_script *script;
  mxml_node_t *mes_ordis = NULL, *mon_ordi = NULL;
  computer * computers = NULL;

  struct link_metaleto * links = NULL;

  FILE *fp;
  int val;

  tree = mxmlNewElement(MXML_NO_PARENT, "application");
  xml_set_float(tree, "version", 1.0);

  /*
   * Network
   */
  if (tree != NULL)
  {

    net = mxmlNewElement(tree, "network");
    tmp = mxmlNewElement(net, "ivybus");
    /*	mxmlNewOpaque(tmp, ivybus_envvar);  TODO see how to manage ivy_var*/
    
    if(  promnet->global != NULL  )
    {
	glob = mxmlNewElement(net, "global");
        tmp = mxmlNewElement(glob, "var");
        mxmlNewOpaque(tmp,  promnet->global->var );
/*        
	tmp = mxmlNewElement(glob, "prt");
        mxmlNewOpaque(tmp,  promnet->global->prt );
*/
    } 	

    script = promnet->prom_script;
    while (script != NULL)
    {
      node = mxmlNewElement(net, "node");

      xml_set_int(node, "overwrite_res", script->overwrite_res);

      tmp = mxmlNewElement(node, "name");
      mxmlNewOpaque(tmp, script->logical_name);

      tmp = mxmlNewElement(node, "computer");
      mxmlNewOpaque(tmp, script->computer);

      tmp = mxmlNewElement(node, "login");
      mxmlNewOpaque(tmp, script->login);

      tmp = mxmlNewElement(node, "cmd");
      mxmlNewOpaque(tmp, script->path_prom_binary);

      tmp = mxmlNewElement(node, "deploy");
      mxmlNewOpaque(tmp, script->path_prom_deploy);

      tmp = mxmlNewElement(node, "distant_directory");
      mxmlNewOpaque(tmp, script->distant_directory);

      tmp = mxmlNewElement(node, "keyboard");
      mxmlNewOpaque(tmp, script->keyboard_input);

      tmp = mxmlNewElement(node, "synchronize_files");
      mxmlNewOpaque(tmp, script->synchronize_files);

      tmp = mxmlNewElement(node, "synchronize_directories");
      mxmlNewOpaque(tmp, script->synchronize_directories);

      tmp = mxmlNewElement(node, "options");
      mxmlNewOpaque(tmp, script->prom_args_line);

      node = mxmlNewElement(node, "path");

      tmp = mxmlNewElement(node, "script");
      mxmlNewOpaque(tmp, script->path_file_script);

      tmp = mxmlNewElement(node, "symb");
      mxmlNewOpaque(tmp, script->path_file_symb);

      tmp = mxmlNewElement(node, "draw");
      mxmlNewOpaque(tmp, script->path_file_draw);

      tmp = mxmlNewElement(node, "res");
      mxmlNewOpaque(tmp, script->path_file_res);

      tmp = mxmlNewElement(node, "config");
      mxmlNewOpaque(tmp, script->path_file_config);

      tmp = mxmlNewElement(node, "bus");
      mxmlNewOpaque(tmp, script->path_file_bus);

      tmp = mxmlNewElement(node, "var");
      mxmlNewOpaque(tmp, script->path_file_var);

      tmp = mxmlNewElement(node, "dev");
      mxmlNewOpaque(tmp, script->path_file_dev);

      tmp = mxmlNewElement(node, "gcd");
      mxmlNewOpaque(tmp, script->path_file_gcd);

      tmp = mxmlNewElement(node, "prt");
      mxmlNewOpaque(tmp, script->path_file_prt);

      script = script->next;
    }

    /*
     * Link in order to not loose them
     */
    my_links = mxmlNewElement(tree, "link_list");
    if (promnet->tree_link != NULL)
    {
      links = promnet->tree_link->first;
      while (links != NULL)
      {
        my_link = mxmlNewElement(my_links, "link");

        tmp = mxmlNewElement(my_link, "name");
        mxmlNewOpaque(tmp, links->name);

        tmp = mxmlNewElement(my_link, "input");
        mxmlNewOpaque(tmp, links->input);

        tmp = mxmlNewElement(my_link, "output");
        mxmlNewOpaque(tmp, links->output);

        links = links->next;
      }
    }

    /*** AJOUT ALI K. Tentative d'ecrire le cpt directement dans le .net
     * Coeos arrive a ecrire mais pas themis qui ne fait pas le init de promnet avec les memes outils que coeos*/
    mes_ordis = mxmlNewElement(tree, "computer_list");
    if (promnet->tree_computer != NULL)
    {
      computers = promnet->tree_computer->first;
      while (computers != NULL)
      {
        mon_ordi = mxmlNewElement(mes_ordis, "ordi");

        tmp = mxmlNewElement(mon_ordi, "namecpt");
        mxmlNewOpaque(tmp, computers->name);

        tmp = mxmlNewElement(mon_ordi, "ip");
        mxmlNewOpaque(tmp, computers->ip);

        tmp = mxmlNewElement(mon_ordi, "logincpt");
        mxmlNewOpaque(tmp, computers->login);

        tmp = mxmlNewElement(mon_ordi, "pathprom");
        mxmlNewOpaque(tmp, computers->path);

        tmp = mxmlNewElement(mon_ordi, "cpu");
        mxmlNewOpaque(tmp, computers->cpu);

        tmp = mxmlNewElement(mon_ordi, "infoscpt");
        mxmlNewOpaque(tmp, computers->informations);

        computers = computers->next;
      }
    }
  }

  /* Enregistrement */
  fp = fopen(filename, "w");
  if (fp == NULL) EXIT_ON_ERROR("Impossible to open %s in mode w.", filename);

  fprintf(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  val = mxmlSaveFile(tree, fp, whitespace_cb);
  fclose(fp);

  return val;
}

int promnet_save_file_cpt(t_promnet *promnet, char *path)
{
  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_save_file_cpt : invalid promnet pointer\n");
    return -1;
  }

  saveComputerFile(promnet->tree_computer, path);
  promnet_set_cpt_path(promnet, path);
  return 0;
}

int promnet_set_net_path(t_promnet *promnet, char *path)
{
  int len = -1;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_set_net_path : invalid promnet pointer\n");
    return -1;
  }
  if (path == NULL)
  {
    fprintf(stderr, "promnet_set_net_path : invalid path pointer\n");
    return -1;
  }
  len = strlen(path);
  if ((len >= 0) && (len < NET_PATH_MAX))
  {
    memcpy(promnet->net_path, path, len + 1);
    return 0;
  }
  return -1;
}

int promnet_set_ivybus_envvar(t_promnet *promnet, char *ivybus_envvar)
{
  int len = -1;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_set_net_path : invalid promnet pointer\n");
    return -1;
  }
  if (ivybus_envvar == NULL)
  {
    fprintf(stderr, "promnet_set_net_path : invalid ivybus_envar pointer\n");
    return -1;
  }
  len = strlen(ivybus_envvar);
  if ((len >= 0) && (len < IVYBUS_ENVVAR_MAX))
  {
    memcpy(promnet->ivybus_envvar, ivybus_envvar, len + 1);
    return 0;
  }
  return -1;
}

int promnet_set_cpt_path(t_promnet *promnet, char *path)
{
  int len = -1;

  if (promnet == NULL)
  {
    fprintf(stderr, "promnet_set_cpt_path : invalid promnet pointer\n");
    return -1;
  }
  if (path == NULL)
  {
    fprintf(stderr, "promnet_set_cpt_path : invalid path pointer\n");
    return -1;
  }
  len = strlen(path);
  if ((len >= 0) && (len < CPT_PATH_MAX))
  {
    memcpy(promnet->cpt_path, path, len + 1);
    return 0;
  }
  return -1;
}


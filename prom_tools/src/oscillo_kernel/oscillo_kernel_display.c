/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 *      oscillo_kernel_display.c
 *
 *      Copyright 2010 philippe gaussier <pgaussier@wanadoo.fr>
 *      Modified: Arnaud Blanchard
 *      Date: February 2012
 *
 */

#include <sys/time.h>
#include <unistd.h>
#include <graphic_Tx.h>
#include <include/xml_tools.h>
#include <include/oscillo_kernel_display.h>

#define PHASE_OF_RECEIVE_TOKEN 0
#define PHASE_OF_ACTIVATION 1
#define PHASE_OF_EXECUTION 2
#define PHASE_OF_END_ALGO 3
#define PHASE_OF_END_RTTOKEN_AFTER_SEM_POST 4
#define PHASE_OF_END_RTTOKEN_PROPAGATION 5

/* dimension de la zone de dessin */
#define DA_WIDTH 2000
#define DA_HEIGHT 20

#define PROFILER_MAX 64

GtkWidget *window = NULL;
GtkWidget *vbox;
GtkWidget *box_of_legend;
GtkWidget *sw;
GtkWidget *vbox2;
GtkWidget *box3;
GtkWidget *box4;
GtkWidget *box_entree;
GtkWidget *box_ascenseur;
GtkWidget *label;
GtkWidget *hscale;
GtkWidget *button;
GtkAdjustment *adj0;
GtkWidget *hide_see_uncheck_groups_label;
char valeur_periode[255];
gboolean update_visible_groups = TRUE;

TxDonneesFenetre fenetre_oscillo_kernel;

GtkWidget *entry_periode_reset_oscillo_kernel;

int oscillo_kernel_activated = 0;
int oscillo_kernel_stop_asked = 0;
int demande_reset_oscillo_kernel = 1;
float time_gain;

long temps_old;
long last_reset_oscillo_kernel = 0; /* variable globale pour la synchro de l'affichage / pas plus d'un reset par periode globale de l'oscillo kernel */
int hauteur_display_gpe = 23;
int x_offset_display_gpe = 20;
int y_offset_display_gpe = 20;
unsigned long periode_reset_oscillo_kernel = 1000000; /* 1 secondes */
unsigned long pas_temps_oscillo_kernel;
int longueur_affichage_oscillo_kernel = 2000;
int recent_reset_affichage = 0; /*pour gestion par oscillo_distant multi RNs: reaffichage de tous les RNs */

type_profiler *profilers[PROFILER_MAX];
int number_of_profilers = 0;

static void profilers_destroy()
{
  int i, gpe;
  type_profiler *profiler;

  for (i = 0; i < number_of_profilers; i++)
  {
    profiler = profilers[i];
    for (gpe = 0; gpe < profiler->number_of_groups; gpe++)
    {
      free(profiler->profiler_groups[gpe]);
    }
    free(profiler);
  }
  number_of_profilers = 0;
}


void show_oscillo_kernel(void *data)
{
  (void) data;
  gtk_widget_hide(window);
}



void group_profiler_refresh(type_group_profiler *group_profiler)
{
  sprintf(group_profiler->text_of_period, "%f", group_profiler->period);
  gtk_label_set_text(GTK_LABEL(group_profiler->widget_of_period), group_profiler->text_of_period);
}

/*
gboolean expose_event_callback(GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
  cairo_t *cr;
  (void) widget;
  (void) data;

  cr = gdk_cairo_create(event->window);
  cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
  cairo_paint(cr);
  cairo_destroy(cr);
  return TRUE;
}*/

void reset_affichage(type_profiler *profiler)
{
  cairo_t *cr;
  int i;

  type_group_profiler *group_profiler;

  for (i = 0; i < profiler->number_of_groups; i++)
  {
    group_profiler = profiler->profiler_groups[i];
    group_profiler->size_of_sample = 0; /* reset of the average of the period */

    group_profiler_refresh(group_profiler);

    cr = gdk_cairo_create(group_profiler->drawing_area->window);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_paint(cr);
    cairo_destroy(cr);

    gdk_window_process_updates(group_profiler->drawing_area->window, TRUE);
  }
}

/* les phases de la mise a jour du rttoken : */
/* 0 (blanc) : rttoken active */
/* 1 (bleu/jaune) : fin du reset propagation rtttoken */
/* 2 (rouge) : fin de la mise a jour du rttoken apres le sem_post(&attend_un_rt_token) */
/* 3 idem 1 mais pour RN */
/* 4 inconnu */

void display_oscillo_kernel(type_group_profiler *group_profiler, int gpe, int phase, unsigned long temps)
{
  cairo_t *cr;
  GdkColor *color;
  int i, longueur;
  TxPoint point, point2;

  type_profiler *profiler = group_profiler->profiler;

  if (demande_reset_oscillo_kernel == 1)
  {

    if (oscillo_kernel_stop_asked)
    {
      oscillo_kernel_stop_asked = 0;
      oscillo_kernel_activated = 0;
    }
    else for (i = 0; i < number_of_profilers; i++)
    {
      reset_affichage(profilers[i]);
    }

    demande_reset_oscillo_kernel = 0;
    last_reset_oscillo_kernel = temps;
  }

  if ((temps - last_reset_oscillo_kernel) > periode_reset_oscillo_kernel)
  {
    demande_reset_oscillo_kernel = 1;
  }

  point2.x = (int) ((float) (temps - last_reset_oscillo_kernel) * time_gain);

  if (phase == PHASE_OF_END_ALGO) point.y = 5 * PHASE_OF_ACTIVATION;
  else point.y = 5 * phase;

  point2.y = point.y;

  switch (phase)
  {
  case PHASE_OF_RECEIVE_TOKEN:
    color = &couleurs[blanc];
    break; /*  token ou rttoken active */
  case PHASE_OF_ACTIVATION:
    color = &couleurs[ciel];
    break; /*  fin du reset propagation rtttoken */
  case PHASE_OF_END_ALGO:
    color = &couleurs[vert];
    break; /*  idem 1 mais pour fin RN */
  case PHASE_OF_END_RTTOKEN_AFTER_SEM_POST:
    color = &couleurs[rouge];
    break;
  case PHASE_OF_END_RTTOKEN_PROPAGATION:
    color = &couleurs[jaune];
    break;
  default:
    color = &couleurs[violet]; /* couleur = violet; */
    break;
  }

  cr = gdk_cairo_create(group_profiler->drawing_area->window);
  gdk_cairo_set_source_color(cr, color);

  cairo_rectangle(cr, point2.x, point2.y, 1, 6);
  cairo_fill(cr);

  if (phase == PHASE_OF_RECEIVE_TOKEN) group_profiler->time_of_last_received_token = temps;
  else if (phase == PHASE_OF_ACTIVATION || phase == PHASE_OF_END_ALGO)
  {
    point.x = (int) ((float) (group_profiler->time_of_last_received_token - last_reset_oscillo_kernel)) * time_gain;
    longueur = point2.x - point.x;

    if (longueur < 1)
    {
      longueur = 1;
      point.x = 0;
    }

    if (profiler->profiler_groups[gpe]->type == No_RTTOKEN) gdk_cairo_set_source_color(cr, &couleurs[jaune]);

    cairo_rectangle(cr, point.x, point.y, longueur, 3);
    cairo_fill(cr);
  }

  gdk_window_process_updates(group_profiler->drawing_area->window, FALSE);
  cairo_destroy(cr);
}

void group_profiler_update_info(type_profiler *profiler, int gpe, int phase, long temps)
{
  float eta;
  long group_period;
  type_group_profiler *group_profiler;

  gdk_threads_enter();
  if (oscillo_kernel_stop_asked)
  {
    gtk_widget_destroy(window);
    oscillo_kernel_activated = 0;
    profilers_destroy();
  }
  else if (oscillo_kernel_activated)
  {
    group_profiler = profiler->profiler_groups[gpe];

    if (phase == PHASE_OF_RECEIVE_TOKEN)
    {

      group_profiler->size_of_sample = group_profiler->size_of_sample + 1;

      eta = 1 / group_profiler->size_of_sample;
      if (group_profiler->time_of_last_received_token != 0) group_period = temps - group_profiler->time_of_last_received_token;
      else group_period = 0;
      group_profiler->period = group_profiler->period + eta * (group_period / 1000.0 - group_profiler->period);
    }
    display_oscillo_kernel(group_profiler, gpe, phase, temps);
  }
  gdk_threads_leave();

}

void add_legend(GtkWidget *box_of_legend, const char *content, int index_of_color)
{
  GtkWidget *label_of_legend;

  label_of_legend = gtk_label_new(content);
  gtk_widget_set_size_request(label_of_legend, 250, -1);
  gtk_widget_modify_fg(label_of_legend, GTK_STATE_NORMAL, &couleurs[index_of_color]);
  gtk_box_pack_start(GTK_BOX(box_of_legend), label_of_legend, FALSE, TRUE, 0);
}

Node* oscillo_kernel_get_xml_informations(Node *tree)
{
  int i, j;
  Node *oscillo = mxmlNewElement(tree, "oscillo_kernel");

  if (number_of_profilers != 0)
  {
    xml_set_int(oscillo, "activate", 1);
    for (i = 0; i < number_of_profilers; i++)
    {
      Node *script = mxmlNewElement(oscillo, "script_oscillo");
      xml_set_string(script, "name", profilers[i]->logical_name);
      for (j = 0; j < profilers[i]->number_of_groups; j++)
      {
        Node *group = mxmlNewElement(script, "group");
        xml_set_string(group, "name", profilers[i]->profiler_groups[j]->name);
        xml_set_int(group, "check", gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(profilers[i]->profiler_groups[j]->check_button)));
      }
    }
  }
  else xml_set_int(oscillo, "activate", 0);

  return tree;
}

void select_all_check(GtkWidget *select_all_button, type_profiler *profiler)
{
  int i;

  (void) select_all_button;

  for (i = 0; i < profiler->number_of_groups; i++)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(profiler->profiler_groups[i]->check_button), TRUE);
}

/*gere l'activation du bouton see all groups button */
void on_hide_see_uncheck_groups_activate(GtkWidget *hide_see_uncheck_groups, type_profiler *profiler)
{
  int i;

  if (update_visible_groups)
  {
    for (i = 0; i < profiler->number_of_groups; i++)
    {
      if (gtk_widget_get_visible(profiler->profiler_groups[i]->widget)) gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(profiler->profiler_groups[i]->check_button), TRUE);
      else gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(profiler->profiler_groups[i]->check_button), FALSE);

      gtk_widget_show(profiler->profiler_groups[i]->widget);
    }

    gtk_button_set_label(GTK_BUTTON(hide_see_uncheck_groups), "Hide uncheck groups");
    update_visible_groups = FALSE;
  }
  else
  {
    for (i = 0; i < profiler->number_of_groups; i++)
    {
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(profiler->profiler_groups[i]->check_button))) gtk_widget_show(profiler->profiler_groups[i]->widget);
      else gtk_widget_hide(profiler->profiler_groups[i]->widget);
    }
    gtk_button_set_label(GTK_BUTTON(hide_see_uncheck_groups), "See all groups");
    update_visible_groups = TRUE;
  }
}

void on_check_button_promethe_activate(GtkWidget *check_button, type_profiler *profiler)
{
  gboolean etat = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_button));

  if (etat)
  {
    gtk_widget_show(profiler->widget);
  }
  else
  {
    /*oscillo_kernel_stop((void*)profiler);*/
    gtk_widget_hide(profiler->widget);
  }
}

/*gere l'activation ou non du boutton à cocher */
void toggle_check(GtkWidget *check_button, type_group_profiler *group_profiler)
{
  gboolean activated = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(group_profiler->check_button));

  (void) check_button;

  if (!activated)
  {
    gtk_widget_hide(group_profiler->widget);
  }
}

int destroy_fenetre_oscillo_kernel()
{
  oscillo_kernel_stop_asked = 1;
  return TRUE;
}

void arrete_oscillo_kernel(GtkWidget * widget)
{
  (void) widget;

  if (oscillo_kernel_activated == 1) /* arret de l'oscillo dans le cas du promethe normal */
  {
    oscillo_kernel_stop_asked = 1;
  }
}

void change_periode_oscillo_kernel(GtkWidget * widget, GtkWidget * entry)
{
  const gchar *entry_text;
  (void) widget;

  entry_text = gtk_entry_get_text(GTK_ENTRY(entry));

  periode_reset_oscillo_kernel = atol(entry_text) * 1000;
  gtk_range_set_value(GTK_RANGE(hscale), (double) periode_reset_oscillo_kernel / 10000);
  time_gain = ((float) longueur_affichage_oscillo_kernel) / periode_reset_oscillo_kernel;
  demande_reset_oscillo_kernel = 1;
}

void periode_oscillo_kernel_get_value(GtkWidget * widget, int *val)
{
  char valeur_periode[255];
  *val = gtk_range_get_value(GTK_RANGE(widget));
  periode_reset_oscillo_kernel = (*val) * 1000;

  sprintf(valeur_periode, "%d", (int) *val / 1000);
  gtk_entry_set_text(GTK_ENTRY(entry_periode_reset_oscillo_kernel), valeur_periode);
  time_gain = ((float) longueur_affichage_oscillo_kernel) / periode_reset_oscillo_kernel;

  demande_reset_oscillo_kernel = 1;
}

/* Button oscillo kernel */
void oscillo_kernel_pressed()
{
  if (oscillo_kernel_activated == 1)
  {
    oscillo_kernel_activated = 0;
  }
  else
  {
    oscillo_kernel_activated = 1;
  }
}

void oscillo_kernel_create_window()
{

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_size_request(window, -1, 400);
  gtk_window_set_title(GTK_WINDOW(window), "Oscillo kernel");

  box_of_legend = gtk_hbox_new(FALSE, 1);
  add_legend(box_of_legend, "start activation", blanc);
  add_legend(box_of_legend, "execution", ciel);
  add_legend(box_of_legend, "end algo group", vert);
  add_legend(box_of_legend, "end update rt-token after sem_post", rouge);
  add_legend(box_of_legend, "end reset rt-token propagation", jaune);

  vbox = gtk_vbox_new(FALSE, 1);
  gtk_box_pack_start(GTK_BOX(vbox), box_of_legend, FALSE, TRUE, 0);
  gtk_container_add(GTK_CONTAINER(window), vbox);

  sw = gtk_scrolled_window_new(NULL, NULL );

  vbox2 = gtk_vbox_new(FALSE, 1);
  gtk_box_pack_start(GTK_BOX(vbox), sw, TRUE, TRUE, 0);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sw), vbox2);

  box3 = gtk_hbox_new(FALSE, 1);
  gtk_container_set_border_width(GTK_CONTAINER(box3), 10);
  gtk_box_pack_start(GTK_BOX(vbox), box3, FALSE, TRUE, 0);

  button = gtk_button_new_with_label("Oscillo Kernel");
  g_signal_connect(GTK_OBJECT(button), "clicked", G_CALLBACK(oscillo_kernel_pressed), window);

  gtk_box_pack_start(GTK_BOX(box3), button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(button);

  box_entree = gtk_vbox_new(FALSE, 1);
  gtk_container_add(GTK_CONTAINER(box3), box_entree);
  gtk_widget_set_size_request(box_entree, 100, 20);

  entry_periode_reset_oscillo_kernel = gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(entry_periode_reset_oscillo_kernel), 30);
  g_signal_connect(G_OBJECT(entry_periode_reset_oscillo_kernel), "activate", G_CALLBACK(change_periode_oscillo_kernel), (gpointer) entry_periode_reset_oscillo_kernel);

  sprintf(valeur_periode, "%ld", periode_reset_oscillo_kernel / 1000);
  gtk_entry_set_text(GTK_ENTRY(entry_periode_reset_oscillo_kernel), valeur_periode);

  gtk_editable_select_region(GTK_EDITABLE(entry_periode_reset_oscillo_kernel), 0, GTK_ENTRY(entry_periode_reset_oscillo_kernel)->text_length);
  gtk_box_pack_start(GTK_BOX(box_entree), entry_periode_reset_oscillo_kernel, TRUE, TRUE, 0);

  box_ascenseur = gtk_vbox_new(FALSE, 1);
  label = gtk_label_new("time cst");
  gtk_container_add(GTK_CONTAINER(box3), box_ascenseur);
  gtk_widget_set_size_request(box_ascenseur, 300, 55);

  box4 = gtk_vbox_new(FALSE, 3);
  gtk_container_set_border_width(GTK_CONTAINER(box4), 10);
  gtk_box_pack_start(GTK_BOX(box_ascenseur), box4, TRUE, TRUE, 0);

  hscale = gtk_hscale_new_with_range(1, 1000, 10);
  gtk_range_set_value(GTK_RANGE(hscale), periode_reset_oscillo_kernel / 1000);

  gtk_widget_set_usize(GTK_WIDGET(hscale), 200, 30);
  g_signal_connect(GTK_OBJECT(hscale), "value_changed", G_CALLBACK(periode_oscillo_kernel_get_value), &periode_reset_oscillo_kernel);

  gtk_box_pack_start(GTK_BOX(box4), hscale, TRUE, TRUE, 0);
  g_signal_connect(GTK_OBJECT(window), "delete-event", G_CALLBACK(destroy_fenetre_oscillo_kernel), NULL );

  gtk_widget_show_all(window);

  time_gain = ((float) longueur_affichage_oscillo_kernel) / periode_reset_oscillo_kernel;

  oscillo_kernel_stop_asked = 0;
  demande_reset_oscillo_kernel = 0;
}

type_profiler *oscillo_kernel_add_promethe(type_com_groupe *d_groupe, int number_of_groups, const char *name_promethe, char *file_preferences)
{
  GtkWidget *box_of_group, *label_of_group;
  GtkWidget *box_of_group_profilers;
  GtkWidget *check_button;
  GtkWidget *button_box, *hide_see_uncheck_groups, *select_all;
  Node *tree_preferences, *script = NULL, *group;

  int i;

  type_profiler *profiler;
  type_group_profiler *group_profiler;

  button_box = gtk_hbox_new(FALSE, 1);
  hide_see_uncheck_groups = gtk_toggle_button_new_with_label("See all groups");
  select_all = gtk_toggle_button_new_with_label("Select all groups");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(hide_see_uncheck_groups), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(select_all), TRUE);
  gtk_widget_set_size_request(hide_see_uncheck_groups, 200, -1);
  gtk_widget_set_size_request(select_all, 200, -1);
  gtk_box_pack_start(GTK_BOX(button_box), hide_see_uncheck_groups, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(button_box), select_all, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(vbox2), button_box, FALSE, FALSE, 0);

  box_of_group_profilers = gtk_vbox_new(FALSE, 1);

  profiler = ALLOCATION(type_profiler);
  profiler->button_box = button_box;
  profiler->time_offset = 0;
  profiler->period = periode_reset_oscillo_kernel; /* 1s */
  profiler->number_of_groups = number_of_groups;
  strcpy(profiler->logical_name, name_promethe);

  for (i = 0; i < number_of_groups; i++)
  {
    group_profiler = ALLOCATION(type_group_profiler);
    group_profiler->profiler = profiler;
    group_profiler->time_of_last_received_token = 0;
    group_profiler->number_of_historical_events = 0;
    group_profiler->period = 0.;
    group_profiler->type = d_groupe[i].type;
    strcpy(group_profiler->name, d_groupe[i].nom);

    check_button = gtk_check_button_new_with_label(d_groupe[i].nom);
    group_profiler->check_button = check_button;

    box_of_group = gtk_hbox_new(FALSE, 1);

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_button), TRUE);

    gtk_widget_set_size_request(check_button, 75, -1);
    gtk_box_pack_start(GTK_BOX(box_of_group), check_button, FALSE, TRUE, 0);

    label_of_group = gtk_label_new(d_groupe[i].no_name);
    gtk_widget_set_size_request(label_of_group, 50, -1);
    gtk_box_pack_start(GTK_BOX(box_of_group), label_of_group, FALSE, FALSE, 0);
    group_profiler->widget = box_of_group;

    group_profiler->drawing_area = gtk_drawing_area_new();

    gtk_widget_set_size_request(group_profiler->drawing_area, DA_WIDTH, DA_HEIGHT);
    gtk_box_pack_start(GTK_BOX(box_of_group), group_profiler->drawing_area, TRUE, TRUE, 0);

    group_profiler->widget_of_period = gtk_label_new("period");
    gtk_widget_set_size_request(group_profiler->widget_of_period, 100, -1);
    gtk_box_pack_start(GTK_BOX(box_of_group), group_profiler->widget_of_period, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(box_of_group_profilers), box_of_group, FALSE, FALSE, 0);

    profiler->profiler_groups[i] = group_profiler;
    profiler->widget = box_of_group_profilers;

    /* ecoute si l'on cache ou non l'un des groupes */
    g_signal_connect(check_button, "toggled", G_CALLBACK(toggle_check), group_profiler);
  }

  gtk_box_pack_start(GTK_BOX(vbox2), box_of_group_profilers, FALSE, FALSE, 0);
  g_signal_connect(hide_see_uncheck_groups, "toggled", G_CALLBACK(on_hide_see_uncheck_groups_activate), profiler);
  g_signal_connect(select_all, "toggled", G_CALLBACK(select_all_check), profiler);

  gtk_widget_show_all(button_box);
  gtk_widget_show_all(box_of_group_profilers);

  if (file_preferences != NULL && file_preferences[0] != 0)
  {
    tree_preferences = xml_load_file(file_preferences);
    tree_preferences = xml_get_first_child_with_node_name(tree_preferences, "oscillo_kernel");
    if (xml_get_int(tree_preferences, "activate") == 1)
    {
      script = xml_get_first_child_with_node_name(tree_preferences, "script_oscillo");

      if (strcmp(xml_get_string(script, "name"), profiler->logical_name))
      {
        script = NULL;

        for (i = 1; i < xml_get_number_of_childs(tree_preferences); i++)
        {
          script = xml_get_next_homonymous_sibling(xml_get_first_child_with_node_name(tree_preferences, "script_oscillo"));

          if (strcmp(xml_get_string(script, "name"), profiler->logical_name)) script = NULL;
          else break;
        }
      }
    }
  }

  if (script != NULL )
  {
    group = xml_get_first_child_with_node_name(script, "group");
    if (xml_get_int(group, "check") == 0)
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(profiler->profiler_groups[0]->check_button), FALSE);
      gtk_widget_hide(profiler->profiler_groups[0]->widget);
    }

    for (i = 1; i < xml_get_number_of_childs(script); i++)
    {
      group = xml_get_next_homonymous_sibling(group);
      if (xml_get_int(group, "check") == 0)
      {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(profiler->profiler_groups[i]->check_button), FALSE);
        gtk_widget_hide(profiler->profiler_groups[i]->widget);
      }
    }
  }

  profilers[number_of_profilers] = profiler;
  number_of_profilers++;

  return profiler;
}


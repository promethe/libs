/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <math.h>

#define PI 3.1415926535

int main (int argv, char *argc[])
{
  float a, a1, a2, b, c1, c2, borne_inf, borne_sup, pas, x, dog;
  int nb_echt;
  
  if(argv < 8)
  {
    printf("erreur dans le nombre d'argument\n");
    printf("l'equation de la d.o.g. est:\n");
    printf("a*[1/(c*sqrt(2*Pi))*exp((x-b)²/(2c²))\n");
    printf("   - 1/(c'*sqrt(2*Pi))*exp((x-b)²/(2c'²))]\n");
    printf("vous devez specifier les valeurs a, b, c, c' et l'intervalle de calcul\n");
    printf(" ainsi que le nombre d'echantions desires\n");
    printf("ex: dog 1 0 1 2 -3 3 11\n\n");
    return 1;
  }
  
  
  if(sscanf(argc[1], "%f", &a) != 1)
  {
    printf("erreur: impossible d'interpreter le premier argument '%s'\n", argc[1]);
    return 1;
  }
  if(sscanf(argc[2], "%f", &b) != 1)
  {
    printf("erreur: impossible d'interpreter le deuxieme argument '%s'\n", argc[2]);
    return 1;
  }
  if(sscanf(argc[3], "%f", &c1) != 1)
  {
    printf("erreur: impossible d'interpreter le troisieme argument '%s'\n", argc[3]);
    return 1;
  }
  if(sscanf(argc[4], "%f", &c2) != 1)
  {
    printf("erreur: impossible d'interpreter le quatrieme argument '%s'\n", argc[3]);
    return 1;
  }
  if(sscanf(argc[5], "%f", &borne_inf) != 1)
  {
    printf("erreur: impossible d'interpreter le cinquieme argument '%s'\n", argc[4]);
    return 1;
  }
  if(sscanf(argc[6], "%f", &borne_sup) != 1)
  {
    printf("erreur: impossible d'interpreter le sixieme argument '%s'\n", argc[5]);
    return 1;
  }
  if(sscanf(argc[7], "%i", &nb_echt) != 1)
  {
    printf("erreur: impossible d'interpreter le septieme argument '%s'\n", argc[6]);
    return 1;
  }
  
  
 
	a1 = 1 / (c1 * sqrt(2.0 * PI));
	a2 = 1 / (c2 * sqrt(2.0 * PI));

  pas = (borne_sup - borne_inf) / (nb_echt - 1);

  /*printf("a1=%f, a2=%f, b=%f, c=%f, c'=%f, borne inf=%f, borne sup=%f, pas=%f\n", a1, a2, b, c1, c2, borne_inf, borne_sup, pas);*/
  
  for(x=borne_inf; nb_echt>0; x+=pas, nb_echt--)
  {
    dog = a1 * expf(-(x-b)*(x-b) / (2.0 * c1 * c1)) - a2 * expf(-(x-b)*(x-b) / (2.0 * c2 * c2));
    dog *= a;
    printf("%f ", dog);
  }
  
  return 0;
}

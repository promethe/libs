/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "basic_tools.h"

#include "libparserxml_MetaLeto.h"

#include "xml_tools.h"

/* Methode interne utilisee pour recuperer la valeur d'un noeud */
char * getValeur(mxml_node_t * mxml_node, mxml_node_t * tree, const char * arg)
{
  mxml_node_t * child = NULL, *node = NULL;

  if (mxml_node == NULL || tree == NULL)
  {
    fprintf(stderr, "Error %s for field %s\n", __FUNCTION__, arg);
    exit(1);
  }

  if ((node = mxmlFindElement(mxml_node, tree, arg, NULL, NULL, MXML_DESCEND)) == NULL)
  {
    /*fprintf(stderr, "Warning in %s: no field %s: supposed empty\n", __FUNCTION__, arg); */
    return (char *) "";
  }

  child = node->child;

  if (child == NULL)
  {
    /*printf("le champ %s est vide\n",arg);
     fprintf(stderr,"%s \n",__FUNCTION__);*/
    return (char *) "";
  }

  return child->value.opaque;
}

/* ------ NODE ------ */
struct node * addNode(node * o)
{
  node * new_node = NULL, *current = NULL;

  if ((new_node = (node *) (malloc(sizeof(node)))) == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    exit(1);
  }
  memset(new_node, 0, sizeof(node));
  current = o;

  if (current == NULL)
  {
    current = new_node;
    current->next = NULL;
    current->first = current;

  }
  else
  {
    while (current->next != NULL)
    {
      current = current->next;
    }

    /* Gestion de la liste */
    new_node->first = current->first;
    new_node->next = NULL;

    current->next = new_node;
    current = new_node;

  }

  /* initialisation des champs */
  setNameNode(current, (char *) "");
  setComputer(current, (char *) "");
  setNodeField(current, &current->login, "");
  setCmd(current, (char *) "");
  setDeploy(current, (char *) "");
  setOptions(current, (char *) "");
  setNodeField(current, &current->symb, (char *) "");
  setNodeField(current, &current->script, (char *) "");
  setDraw(current, (char *) "");
  setRes(current, (char *) "");
  setConfig(current, (char *) "");
  setBus(current, (char *) "");
  setVar(current, (char *) "");
  setDev(current, (char *) "");
  setGcd(current, (char *) "");
  setPrt(current, (char *) "");

  /* Equivalent to setKeyboard mais plus generique */
  setNodeField(current, &current->keyboard_input, "");
  setNodeField(current, &current->synchronize_files, "");
  setNodeField(current, &current->synchronize_directories, "");

  setNodeField(current, &current->distant_directory, "");

  return current;
}

struct node * delNode(struct node * n)
{
  node * current = NULL;
  node * previous = NULL;

  if (n == NULL) EXIT_ON_ERROR("node a supprimer 'NULL' \n");

  /* Recherche de l'element precedent */
  current = n->first;
  while (current != NULL && current != n)
  {
    previous = current;
    current = current->next;
  }
  current = n;

  if (previous == NULL)
  {
    if (current->next == NULL)
    {/* un seul element dans la liste*/
      free(current);
      return NULL;

    }
    else
    { /* 1er element dans la liste*/
      node * new_first = current->next;
      free(current);
      current = new_first;

      /* on reassigne le pointeur sur le premier element de la liste pour chaque element*/
      while (current != NULL)
      {
        current->first = new_first;
        current = current->next;
      }

      return new_first;
    }
  }
  else
  {
    previous->next = current->next;
    return current->first;
  }

  fprintf(stderr, "Nom de node inconnu %s \n", __FUNCTION__);
  return NULL;
}

struct node * delNodeByName(struct node * n, char * name)
{
  node * current = NULL;
  node * previous = NULL;

  if (n == NULL || name == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  current = n->first;
  while (current != NULL)
  {
    if (strcmp(name, getNameNode(current)) == 0)
    {
      if (previous == NULL)
      {
        if (current->next == NULL)
        {/* un seul element dans la liste*/
          free(current);
          return NULL;

        }
        else
        { /* 1er element dans la liste*/
          node * new_first = current->next;
          free(current);
          current = new_first;

          /* on reassigne le pointeur sur le premier element de la liste pour chaque element*/
          while (current != NULL)
          {
            current->first = new_first;
            current = current->next;
          }

          return new_first;
        }
      }
      else
      {
        previous->next = current->next;
        return current->first;
      }
    }
    previous = current;
    current = current->next;
  }

  fprintf(stderr, "Nom de node inconnu %s \n", __FUNCTION__);
  return NULL;
}

void getIvy(char * dest, mxml_node_t * tree)
{
  mxml_node_t *mxml_node = NULL;

  mxml_node = mxmlFindElement(tree, tree, "network", NULL, NULL, MXML_DESCEND);
  strcpy(dest, getValeur(mxml_node, tree, "ivybus"));
}

struct node * getNetwork(mxml_node_t * tree)
{
  float version;

  /* Attention: Node correspond au noeud xml, et node au noeud specific de promethe */
  Node *mxml_node = NULL;
  Node *xml_node_application, *xml_node_symb, *xml_node_script;

  node * Current = NULL;
  node * Next = NULL;
  node * First = NULL;

  if (tree == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  xml_node_application = xml_get_first_child_with_node_name(tree, "application");
  if (xml_try_to_get_float(xml_node_application, "version", &version) == 0) version = 0;

  for (mxml_node = mxmlFindElement(tree, tree, "node", NULL, NULL, MXML_DESCEND); mxml_node != NULL; mxml_node = mxmlFindElement(mxml_node, tree, "node", NULL, NULL, MXML_DESCEND))
  {
    if (version < 1.0)
    {
      xml_node_script = xml_get_first_child_with_node_name(mxml_node, "script_non_symb");
      xml_node_symb = xml_get_first_child_with_node_name(mxml_node, "script");
      xml_set_node_name(xml_node_script, "script");
      xml_set_node_name(xml_node_symb, "symb");
    }

    if ((Next = (node *) (malloc(sizeof(node)))) == NULL)
    {
      fprintf(stderr, "%s \n", __FUNCTION__);
      exit(1);
    }
    memset(Next, 0, sizeof(node));
    if (Current == NULL) First = Next;

    Next->next = NULL;
    Next->first = First;

    if (Current != NULL) Current->next = Next;

    Current = Next;

    setNameNode(Current, getValeur(mxml_node, tree, "name"));
    setComputer(Current, getValeur(mxml_node, tree, "computer"));
    setNodeField(Current, &Current->login, getValeur(mxml_node, tree, "login"));
    setCmd(Current, getValeur(mxml_node, tree, "cmd"));
    setDeploy(Current, getValeur(mxml_node, tree, "deploy"));
    setOptions(Current, getValeur(mxml_node, tree, "options"));

    setNodeField(Current, &Current->symb, getValeur(mxml_node, tree, "symb"));
    setNodeField(Current, &Current->script, getValeur(mxml_node, tree, "script"));

    setDraw(Current, getValeur(mxml_node, tree, "draw"));
    setRes(Current, getValeur(mxml_node, tree, "res"));
    setConfig(Current, getValeur(mxml_node, tree, "config"));
    setBus(Current, getValeur(mxml_node, tree, "bus"));
    setVar(Current, getValeur(mxml_node, tree, "var"));
    setDev(Current, getValeur(mxml_node, tree, "dev"));
    setGcd(Current, getValeur(mxml_node, tree, "gcd"));
    setPrt(Current, getValeur(mxml_node, tree, "prt"));

    setNodeField(Current, &Current->keyboard_input, getValeur(mxml_node, tree, "keyboard"));
    setNodeField(Current, &Current->synchronize_files, getValeur(mxml_node, tree, "synchronize_files"));
    setNodeField(Current, &Current->synchronize_directories, getValeur(mxml_node, tree, "synchronize_directories"));

    setNodeField(Current, &Current->distant_directory, getValeur(mxml_node, tree, "distant_directory"));

    if (!xml_try_to_get_int(mxml_node, "overwrite_res", &Current->overwrite_res)) Current->overwrite_res = 0;
  }

  return First;

}

struct node * getNextNode(struct node * n)
{

  if (n == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  return n->next;
}

struct node * getNodeByName(struct node * n, char * name)
{

  node * current = NULL;

  if (n == NULL || name == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  current = n->first;
  while (current != NULL)
  {
    /* printf("%s - %s\n",name,getNameNode(n)); */
    if (strcmp(name, getNameNode(current)) == 0)
    {
      return current;
    }
    current = current->next;
  }

  fprintf(stderr, "%s \n", __FUNCTION__);
  return NULL;
}

char * getNameNode(struct node * n)
{
  char *resultat = NULL;
  if (n == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(n->name)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}

char *getNodeField(struct node *node, char *field)
{
  char *resultat = NULL;
  if (node == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(field)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}

int setNameNode(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->name = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setComputer(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->computer = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setCmd(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->cmd = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setDeploy(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->deploy = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setOptions(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->options = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}

int setDraw(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->draw = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setRes(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->res = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setConfig(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->config = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setBus(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->bus = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setVar(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->var = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setDev(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->dev = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setGcd(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->gcd = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}

int setPrt(node * n, char * c)
{
  if (n == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((n->prt = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}

int setNodeField(node *node, char **field, const char *value)
{
  if (node == NULL || value == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((*field = strdup(value)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }
  return 0;
}

/* ------ LINK ------ */
struct link_metaleto * addLink(link_metaleto * o)
{
  link_metaleto * new_link = NULL;
  link_metaleto * current = NULL;

  current = o;
  if ((new_link = (link_metaleto *) (malloc(sizeof(link_metaleto)))) == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    exit(1);
  }
  memset(new_link, 0, sizeof(link_metaleto));
  if (current == NULL)
  {
    current = new_link;
    current->next = NULL;
    current->first = current;
  }
  else
  {
    while (current->next != NULL)
    {
      current = current->next;
    }

    /* Gestion de la liste */
    new_link->first = current->first;
    new_link->next = NULL;

    current->next = new_link;
    current = new_link;
  }

  /* initialisation des champs */
  setNameLink(current, (char *) "");
  setInput(current, (char *) "");
  setOutput(current, (char *) "");

  return current;
}

struct link_metaleto * delLink(struct link_metaleto * n)
{
  link_metaleto * current = NULL;
  link_metaleto * previous = NULL;

  if (n == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  /* Recherche de l'element precedent */
  current = n->first;
  while (current != NULL && current != n)
  {
    previous = current;
    current = current->next;
  }
  current = n;

  if (previous == NULL)
  {
    if (current->next == NULL)
    {/* un seul element dans la liste*/
      free(current);
      return NULL;

    }
    else
    { /* 1er element dans la liste*/
      link_metaleto * new_first = current->next;
      free(current);
      current = new_first;

      /* on reassigne le pointeur sur le premier element de la liste pour chaque element*/
      while (current != NULL)
      {
        current->first = new_first;
        current = current->next;
      }

      return new_first;
    }
  }
  else
  {
    previous->next = current->next;
    return current->first;
  }

  fprintf(stderr, "Nom de lien inconnu dans %s \n", __FUNCTION__);
  return NULL;
}

struct link_metaleto * delLinkByName(struct link_metaleto * l, char * name)
{
  link_metaleto * current = NULL;
  link_metaleto * previous = NULL;

  if (l == NULL || name == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  current = l->first;
  while (current != NULL)
  {
    if (strcmp(name, getNameLink(current)) == 0)
    {
      if (previous == NULL)
      {
        if (current->next == NULL)
        {/* un seul element dans la liste*/
          free(current);
          return NULL;

        }
        else
        { /* 1er element dans la liste*/
          link_metaleto * new_first = current->next;
          free(current);
          current = new_first;

          /* on reassigne le pointeur sur le premier element de la liste pour chaque element*/
          while (current != NULL)
          {
            current->first = new_first;
            current = current->next;
          }

          return new_first;
        }
      }
      else
      {
        previous->next = current->next;
        return current->first;
      }
    }
    previous = current;
    current = current->next;
  }

  fprintf(stderr, "Nom de node inconnu %s \n", __FUNCTION__);
  return NULL;
}

struct link_metaleto * getLinks(mxml_node_t * tree)
{
  mxml_node_t *mxml_node = NULL;

  link_metaleto * Current = NULL;
  link_metaleto * Next = NULL;
  link_metaleto * First = NULL;

  if (tree == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  for (mxml_node = mxmlFindElement(tree, tree, "link", NULL, NULL, MXML_DESCEND); mxml_node != NULL; mxml_node = mxmlFindElement(mxml_node, tree, "link", NULL, NULL, MXML_DESCEND))
  {

    if ((Next = (link_metaleto *) (malloc(sizeof(link_metaleto)))) == NULL)
    {
      fprintf(stderr, "%s \n", __FUNCTION__);
      exit(1);
    }
    memset(Next, 0, sizeof(link_metaleto));
    if (Current == NULL) First = Next;

    Next->next = NULL;
    Next->first = First;

    if (Current != NULL) Current->next = Next;

    Current = Next;

    setNameLink(Current, getValeur(mxml_node, tree, "name"));
    setInput(Current, getValeur(mxml_node, tree, "input"));
    setOutput(Current, getValeur(mxml_node, tree, "output"));

  }

  return First;
}
struct link_metaleto * getNextLink(link_metaleto * l)
{

  if (l == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }
  return l->next;
}
struct link_metaleto * getLinkByName(link_metaleto * l, char * name)
{
  link_metaleto * current = NULL;

  if (l == NULL || name == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  current = l->first;
  while (current != NULL)
  {
    if (strcmp(name, getNameLink(current)) == 0)
    {
      return current;
    }
    current = current->next;
  }

  fprintf(stderr, "Nom de lien inconnu %s \n", __FUNCTION__);
  return NULL;
}

char * getNameLink(link_metaleto * l)
{
  char *resultat = NULL;
  if (l == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(l->name)) == NULL)
  {
    fprintf(stderr, "strdup in  %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}
char * getInput(link_metaleto * l)
{
  char *resultat = NULL;
  if (l == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(l->input)) == NULL)
  {
    fprintf(stderr, "strdup in  %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}
char * getOutput(link_metaleto * l)
{
  char *resultat = NULL;
  if (l == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(l->output)) == NULL)
  {
    fprintf(stderr, "strdup in  %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}

int setNameLink(link_metaleto * l, char * c)
{
  if (l == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((l->name = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in  %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setInput(link_metaleto * l, char * c)
{
  if (l == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((l->input = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in  %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setOutput(link_metaleto * l, char * c)
{
  if (l == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((l->output = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in  %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}

/* ------ COMPUTER ------ */
struct computer * addComputer(computer * o)
{
  computer * new_computer = NULL;
  computer * current = NULL;

  if ((new_computer = (computer *) (malloc(sizeof(computer)))) == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    exit(1);
  }
  memset(new_computer, 0, sizeof(computer));
  current = o;

  if (current == NULL)
  {

    current = new_computer;
    current->next = NULL;
    current->first = current;
  }
  else
  {

    while (current->next != NULL)
    {
      current = current->next;
    }

    /* Gestion de la liste */
    new_computer->first = current->first;
    new_computer->next = NULL;

    current->next = new_computer;
    current = new_computer;
  }

  /* initialisation des champs */
  setNameComputer(current, (char *) "");
  setIp(current, (char *) "");
  setLogin(current, (char *) "");
  setPath(current, (char *) "");
  setCpu(current, (char *) "");
  setInformations(current, (char *) "");

  return current;
}

struct computer * delComputer(struct computer * n)
{
  computer * current = NULL;
  computer * previous = NULL;

  if (n == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  /* Recherche de l'element precedent */
  current = n->first;
  while (current != NULL && current != n)
  {
    previous = current;
    current = current->next;
  }
  current = n;

  if (previous == NULL)
  {
    if (current->next == NULL)
    {/* un seul element dans la liste*/
      free(current);
      return NULL;

    }
    else
    { /* 1er element dans la liste*/
      computer * new_first = current->next;
      free(current);
      current = new_first;

      /* on reassigne le pointeur sur le premier element de la liste pour chaque element*/
      while (current != NULL)
      {
        current->first = new_first;
        current = current->next;
      }

      return new_first;
    }
  }
  else
  {
    previous->next = current->next;
    return current->first;
  }

  fprintf(stderr, "Nom de computer inconnu %s \n", __FUNCTION__);
  return NULL;
}

struct computer * delComputerByName(struct computer * o, char * name)
{
  computer * current = NULL;
  computer * previous = NULL;

  if (o == NULL || name == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  current = o->first;
  while (current != NULL)
  {
    if (strcmp(name, getNameComputer(current)) == 0)
    {
      if (previous == NULL)
      {
        if (current->next == NULL)
        {/* un seul element dans la liste*/
          free(current);
          return NULL;

        }
        else
        { /* 1er element dans la liste*/
          computer * new_first = current->next;
          free(current);
          current = new_first;

          /* on reassigne le pointeur sur le premier element de la liste pour chaque element*/
          while (current != NULL)
          {
            current->first = new_first;
            current = current->next;
          }

          return new_first;
        }
      }
      else
      {
        previous->next = current->next;
        return current->first;
      }
    }
    previous = current;
    current = current->next;
  }

  fprintf(stderr, "Nom de node inconnu %s \n", __FUNCTION__);
  return NULL;
}

struct computer * getComputers(mxml_node_t * tree)
{
  mxml_node_t *mxml_node;

  computer * Current = NULL;
  computer * Next = NULL;
  computer * First = NULL;

  if (tree == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  for (mxml_node = mxmlFindElement(tree, tree, "computer", NULL, NULL, MXML_DESCEND); mxml_node != NULL;
      mxml_node = mxmlFindElement(mxml_node, tree, "computer", NULL, NULL, MXML_NO_DESCEND))
  {

    if ((Next = (computer *) (malloc(sizeof(computer)))) == NULL)
    {
      fprintf(stderr, "%s \n", __FUNCTION__);
      exit(1);
    }
    memset(Next, 0, sizeof(computer));
    if (Current == NULL) First = Next;

    Next->next = NULL;
    Next->first = First;

    if (Current != NULL) Current->next = Next;

    Current = Next;

    setNameComputer(Current, getValeur(mxml_node, tree, "name"));
    setIp(Current, getValeur(mxml_node, tree, "ip"));
    setLogin(Current, getValeur(mxml_node, tree, "login"));
    setPath(Current, getValeur(mxml_node, tree, "path"));
    setCpu(Current, getValeur(mxml_node, tree, "cpu"));
    setInformations(Current, getValeur(mxml_node, tree, "informations"));

  }

  return First;
}
struct computer * getNextComputer(computer * o)
{

  if (o == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  return o->next;
}
struct computer * getComputerByName(computer * o, char * name)
{
  computer * current = NULL;
  if (o == NULL || name == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  current = o->first;
  while (current != NULL)
  {
    if (strcmp(name, getNameComputer(current)) == 0)
    {
      return current;
    }
    current = current->next;
  }

  fprintf(stderr, "Nom de lien inconnu %s \n", __FUNCTION__);
  return NULL;
}

char * getNameComputer(computer * o)
{
  char *resultat = NULL;
  if (o == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(o->name)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}
char * getIp(computer * o)
{
  char *resultat = NULL;
  if (o == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(o->ip)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}
char * getLogin(computer * o)
{
  char *resultat = NULL;
  if (o == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(o->login)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}
char * getPath(computer * o)
{
  char *resultat = NULL;
  if (o == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(o->path)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}
char * getCpu(computer * o)
{
  char *resultat = NULL;
  if (o == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(o->cpu)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}
char * getInformations(computer * o)
{
  char *resultat = NULL;
  if (o == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  if ((resultat = strdup(o->informations)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return NULL;
  }

  return resultat;
}

int setNameComputer(computer * o, char * c)
{
  if (o == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((o->name = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setIp(computer * o, char * c)
{
  if (o == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((o->ip = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setLogin(computer * o, char * c)
{
  if (o == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((o->login = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setPath(computer * o, char * c)
{
  if (o == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((o->path = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setCpu(computer * o, char * c)
{
  if (o == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((o->cpu = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}
int setInformations(computer * o, char * c)
{
  if (o == NULL || c == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return -1;
  }

  if ((o->informations = strdup(c)) == NULL)
  {
    fprintf(stderr, "strdup in %s \n", __FUNCTION__);
    return 1;
  }

  return 0;
}


/*Computers from .net file  Ali K.*/

struct computer * getComputersNet(mxml_node_t * tree)
{
  mxml_node_t *mxml_node;

  computer * Current = NULL;
  computer * Next = NULL;
  computer * First = NULL;

  if (tree == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  for (mxml_node = mxmlFindElement(tree, tree, "ordi", NULL, NULL, MXML_DESCEND); mxml_node != NULL;
      mxml_node = mxmlFindElement(mxml_node, tree, "ordi", NULL, NULL, MXML_NO_DESCEND))
  {

    if ((Next = (computer *) (malloc(sizeof(computer)))) == NULL)
    {
      fprintf(stderr, "%s \n", __FUNCTION__);
      exit(1);
    }
    memset(Next, 0, sizeof(computer));
    if (Current == NULL) First = Next;

    Next->next = NULL;
    Next->first = First;

    if (Current != NULL) Current->next = Next;

    Current = Next;

    setNameComputer(Current, getValeur(mxml_node, tree, "namecpt"));
    setIp(Current, getValeur(mxml_node, tree, "ip"));
    setLogin(Current, getValeur(mxml_node, tree, "logincpt"));
    setPath(Current, getValeur(mxml_node, tree, "pathprom"));
    setCpu(Current, getValeur(mxml_node, tree, "cpu"));
    setInformations(Current, getValeur(mxml_node, tree, "infoscpt"));

  }

  return First;
}

/* ------ OTHERS ------ */
/*mxml_node_t * loadFile(char * filename) {

 FILE *fp;
 mxml_node_t *tree;

 if ( filename == NULL ) {
 fprintf(stderr,"%s \n",__FUNCTION__);
 return NULL;
 }

 fp = fopen(filename, "r");
 tree = mxmlLoadFile(NULL, fp, MXML_OPAQUE_CALLBACK); *//* "OPAQUE" pour avoir l'ensemble du texte contenu dans un noeud */
/*	fclose(fp);

 return tree;
 }*/

/*
 * 'whitespace_cb()' - Let the mxmlSaveFile() function know when to insert newlines and tabs...
 */
const char * whitespace_cb(mxml_node_t *node, int where)
{
  const char *name; /* Name of element */

  if (node == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  /*
   * We can conditionally break to a new line before or after any element.
   * These are just common HTML elements...
   */
  name = node->value.element.name;

  /*
   * Newline after close...
   */
  if (where == MXML_WS_AFTER_CLOSE)
  {
    return ("\n");
  }
  /* application - xml */
  if (!strcmp(name, "application") || !strcmp(name, "?xml"))
  {
    if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }
  /* network - links */
  else if (!strcmp(name, "network") || !strcmp(name, "link_list") || !strcmp(name, "computer_list"))
  {
    if (where == MXML_WS_BEFORE_OPEN || where == MXML_WS_BEFORE_CLOSE) return ("\t");
    else if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }
  /* ivybus */
  else if (!strcmp(name, "ivybus"))
  {
    //if (where == MXML_WS_BEFORE_OPEN) return ("\t\t");
    if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }
  /* node - link */
  else if (!strcmp(name, "node") || !strcmp(name, "link") || !strcmp(name, "ordi"))
  {
    if (where == MXML_WS_BEFORE_OPEN || where == MXML_WS_BEFORE_CLOSE) return ("\t\t");
    else if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }
  /* name - computer - cmd - options - deploy - input - output*/
  else if (!strcmp(name, "name") || !strcmp(name, "deploy") || !strcmp(name, "cmd") || !strcmp(name, "computer")
      || !strcmp(name, "options") || !strcmp(name, "input") || !strcmp(name, "output") || !strcmp(name, "login")
      || !strcmp(name, "distant_directory") || !strcmp(name, "keyboard") || !strcmp(name, "synchronize_files")
      || !strcmp(name, "synchronize_directories") || !strcmp(name, "ip") || !strcmp(name, "cpu")
      || !strcmp(name, "infoscpt") || !strcmp(name, "namecpt") || !strcmp(name, "pathprom") || !strcmp(name, "logincpt"))
  {
    if (where == MXML_WS_BEFORE_OPEN) return ("\t\t\t");
  }
  /* path */
  else if (!strcmp(name, "path"))
  {
    if (where == MXML_WS_BEFORE_OPEN || where == MXML_WS_BEFORE_CLOSE) return ("\t\t\t");
    else if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }
  /* script - res - config - dev - gcd - prt -bus - var */
  else if (!strcmp(name, "var") || !strcmp(name, "bus") || !strcmp(name, "script_non_symb") || !strcmp(name, "script") || !strcmp(name, "symb") || !strcmp(name, "draw") || !strcmp(name, "res") || !strcmp(name, "config") || !strcmp(name, "dev")
      || !strcmp(name, "gcd") || !strcmp(name, "prt"))
  {
    if (where == MXML_WS_BEFORE_OPEN) return ("\t\t\t\t");
  }

  /*
   * Return NULL for no added whitespace...
   */
  return (NULL);
}

const char * whitespace_cb_computer(mxml_node_t *node, int where)
{
  const char *name; /* Name of element */

  if (node == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  /*
   * We can conditionally break to a new line before or after any element.
   * These are just common HTML elements...
   */
  name = node->value.element.name;

  /*
   * Newline after close...
   */
  if (where == MXML_WS_AFTER_CLOSE)
  {
    return ("\n");
  }
  /* computer_list */
  if (!strcmp(name, "computer_list"))
  {
    if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }
  /* computer */
  else if (!strcmp(name, "computer"))
  {
    if (where == MXML_WS_BEFORE_OPEN || where == MXML_WS_BEFORE_CLOSE) return ("\t");
    else if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }
  /* name - ip - login - path - cpu - informations */
  else if (!strcmp(name, "name") || !strcmp(name, "ip") || !strcmp(name, "login") || !strcmp(name, "path") || !strcmp(name, "cpu") || !strcmp(name, "informations"))
  {
    if (where == MXML_WS_BEFORE_OPEN) return ("\t\t");
  }

  /*
   * Return NULL for no added whitespace...
   */
  return (NULL);
}

int saveNetFile(struct node * the_network, struct link_metaleto * the_links, char * filename, char * ivybus_envvar)
{

  mxml_node_t * tree = NULL, *net = NULL, *node = NULL, *my_links = NULL, *my_link = NULL;
  mxml_node_t * tmp = NULL;

  struct node * network = NULL;
  struct link_metaleto * links = NULL;

  FILE *fp;
  int val;

  if (filename == NULL || strlen(filename) == 0) return -1;

  tree = mxmlNewElement(MXML_NO_PARENT, "application");
  xml_set_float(tree, "version", 1.0);

  /*
   * Network
   */
  if (tree != NULL)
  {
    net = mxmlNewElement(tree, "network");
    tmp = mxmlNewElement(net, "ivybus");
    mxmlNewOpaque(tmp, ivybus_envvar);

    if (the_network != NULL)
    {
      network = the_network->first;
      while (network != NULL)
      {
        node = mxmlNewElement(net, "node");

        xml_set_int(node, "overwrite_res", network->overwrite_res);

        tmp = mxmlNewElement(node, "name");
        mxmlNewOpaque(tmp, network->name);

        tmp = mxmlNewElement(node, "computer");
        mxmlNewOpaque(tmp, network->computer);

        tmp = mxmlNewElement(node, "login");
        mxmlNewOpaque(tmp, network->login);

        tmp = mxmlNewElement(node, "cmd");
        mxmlNewOpaque(tmp, network->cmd);

        tmp = mxmlNewElement(node, "deploy");
        mxmlNewOpaque(tmp, network->deploy);

        tmp = mxmlNewElement(node, "distant_directory");
        mxmlNewOpaque(tmp, network->distant_directory);

        tmp = mxmlNewElement(node, "keyboard");
        mxmlNewOpaque(tmp, network->keyboard_input);

        tmp = mxmlNewElement(node, "synchronize_files");
        mxmlNewOpaque(tmp, network->synchronize_files);

        tmp = mxmlNewElement(node, "synchronize_directories");
        mxmlNewOpaque(tmp, network->synchronize_directories);

        tmp = mxmlNewElement(node, "options");
        mxmlNewOpaque(tmp, network->options);

        node = mxmlNewElement(node, "path");

        tmp = mxmlNewElement(node, "script");
        mxmlNewOpaque(tmp, network->script);

        tmp = mxmlNewElement(node, "symb");
        mxmlNewOpaque(tmp, network->symb);

        tmp = mxmlNewElement(node, "draw");
        mxmlNewOpaque(tmp, network->draw);

        tmp = mxmlNewElement(node, "res");
        mxmlNewOpaque(tmp, network->res);

        tmp = mxmlNewElement(node, "config");
        mxmlNewOpaque(tmp, network->config);

        tmp = mxmlNewElement(node, "bus");
        mxmlNewOpaque(tmp, network->bus);

        tmp = mxmlNewElement(node, "var");
        mxmlNewOpaque(tmp, network->var);

        tmp = mxmlNewElement(node, "dev");
        mxmlNewOpaque(tmp, network->dev);

        tmp = mxmlNewElement(node, "gcd");
        mxmlNewOpaque(tmp, network->gcd);

        tmp = mxmlNewElement(node, "prt");
        mxmlNewOpaque(tmp, network->prt);

        network = network->next;
      }
    }
  }

  /*
   * Link
   */
  if (tree != NULL)
  {
    my_links = mxmlNewElement(tree, "link_list");
    if (the_links != NULL)
    {
      links = the_links->first;
      while (links != NULL)
      {

        my_link = mxmlNewElement(my_links, "link");

        tmp = mxmlNewElement(my_link, "name");
        mxmlNewOpaque(tmp, links->name);

        tmp = mxmlNewElement(my_link, "input");
        mxmlNewOpaque(tmp, links->input);

        tmp = mxmlNewElement(my_link, "output");
        mxmlNewOpaque(tmp, links->output);

        links = links->next;

      }
    }
  }

  /* Enregistrement */
  fp = fopen(filename, "w");
  /* Noeud <?xml version="1.0" encoding="UTF-8"?>*/
  fprintf(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  val = mxmlSaveFile(tree, fp, whitespace_cb);
  fclose(fp);

  return val;
}

int saveComputerFile(computer * my_computer, char * filename)
{

  mxml_node_t * tree = NULL, *ordi = NULL;
  mxml_node_t * tmp = NULL;

  FILE *fp;
  int val;

  computer * _computer = NULL;

  if (filename == NULL || my_computer == NULL || strlen(filename) == 0) return -1;

  tree = mxmlNewElement(MXML_NO_PARENT, "computer_list");

  /*
   * Computer
   */
  if (tree != NULL)
  {
    _computer = my_computer->first;
    while (_computer != NULL)
    {
      ordi = mxmlNewElement(tree, "computer");

      tmp = mxmlNewElement(ordi, "name");
      mxmlNewOpaque(tmp, _computer->name);

      tmp = mxmlNewElement(ordi, "ip");
      mxmlNewOpaque(tmp, _computer->ip);

      tmp = mxmlNewElement(ordi, "login");
      mxmlNewOpaque(tmp, _computer->login);

      tmp = mxmlNewElement(ordi, "path");
      mxmlNewOpaque(tmp, _computer->path);

      tmp = mxmlNewElement(ordi, "cpu");
      mxmlNewOpaque(tmp, _computer->cpu);

      tmp = mxmlNewElement(ordi, "informations");
      mxmlNewOpaque(tmp, _computer->informations);

      _computer = _computer->next;
    }
  }

  /* Enregistrement */
  fp = fopen(filename, "w");
  /* Noeud <?xml version="1.0" encoding="UTF-8"?>*/
  fprintf(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  val = mxmlSaveFile(tree, fp, whitespace_cb_computer);
  fclose(fp);

  return val;
}

global_sec * getGlobalSec( mxml_node_t * tree )
{
   mxml_node_t *mxml_node;
   global_sec * glob;
   char * var = NULL; 
   char * prt = NULL; 
	

   mxml_node = mxmlFindElement(tree, tree, "global", NULL, NULL, MXML_DESCEND); 

   if( mxml_node == NULL ) return NULL;
    
   if ((glob = (global_sec *) (malloc(sizeof(global_sec)))) == NULL)
    {
      fprintf(stderr, "%s \n", __FUNCTION__);
      exit(1);
    }
    
    var = strdup ( getValeur(mxml_node, tree, "var"));
    if( var != NULL )  
    { 
	 glob->var = var;
    }
    
    prt = strdup ( getValeur(mxml_node, tree, "prt"));
    if( prt != NULL )  
    { 
	 glob->prt = prt;
    }
    
    return glob;
}

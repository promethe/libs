/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "script_parserxml.h"
#include "libparserxml_MetaLeto.h"
#include "reseau.h"
#include "basic_tools.h"

/*#define DEBUG*/
/*#define LIB_MXML2 1*//* supprimer ce define si vieille lib mxml */

extern t_polyline *insert_polyline_before_at_coordinates(t_polyline_list * polyline_list, t_polyline *previous_polyline, int x, int y);

/* ------ COUDE ------ */

/* recupere les coudes de la liaison dans le fichier xml et dans le futur cree la liste des coudes */
void getCoudes(mxml_node_t * coude_list, type_liaison *liaison)
{
  mxml_node_t *mxml_node = NULL;
  t_polyline *previous_polyline = NULL;
  t_polyline *polyline = NULL;
  int x, y;

  if (coude_list == NULL)
  {
    fprintf(stderr, "ERROR: %s le pointeur coude_list en entree est null !\n", __FUNCTION__);
    return;
  }

  if (liaison->polyline_list != NULL)
  {
    previous_polyline = liaison->polyline_list->first;

    mxml_node = mxmlFindElement(coude_list, coude_list, "coude", NULL, NULL, MXML_DESCEND);
    while (mxml_node != NULL)
    {
      x = atoi(getValeur(mxml_node, coude_list, "x"));
      y = atoi(getValeur(mxml_node, coude_list, "y"));

      polyline = insert_polyline_before_at_coordinates(liaison->polyline_list, previous_polyline, x, y);

      polyline->fixed = 1 - atoi(getValeur(mxml_node, coude_list, "relatif"));
      polyline->caption_mode = atoi(getValeur(mxml_node, coude_list, "legende"));

      mxml_node = mxmlFindElement(mxml_node, coude_list, "coude", NULL, NULL, MXML_NO_DESCEND);
    }
  }
}

/* ------ LIAISON ------ */
/* analyse la liste de liaisons / fichier xml */
void getLiaisons(type_liaison *deb_liaison, mxml_node_t * tree)
{
  mxml_node_t * mxml_node = NULL, *coude_list;
  type_liaison *liaison;
  char *return_value = NULL;
  int no_liaison = 0;

#ifdef DEBUG		
  printf("xml getliaisons \n");
#endif
  if (tree == NULL)
  {
    EXIT_ON_ERROR(" tree=%p  (ne devrait pas etre null!)\n",(void *) tree);
  }

  mxml_node = mxmlFindElement(tree, tree, "liaison", NULL, NULL, MXML_DESCEND);
  if (mxml_node == NULL)
  {
    EXIT_ON_ERROR("xml / pas de noeud liaison trouve ! \n");
  }

  liaison = deb_liaison;
  while (mxml_node != NULL && liaison != NULL)
  {
    return_value = getValeur(mxml_node, tree, "depart");

    if (strlen(return_value) == 0 || strcmp(return_value, liaison->depart_name) != 0)
    {
      printf("lecture fichier draw: Le depart (%s) de la liaison No %d ne correspond pas avec la valeur lue (%s)\n", liaison->depart_name, no_liaison, return_value);
      fprintf(stderr, "WARNING: on abandonne la lecture du fichier draw \n");
      return;
    }

    return_value = getValeur(mxml_node, tree, "arrivee");

    if (strlen(return_value) == 0 || strcmp(return_value, liaison->arrivee_name) != 0)
    {
      printf("lecture fichier draw: L'arrivee (%s) de la liaison No %d ne correspond pas avec la valeur lue (%s)\n", liaison->arrivee_name, no_liaison, return_value);
      fprintf(stderr, "WARNING: on abandonne la lecture du fichier draw \n");
      return;
    }

    return_value = getValeur(mxml_node, tree, "style");

    if (strlen(return_value) == 0)
    {
      liaison->style = 1;
    }
    else
    {
      liaison->style = atoi(return_value);
    }

    coude_list = mxmlFindElement(mxml_node, tree, "coude_list", NULL, NULL, MXML_DESCEND);
    if (coude_list != NULL) /* il y a une liste de coudes */
    {

      getCoudes(coude_list, liaison);
    }

    no_liaison++;
    mxml_node = mxmlFindElement(mxml_node, tree, "liaison", NULL, NULL, MXML_NO_DESCEND);
    liaison = liaison->s;
  }
}

/* ------ OTHERS ------ */

mxml_node_t * loadFile(char * filename)
{
  FILE *fp;
  mxml_node_t *tree;

#ifdef DEBUG
  printf("XML loadFile %s \n",filename);
#endif
  if (filename == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  fp = fopen(filename, "r");
  if (fp != NULL)
  {
    tree = mxmlLoadFile(NULL, fp, MXML_OPAQUE_CALLBACK); /* "OPAQUE" pour avoir l'ensemble du texte contenu dans un noeud */
    fclose(fp);
  }
  else
  {
    fprintf(stderr, "echec ouverture fichier %s\n", filename);
    tree = NULL;
  }

  return tree;
}

/*
 * 'whitespace_cb_liaison()' - Let the mxmlSaveFile() function know when to insert newlines and tabs...
 */

const char * whitespace_cb_liaison(mxml_node_t *node, int where)
{
  const char *name; /* Name of element */

  if (node == NULL)
  {
    fprintf(stderr, "%s \n", __FUNCTION__);
    return NULL;
  }

  /*
   * We can conditionally break to a new line before or after any element.
   * These are just common HTML elements...
   */
  name = node->value.element.name;

  /*
   * Newline after close...
   */
  if (where == MXML_WS_AFTER_CLOSE)
  {
    return ("\n");
  }

  /* liaison_list */
  if (!strcmp(name, "liaison_list"))
  {
    if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }

  /* liaison */
  else if (!strcmp(name, "liaison"))
  {
    if (where == MXML_WS_BEFORE_OPEN || where == MXML_WS_BEFORE_CLOSE) return ("\t");
    else if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }

  /* depart -- arrivee */
  else if (!strcmp(name, "depart") || !strcmp(name, "arrivee") || !strcmp(name, "style"))
  {
    if (where == MXML_WS_BEFORE_OPEN) return ("\t\t");
  }

  /* coude_list */
  else if (strcmp(name, "coude_list") == 0)
  {
    if (where == MXML_WS_BEFORE_OPEN || where == MXML_WS_BEFORE_CLOSE) return ("\t\t");
    if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }

  /* coude */
  else if (!strcmp(name, "coude"))
  {
    if (where == MXML_WS_BEFORE_OPEN || where == MXML_WS_BEFORE_CLOSE) return ("\t\t\t");
    else if (where == MXML_WS_AFTER_OPEN) return ("\n");
  }

  /* x -- y */
  else if (!strcmp(name, "x") || !strcmp(name, "y") || !strcmp(name, "relatif") || !strcmp(name, "legende"))
  {
    if (where == MXML_WS_BEFORE_OPEN) return ("\t\t\t\t");
  }

  /*
   * Return NULL for no added whitespace...
   */
  return (NULL);
}

/*
 * fonction xml_saveLiaisonFile 
 */

void save_coudes(type_liaison *liaison, mxml_node_t *liaison_node)
{
  mxml_node_t *coude_node = NULL;
  mxml_node_t *tmp = NULL;
  mxml_node_t *coude_list_node = NULL;
  t_polyline *polyline = NULL;

  if (liaison->polyline_list != NULL)
  {

    coude_list_node = mxmlNewElement(liaison_node, "coude_list");

    polyline = liaison->polyline_list->first;
    while (polyline != NULL && polyline->next != NULL)
    {
      coude_node = mxmlNewElement(coude_list_node, "coude");

      tmp = mxmlNewElement(coude_node, "x");
      mxmlNewInteger(tmp, polyline->x_e);

      tmp = mxmlNewElement(coude_node, "y");
      mxmlNewInteger(tmp, polyline->y_e);

      tmp = mxmlNewElement(coude_node, "relatif");
      mxmlNewInteger(tmp, 1 - polyline->fixed);

      tmp = mxmlNewElement(coude_node, "legende");
      mxmlNewInteger(tmp, polyline->caption_mode);

      polyline = polyline->next;
    }
  }
}

int xml_saveLiaisonFile(type_liaison *deb_liaison, char * filename)
{
  mxml_node_t *xml = NULL; /* <?xml ... ?> */
  mxml_node_t * liaison_list;
  mxml_node_t * tmp = NULL;
  mxml_node_t *liaison; /* bizarre ce ne sont pas des liaisons ou des coudes! (mais correct dans code) */

  FILE *fp;
  int val;

  type_liaison * _liaison = NULL;

#ifdef DEBUG
  printf("xml debut xml_save_LiaisonFile\n");
#endif
  _liaison = deb_liaison;
  if (_liaison == NULL || filename == NULL)
  {
    return -1;
  }

  /*ligne a enlever si l'on a une vielle version de mxml */
  /* supprimer le define au debut LIB_MXML2 */
#ifdef LIB_MXML2
  xml = mxmlNewXML("1.0");
  liaison_list = mxmlNewElement(xml, "liaison_list");
#else
  liaison_list = xml = mxmlNewElement(MXML_NO_PARENT, "liaison_list");
#endif

  /*
   * Liaison
   */
  while (_liaison != NULL)
  {
    liaison = mxmlNewElement(liaison_list, "liaison");

#ifdef DEBUG
    printf("saveLiaisonFile: depart name = %s\n", _liaison->depart_name);
#endif
    tmp = mxmlNewElement(liaison, "depart");
    mxmlNewOpaque(tmp, _liaison->depart_name);

#ifdef DEBUG
    printf("saveLiaisonFile: arrivee name = %s\n", _liaison->arrivee_name);
#endif
    tmp = mxmlNewElement(liaison, "arrivee");
    mxmlNewOpaque(tmp, _liaison->arrivee_name);

    tmp = mxmlNewElement(liaison, "style");
    mxmlNewInteger(tmp, _liaison->style);

    save_coudes(_liaison, liaison);

    _liaison = _liaison->s;
  }

  /* Enregistrement */
  fp = fopen(filename, "w");
  /* Noeud <?xml version="1.0" encoding="UTF-8"?>*/
#ifndef LIB_MXML2
  fprintf(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
#endif
  val = mxmlSaveFile(xml, fp, whitespace_cb_liaison);
  fclose(fp);

#ifdef DEBUG
  printf("xml fin xml_save_LiaisonFile\n");
#endif

  return val;
}


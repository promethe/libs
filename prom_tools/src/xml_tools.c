/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <dlfcn.h>
#include <stdarg.h>
#include <math.h>
#include <stdlib.h>

#include <net_message_debug_dist.h>
#include <xml_tools.h>
#include <dlfcn.h>

#define STRING_NUMBER_MAX 16


const char *xml_get_node_name(Node *node)
{
    return node->value.element.name;
}

void xml_set_node_name(Node *node, const char *name)
{
	mxmlSetElement(node, name);
}


Node *xml_get_first_child(Node *node)
{
    return mxmlFindElement(node, node, NULL, NULL, NULL, MXML_DESCEND);
}

Node  *xml_get_next_sibling(Node *node)
{
    return mxmlFindElement( node, node->parent, NULL, NULL, NULL, MXML_NO_DESCEND);
}

Node  *xml_get_first_child_with_node_name(Node *tree, const char *name)
{
    return mxmlFindElement(tree, tree, name, NULL, NULL, MXML_DESCEND);
}

Node  *xml_get_next_homonymous_sibling(Node *current_node)
{
    return mxmlFindElement( current_node, current_node->parent, current_node->value.element.name, NULL, NULL, MXML_NO_DESCEND);
}

Node *xml_load_file(const char *filename)
{
    Node *n=NULL;
    FILE *f=NULL;

    f=fopen(filename, "r");
    if(f==NULL) EXIT_ON_ERROR("unable to open %s", filename);
    n=mxmlLoadFile(NULL, f, MXML_TEXT_CALLBACK);
    fclose(f);

    return n;
}

void xml_delete_tree(Node *tree)
{
    mxmlDelete(tree);
}

int xml_get_number_of_childs(Node *node)
{
    int number = 0;
    Node *current_node;

    for(current_node = xml_get_first_child(node); current_node != NULL; current_node = xml_get_next_sibling(current_node))
    {
        number++;
    }
    return number;
}

const char *xml_try_to_get_string(Node *node, const char *name_of_attribute)
{
    return mxmlElementGetAttr(node, name_of_attribute);
}

const char *xml_get_string(Node *node, const char *name_of_attribute)
{
    const char *string_of_attribute;
    string_of_attribute = mxmlElementGetAttr(node, name_of_attribute);
    if (string_of_attribute == NULL) EXIT_ON_ERROR("The required attribute '%s' is not found in node '%s'", name_of_attribute, node->value.element.name);
    return string_of_attribute;
}

/**
 *   Try to get to set the value of pointer_to_int with the value of the attribute. In case of succes it return 1. In case of failure it returns 0 and the content of the pointer_to_int is undefined.
 *   */
int xml_try_to_get_int(Node *node, const char *name_of_attribute, int *pointer_to_int)
{
    const char *string_value;
    int success;

    string_value = xml_try_to_get_string(node, name_of_attribute);
    if (string_value == NULL) return 0;

    success = sscanf(string_value, "%d", pointer_to_int);
    if (!success)  EXIT_ON_ERROR("The value '%s' of attribute '%s' in node '%s' is not an int", string_value, name_of_attribute, node->value.element.name);
    return 1;
}

int xml_get_int(Node *node, const char *name_of_attribute)
{
    const char *string_value;
    int int_value;

    string_value = xml_get_string(node, name_of_attribute);
    if (!string_value) EXIT_ON_ERROR("The attribute '%' does not exist for node '%s'.", name_of_attribute, node->value.element.name);

    if (sscanf(string_value, "%d", &int_value) != 1) EXIT_ON_ERROR("The value '%s' of attribute '%s' in node '%s' is not a int", string_value, name_of_attribute, node->value.element.name);
    return int_value;
}

int xml_try_to_get_float(Node *node, const char *name_of_attribute, float *pointer_to_float)
{
    const char *string_value;
    int success;

    string_value = xml_try_to_get_string(node, name_of_attribute);
    if (string_value == NULL) return 0;

    success = sscanf(string_value, "%f", pointer_to_float);
    if (!success)  EXIT_ON_ERROR("The value '%s' of attribute '%s' in node '%s' is not a float", string_value, name_of_attribute, node->value.element.name);
    return SUCCESS;
}

float xml_get_float(Node *node, const char *name_of_attribute)
{
    float float_value;
    int success;

    success = xml_try_to_get_float(node, name_of_attribute, &float_value);
    if  (!success) EXIT_ON_ERROR("The attribute '%s' in node '%s' has not been defined.", name_of_attribute, node->value.element.name);

    return float_value;
}


void xml_set_float(Node *node, const char *name_of_attribute, float value)
{
	char buffer[STRING_NUMBER_MAX];

	snprintf(buffer, STRING_NUMBER_MAX, "%f", value);
	mxmlElementSetAttr(node, name_of_attribute, buffer);
}

void xml_set_int(Node *node, const char *name_of_attribute, int value)
{
	char buffer[STRING_NUMBER_MAX];

	snprintf(buffer, STRING_NUMBER_MAX, "%d", value);
	mxmlElementSetAttr(node, name_of_attribute, buffer);
}

void xml_set_string(Node *node, const char *name_of_attribute, const char *content)
{
	mxmlElementSetAttr(node, name_of_attribute, content);
}

void xml_save_file(char *filename, Node *node, mxml_save_cb_t whitespace_cb)
{
	FILE *f = NULL;

	f = fopen(filename,"w");

	if(f == NULL){EXIT_ON_ERROR("unable to open %s", filename);}
	mxmlSaveFile(node, f, whitespace_cb);

	fclose(f);
}





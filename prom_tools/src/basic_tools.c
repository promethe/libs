/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * basic_tools.c
 *
 *  Created on: Apr 28, 2011
 *      Author: Arnaud Blanchard
 */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <dlfcn.h>
#include <basic_tools.h>
#include <string.h>

char *Int2Str(int entier)
{
    static char chaine[255];

    sprintf(chaine, "%d", entier);
    return chaine;
}

char *Float2Str(float x)
{
    static char chaine[255];

    sprintf(chaine, "%f", x);
    return chaine;
}

void* load_libraryf(const char *format, ...)
{
    void *lib_handle;
    va_list arguments;
    char filename_of_library[PATH_MAX];

    va_start(arguments, format);
    vsprintf(filename_of_library, format, arguments);
    va_end(arguments);

    lib_handle = dlopen(filename_of_library, RTLD_GLOBAL|RTLD_LAZY) ;
    if (lib_handle==NULL) EXIT_ON_ERROR("Library '%s' has not been found.' \n dlerror: %s.", filename_of_library, dlerror());
    return lib_handle;
}

void buffer_allocate_min(type_buffer *buffer, int minimum_size)
{
  if (minimum_size > buffer->size)
  {
    buffer->size = minimum_size;
    if (buffer->position > 0)
      {
         MANY_REALLOCATIONS(&(buffer->data),minimum_size);
      }
    else
    {
      free(buffer->data);
      buffer->data = MANY_ALLOCATIONS(minimum_size, char);
    }
  }
}

void buffer_replace(type_buffer *buffer, const char *data, int size)
{
  buffer_allocate_min(buffer, size);
  memcpy(buffer->data, data, size);
  buffer->position = size;
}

void buffer_append(type_buffer *buffer, const char *data, int size)
{
  buffer_allocate_min(buffer, buffer->position + size);
  memcpy(&(buffer->data[buffer->position]), data, size);
  buffer->position += size;
}

double frand_a_b(double a, double b){
    return (( rand()/(double)RAND_MAX ) * (b-a) + a);
}

int prom_getopt(const char *args, const char *opt, char *retour)
{
  /* on en peut pas utiliser un pointeur static
   puisque dans le cas des threads on peut avoir
   plusieurs appels simultanes a cette fonction */
  char *souschaine;
  char option[32];
  int i, len;
  setlocale( LC_ALL, "C" );
  /* on regarde si opt designe 'option' ou '-option' */
  if (opt[0] == '-')
  {
    /* okay on accepte cette formulation */
    strcpy(option, opt);
  }
  else
  {
    /* la formulation preferee */
    option[0] = '-';
    option[1] = '\0';
    strcat(option, opt);
  }
  /* recherche de la souschaine */
  souschaine = strstr(args, option);
  /* on recopie vers la chaine du pointeur retour, si necessaire */
  if (souschaine == NULL)
  {
    if (retour != NULL) retour[0] = '\0';
    return 0;
  }
  else
  {
    len = strlen(option);
    i = 0;
    while (souschaine[i + len] != ' ' && souschaine[i + len] != '\0' && souschaine[i + len] != '-' && i < (PARAM_MAX - len))
    {
      if (retour == NULL) EXIT_ON_ERROR("The option %s has an argument but it was not expected. The retour pointer is NULL", option);
      retour[i] = souschaine[i + len]; /* ecrit les arguments de l'option dans retour */
      i++;
    }
    if (i == 0)
    {
      if (retour != NULL) retour[0] = '\0';
      return 1;
    }
    else
    {
      retour[i] = '\0';
      return 2;
    }
  }
}

/* Parse args pour trouver option et converti en int si possible.
 *
 * Return 0 s'il n'est pas trouv��, 1 si la conversion pause probl��me et 2 si tout se passe bien.
 *
 */
int prom_getopt_int(const char *args, const char *option, int *value)
{
  char const *current_addr;
  setlocale( LC_ALL, "C" );
  for (current_addr = strstr(args, option); current_addr != NULL;  current_addr = strstr(current_addr, option))
  {
     current_addr += strlen(option);
     if (sscanf(current_addr, "%d", value) == 1) return 2; // This is followed by a number
     if (current_addr[0]  == '-')  return 1;
  }
  return 0;
}

/* Parse args pour trouver option et converti en float si possible.
 *
 * Return 0 s'il n'est pas trouv��, 1 si la conversion pause probl��me et 2 si tout se passe bien.
 *
 */
int prom_getopt_float(const char *args, const char *option, float *value)
{
  char const *current_addr;
  setlocale( LC_ALL, "C" );
  for (current_addr = strstr(args, option); current_addr != NULL;  current_addr = strstr(current_addr, option))
  {
     current_addr += strlen(option);
     if (sscanf(current_addr, "%f", value) == 1)
       {
         return 2; // This is followed by a number nor a new argument.
       }
     if (current_addr[0] == '-')  return 1;
  }
  return 0;
}

/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/* Philippe Gaussier oct 2004 */
/* outils_script.c fusion des fonctions de leto et promethe pour homogeneiser */
/* et simplifier la maintenance du code.                                      */

/* Attention probleme reset pointeur a NULL sans le free de l'ensemble des structures */
/* pouvant etre crees par un groupe. */

/* janvier 2005 ajout debut gestion lecture information symbolique (champs contenant
 des valeurs symboliques au lieu de valeurs numeriques */
/* fevrier 2007 ajout de la lecture de numero de groupes symboliques */
/* Il faut utiliser reseau.h avec le #define  SYMBOLIQUE_VERSION actif */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif /* _GNU_SOURCE */

/*#define DEBUG*/
#include "net_message_debug_dist.h" 

/* pour mac OS */
#ifdef Darwin
#define __USE_GNU
#include "search_gnu.h"
#else
#include <search.h>  /* classique linux */
#endif

#include <reseau.h>

/************************************* G_HASH_TABLE ************************************************/
/*#include <glib.h> */

/* recherche un groupe par la table de hachage */
void *lookup_hash_table(struct hsearch_data *hash_table, char *name)
{
  ENTRY entry, *ep = NULL;

  /* si c'est Promethe ou cc_leto qui utilise le basic parser alors hashtab est null */
  if (hash_table == NULL)
  {
    entry.key = name;
    entry.data = NULL;
    ep = hsearch(entry, FIND);
    if (ep == NULL) return NULL;
    else return ep->data;
  }
  else
  {
    entry.key = name;
    entry.data = NULL;
    hsearch_r(entry, FIND, &ep, hash_table);
    if (ep == NULL)
    {
      /* PRINT_WARNING("group %s not found size = %i in hash table\n", name, strlen(name));*/
      return NULL;
    }
    else
    {
      return ep->data;
    }
  }
  /*   else return g_hash_table_lookup(onglet_leto->gHashTable,name); */
  return NULL;
}

/* insere un nouveau groupe dans la table de hachage */
void insert_hash_table(struct hsearch_data *hash_table, char *key, void *data)
{
  ENTRY e, *ep;

  if (hash_table == NULL)
  {
    e.key = key;
    e.data = data;
    ep = hsearch(e, ENTER); /* creation */
  }
  else
  {
    e.key = key;
    e.data = data;
    hsearch_r(e, ENTER, &ep, hash_table); /* creation */
  }
  if (ep == NULL) EXIT_ON_ERROR("Fail to add key %s", key);
}

/*-----------------------------------------------------------------------*/
/* rajoute une entree dans la table avec le no_name comme cle            */
/* renvoie 0 en cas de succes et -1 si le no_name est deja utilse        */
/*-----------------------------------------------------------------------*/

int create_new_entry_in_hash_table(type_groupe *groupe, struct hsearch_data *hashtab)
{
  void *exist = NULL;

  exist = lookup_hash_table(hashtab, groupe->no_name);

  if (exist != NULL) return -1;
  else insert_hash_table(hashtab, groupe->no_name, groupe);

  return 0;
}

/* efface les traces d'un script de la table de hachage  */
void remove_hash_table(struct hsearch_data *hashtab)
{
  if (hashtab == NULL)
  {
    hdestroy();
  }
  else
  {
    hdestroy_r(hashtab);
  }
}

/* initialise la table de hachage d'un onglet */
void init_hash_table(struct hsearch_data *hashtab)
{
  if (hashtab == NULL)
  {
    hcreate(10000);
  }
  else
  {
    hcreate_r(10000, hashtab);
  }
}

/****************************************************************************************************/

/**************************************************************/
/* transforme un entier en chaine de caracteres */
/* Attention la definition est aussi dans Tx_graphic2.c */
/*char  * Int2Str(int entier)
 {
 static char chaine[255];

 sprintf(chaine,"%d",entier);
 return chaine;
 }

 char *Float2Str(float x)
 {
 static char chaine[255];

 sprintf(chaine,"%f",x);
 return chaine;
 }*/

int nbre_groupes_lus = 0;

#define TEST_NON_CHIFFRE(x) (x<'0' || x>'9')

/* Le test n'est pas complet: pas de verif si '-' au milieu de l'expression ou 2 - ...*/
int verify_if_int(const char *chaine)
{
  int i, longueur;
  char c;

  dprints("Verify if int: %s\n", chaine);

  longueur = strlen(chaine);
  for (i = 0; i < longueur; i++)
  {
    c = chaine[i];
    if (TEST_NON_CHIFFRE(c) && (c != '-'))
    {
      dprints("la chaine %s n'est pas un entier !\n", chaine);
      dprints("Utiliser avant le preprocesseur...\n");
      return 0;
    }
  }
  return 1;
}

/* Le test n'est pas complet: pas de verif si 2 '.' ou '-' au milieu de l'expression...*/
int verify_if_float(const char *chaine)
{
  int i, longueur;
  char c;

  dprints("Verify if float: %s\n", chaine);
  longueur = strlen(chaine);

  for (i = 0; i < longueur; i++)
  {
    c = chaine[i];
    if (TEST_NON_CHIFFRE(c) && (c != '-') && (c != '.'))
    {
      dprints("la chaine %s n'est pas un flottant !\n", chaine);
      dprints("Utiliser avant le preprocesseur...\n");
      return 0;
    }
  }
  return 1;
}

const char *stop_if_not_int(const char *chaine)
{
  if (verify_if_int(chaine) == 0) EXIT_ON_ERROR("stop_if_not_int %s \n\tThis is probably a problem with the variables. Check your compilation and your .var .", chaine);
  return chaine;
}

const char *stop_if_not_float(const char *chaine)
{
  if (verify_if_float(chaine) == 0) EXIT_ON_ERROR("stop_if_not_float %s \n\tThis is probably a problem with the variables. Check your compilation and your .var .", chaine);
  return chaine;
}

inline void Str2Int(int *x, char *chaine)
{
  printf("Str2Int chaine = %s \n", chaine);
  *x = atoi(stop_if_not_int(chaine));
}

inline void Str2Float(float *x, char *chaine)
{
  printf("Str2Float chaine = %s \n", chaine);
  *x = atof(stop_if_not_float(chaine));
}

/* renvoie le numero du groupe associe a un symbole. renvoie -1 si pas trouve */
int find_no_associated_to_symbolic_name(char *name, struct hsearch_data *hashtab)
{
  void *exist = NULL;

  if (name == NULL)
  {
    EXIT_ON_ERROR("The name cannot be NULL \n");
  }
  exist = lookup_hash_table(hashtab, name);

  if (exist == NULL)
  {
    PRINT_WARNING("'%s' is not the name of a group", name);
    return -1;
  }

  return ((type_groupe*) exist)->no;
}

/* renvoie le pt du groupe associe a un symbole. renvoie NULL si pas trouve */
type_groupe *find_group_associated_to_symbolic_name(char *name, struct hsearch_data *hashtab)
{
  void *exist = NULL;

  if (name == NULL)
  {
    EXIT_ON_ERROR("ERROR the name cannot be NULL \n");
  }
  exist = lookup_hash_table(hashtab, name);

  if (exist == NULL)
  {
    fprintf(stderr, "La chaine %s n'est pas un symbole utilise\n", name);
    return NULL;
  }
  /*  fprintf (stderr, "La chaine %s est un symbole deja utilise\n",name);*/

  return ((type_groupe*) exist);
}

/*-----------------------------------------------------------------------*/

void save_comment(FILE * f1, type_noeud_comment * comment)
{
  while (comment != NULL)
  {
    fprintf(f1, "%s", comment->chaine);
    comment = comment->suiv;
  }
}

/*ajoute un commentaire a la fin de la liste chainee des commentaires*/
/* renvoie le pointeur sur le dernier element de la liste */

type_noeud_comment *add_comment(type_noeud_comment * comment, char *chaine)
{
  type_noeud_comment *pt;
  int longueur;
  /*int taille; */

  pt = (type_noeud_comment *) malloc(sizeof(type_noeud_comment));
  pt->suiv = NULL;

  longueur = (strlen(chaine) + 1);
  if (longueur > TAILLE_CHAINE)
  {
    printf("WARNING: la chaine de char: %s a ete tronquee dans add_comment (basic_parser)\n", chaine);
    printf("car sa taille est superieure a %d \n", TAILLE_CHAINE);
    longueur = TAILLE_CHAINE;
  }
  memcpy(pt->chaine, chaine, longueur * sizeof(char));
  if (comment != NULL) comment->suiv = pt;
  return pt;
}

/* renvoie 1 si la ligne debute par un caractere de commentaire */
/* renvoie 0 sinon */
int test_comment(char *ligne)
{
  char c;

  c = ligne[0];
  if (c == '#' || c == '%' || c == '\'' || c == ';' || c == '!' || c == ',' || c == '.' || c == '&' || c == '~' || c == '_') return 1;
  return 0;
}

/* renvoie 1 si la ligne est vide ou ne comporte que des espaces */
/* renvoie 0 sinon */
int empty_line(char *ligne)
{
  char c;
  size_t i;

  for (i = 0; i < strlen(ligne) - 1; i++)
  {
    c = ligne[i];
    if (c == '\n')
    {
      break;
    }
    if (c != ' ' && c != '\t')
    {
      return 0;
    }
  }
  return 1;
}

/* renvoie le pointeur vers le premier element de la liste */
type_noeud_comment *read_line_with_comment(FILE * f1, type_noeud_comment * first_comment, char *ligne)
{
  int is_comment;
  type_noeud_comment *last_comment, *current_comment;

  current_comment = first_comment;
  if (first_comment == NULL) last_comment = NULL;
  else /* va a la fin de la liste */
  {
    while (current_comment != NULL)
    {
      last_comment = current_comment;
      current_comment = current_comment->suiv;
    }
  }
  fgets(ligne, TAILLE_CHAINE, f1);
  while ((is_comment = test_comment(ligne)) == 1 || empty_line(ligne))
  {
#ifdef DEBUG
    printf(">>> %s \n", ligne);
#endif

    if (is_comment) last_comment = add_comment(last_comment, ligne); /*rajout M. maillard
     pour eviter de sauter des lignes en trop : les sauts de ligne entre groues sont deja rajoutes par defaut et donc ca rajoute un en plus a chaque ecriture */
    if (first_comment == NULL) first_comment = last_comment;
    fgets(ligne, TAILLE_CHAINE, f1);
    if (feof(f1) != 0) return first_comment;
  }
  return first_comment;

}

char *recherche_champs(char *ligne, const char *chaine)
{
  char *ptr;
  ptr = strstr(ligne, chaine);
#ifdef DEBUG
  if (ptr == NULL)
  printf("basic_parser: Attention pas de champs [%s] dans [%s]\n", chaine, ligne);
#endif
  return ptr;
}

/*------------------------------------------------------------------------------*/
/* la fonction read_one_group est decoupee en 2 partie de maniere a pouvoir etre
 utilisee leto et promethe en meme temps. La fonction globale ne peut pas etre partagee
 a cause de la difference de type d'acces aux groupes : liste chainee vs tableau et de la
 possibilite de permuter les groupes dans le script en s'assurant que leurs numeros sont corrects */

void read_one_group_second_part(FILE * f1, type_groupe * groupe, type_noeud_comment * first_comment, char** retour_xyz)
{
  char ligne[TAILLE_CHAINE];
  int i, debug_flags;
  char variable[5][TAILLE_CHAINE];
  first_comment = read_line_with_comment(f1, first_comment, ligne);

  if (retour_xyz!=NULL)
   {
    if( recherche_champs(ligne, "infos_xyz") != NULL )
    {
     sscanf(ligne, "infos_xyz = %s \n", variable[0]);
     *retour_xyz = MANY_ALLOCATIONS(TAILLE_CHAINE,char);
     strncpy((char*)(*retour_xyz),variable[0],TAILLE_CHAINE);
     first_comment = read_line_with_comment(f1, first_comment, ligne);
    }
   }


  if (recherche_champs(ligne, "taillex") != NULL)
  {
    sscanf(ligne, "taillex = %s , tailley = %s \n", variable[0], variable[1]);
    Str2MY_Int(groupe->taillex, variable[0]);
    Str2MY_Int(groupe->tailley, variable[1]);
    first_comment = read_line_with_comment(f1, first_comment, ligne);
  }

  dprints("taillex = %s , tailley = %s \n", variable[0], variable[1]);

  if (recherche_champs(ligne, "learning") != NULL)
  {
    sscanf(ligne, "learning rate = %s \n", variable[0]);
    dprints("learning rate = %s \n", variable[0]);
    Str2MY_Float(groupe->learning_rate, variable[0]);
    first_comment = read_line_with_comment(f1, first_comment, ligne);
  }

  if (groupe->type == No_Hebb || groupe->type == No_SAW || groupe->type == No_Kmean_R || groupe->type == No_CTRNN2 || groupe->type == No_NLMS)
  {
    if (recherche_champs(ligne, "alpha") != NULL)
    {
      sscanf(ligne, "alpha = %s \n", variable[0]);
      dprints("alpha = %s \n", variable[0]);
      Str2MY_Float(groupe->alpha, variable[0]);
      first_comment = read_line_with_comment(f1, first_comment, ligne);
    }

  }

  if (groupe->type == No_Winner || groupe->type == No_Winner_Macro || groupe->type == No_Sutton_Barto || No_PCR)
  {
    if (recherche_champs(ligne, "noise_level") != NULL)
    {
      sscanf(ligne, "noise_level = %s \n", variable[0]);
      dprints("noise_level = %s \n", variable[0]);
      Str2MY_Float(groupe->noise_level, variable[0]);
      first_comment = read_line_with_comment(f1, first_comment, ligne);
    }
  }

  if (recherche_champs(ligne, "simulation") != NULL)
  {
    sscanf(ligne, "simulation speed = %s \n", variable[0]);
    Str2MY_Float(groupe->simulation_speed, variable[0]);
    first_comment = read_line_with_comment(f1, first_comment, ligne);
    dprints("simulation speed = %s \n", variable[0]);
  }

  if (groupe->type == No_Winner_Colonne || groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif)
  {
    if (recherche_champs(ligne, "tolerance") != NULL)
    {
      sscanf(ligne, "tolerance = %s\n", variable[0]);
      Str2MY_Float(groupe->tolerance, variable[0]);
      first_comment = read_line_with_comment(f1, first_comment, ligne); /* pour compatib. avec le if suivant */
    }
  }

  if (groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif || groupe->type == No_Macro_Colonne || groupe->type == No_Winner || groupe->type == No_Winner_Macro)
  {
    dprints("---ext groupe \n ligne : %s \n", ligne);
    if (recherche_champs(ligne, "dvp") != NULL)
    {
      i = sscanf(ligne, "dvp       = %s , dvn = %s\n", variable[0], variable[1]);
      Str2MY_Int(groupe->dvp, variable[0]);
      Str2MY_Int(groupe->dvn, variable[1]);
      if (i != 2)
      {
        MY_IntAffect(groupe->dvp, -1);
        MY_IntAffect(groupe->dvn, -1);
      }
      first_comment = read_line_with_comment(f1, first_comment, ligne);
      dprints("dvp: %s, dvn: %s\n", MY_Int2Str(groupe->dvp), MY_Int2Str(groupe->dvn));
    }
    else
    {
      printf("Affectation dvp et dvn par defaut pour le groupe %d \n", groupe->no);
      MY_IntAffect(groupe->dvp, 0);
      MY_IntAffect(groupe->dvn, 0);
    }
  }

  dprints("ligne : %s \n", ligne);

  if (groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif || groupe->type == No_Macro_Colonne || groupe->type == No_Winner || groupe->type == No_Winner_Macro)
  {

    if (recherche_champs(ligne, "alpha") != NULL)
    {
      i = sscanf(ligne, "alpha     = %s \n", variable[0]);
      Str2MY_Float(groupe->alpha, variable[0]);
      if (i != 1)
      {
        MY_FloatAffect(groupe->alpha, 0.);
      }
      first_comment = read_line_with_comment(f1, first_comment, ligne);
      dprints("alpha: %s \n", MY_Float2Str(groupe->alpha));
    }
    else
    {
      MY_FloatAffect(groupe->alpha, 0.);
    }
  }

  if (groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif || groupe->type == No_Macro_Colonne || groupe->type == No_Winner || groupe->type == No_Winner_Macro || groupe->type == No_SAW || groupe->type == No_Kmean_R || groupe->type == No_CTRNN2)
  {
    dprints("ligne : %s \n", ligne);
    if (recherche_champs(ligne, "nbre_de_1") != NULL)
    {
      i = sscanf(ligne, "nbre_de_1 = %s \n", variable[0]);
      Str2MY_Float(groupe->nbre_de_1, variable[0]);
      if (i != 1)
      {
        MY_FloatAffect(groupe->nbre_de_1, 0.);
      }
      first_comment = read_line_with_comment(f1, first_comment, ligne);
      dprints("nbe de 1= %s \n", MY_Float2Str(groupe->nbre_de_1));
    }
    else
    {
      MY_FloatAffect(groupe->nbre_de_1, 0.);
    }
  }

  if (groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif || groupe->type == No_Macro_Colonne || groupe->type == No_Winner || groupe->type == No_Winner_Macro)
  {
    dprints("ligne : %s \n", ligne);
    if (recherche_champs(ligne, "sigma_f") != NULL)
    {
      i = sscanf(ligne, "sigma_f   = %s \n", variable[0]);
      Str2MY_Float(groupe->sigma_f, variable[0]);
      if (i != 1)
      {
        MY_FloatAffect(groupe->sigma_f, 0.);
      }
      dprints("sigma: %s\n", MY_Float2Str(groupe->sigma_f));
      first_comment = read_line_with_comment(f1, first_comment, ligne);
    }
    else
    {
      MY_FloatAffect(groupe->sigma_f, 0.);
    }
  }

  if (groupe->type == No_Kohonen)
  {
    if (recherche_champs(ligne, "dvp") != NULL)
    {
      sscanf(ligne, "dvp       = %s , dvn = %s\n", variable[0], variable[1]);
      Str2MY_Float(groupe->dvp, variable[0]);
      Str2MY_Float(groupe->dvn, variable[1]);
      first_comment = read_line_with_comment(f1, first_comment, ligne);
    }
    else
    {
      printf("Affectation dvp et dvn par defaut pour le groupe %d \n", groupe->no);
      MY_IntAffect(groupe->dvp, 0);
      MY_IntAffect(groupe->dvn, 0);
    }
  }

  if (groupe->type == No_Granular)
  {
    if (recherche_champs(ligne, "min") != NULL)
    {
      sscanf(ligne, "time_spectrum min = %s \n", variable[0]);
      Str2MY_Float(groupe->tolerance, variable[0]);
      first_comment = read_line_with_comment(f1, first_comment, ligne);
    }
    if (recherche_champs(ligne, "max") != NULL)
    {
      sscanf(ligne, "time_spectrum max = %s \n", variable[0]);
      Str2MY_Float(groupe->alpha, variable[0]);
      first_comment = read_line_with_comment(f1, first_comment, ligne);
    }
  }

  if (recherche_champs(ligne, "type2") != NULL)
  {
    sscanf(ligne, "type2  = %s \n", variable[0]);
    Str2MY_Int(groupe->type2, variable[0]);
    first_comment = read_line_with_comment(f1, first_comment, ligne);
  }
  else
  {
    printf("Affectation par defaut pour le groupe %s \n", groupe->no_name);
    MY_IntAffect(groupe->type2, 0);
  }

  if (recherche_champs(ligne, "groupe") != NULL)
  {
    sscanf(ligne, "groupe = %s\n", groupe->nom);
    dprints("nom= %s \n", groupe->nom);
    first_comment = read_line_with_comment(f1, first_comment, ligne);
  }
  else
  {
    printf("Pas de nom pour le groupe %s \n", groupe->no_name);
  }

  sscanf(ligne, "posx = %d , posy = %d \n", &groupe->posx, &groupe->posy);
  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "reverse = %d \n", &groupe->reverse);
  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "p_posx = %d , p_posy = %d\n", &groupe->p_posx, &groupe->p_posy);
  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "debug = %d\n", &debug_flags);
  groupe->debug &= debug_flags | ~(FLAG_LOCAL_DEBUG | FLAG_LOCAL_NORMALIZE_DEBUG);
  groupe->debug |= debug_flags & (FLAG_LOCAL_DEBUG | FLAG_LOCAL_NORMALIZE_DEBUG); /* Only local flags are changed */

  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "ech_temps = %d\n", &groupe->ech_temps);
  dprints("ech temps = %d \n", groupe->ech_temps);
  /*  if(f2!=NULL) fscanf(f2,"%d\n",&groupe->reverse); */

  groupe->comment = first_comment;

}

int read_one_group_leto(FILE * f1, type_groupe * groupe, char *macro_name, struct hsearch_data *hashtab, char** retour_nom_xyz)
{
  static char ligne[TAILLE_CHAINE];
  char variable[5][TAILLE_CHAINE];
  char no_name[SIZE_NO_NAME];
  type_noeud_comment *first_comment = NULL;
  int i;
  int ret;
  void *exist = NULL;

  memset(no_name, 0, sizeof(char) * SIZE_NO_NAME);
  for (i = 0; i < 5; i++)
    memset(variable[i], 0, sizeof(char) * TAILLE_CHAINE);

  first_comment = (groupe->comment); /* le champs comment est initialise a NULL */
  first_comment = read_line_with_comment(f1, first_comment, ligne);

  ret = sscanf(ligne, "groupe = %s , type = %d , nbre neurones = %s , seuil = %s\n", groupe->no_name, &groupe->type, variable[0], variable[1]);
  if (ret != 4)
  {
    EXIT_ON_ERROR("Reading first line of group in the script file : '%s'", ligne);
  }

  Str2MY_Int(groupe->nbre, variable[0]);
  Str2MY_Float(groupe->seuil, variable[1]);
  dprints("groupe = %s , type = %d , nbre neurones = %s , seuil = %s\n", groupe->no_name, groupe->type, variable[0], variable[1]);

  memcpy(no_name, groupe->no_name, (strlen(groupe->no_name) + 1) * sizeof(char));
  if (macro_name != NULL) sprintf(groupe->no_name, "%s_%s", macro_name, no_name);

  exist = lookup_hash_table(hashtab, groupe->no_name);

  if (exist != NULL)
  {
    EXIT_ON_ERROR(" Erreur le label %s du groupe %d a deja ete utilise par le groupe entre en no %s \n", groupe->no_name, nbre_groupes_lus, groupe->no_name);
  }
  else
  {
    insert_hash_table(hashtab, groupe->no_name, groupe);
  }

  /*    printf("groupe %d , nom %s \n",groupe->no,groupe->no_name);*/
  read_one_group_second_part(f1, groupe, first_comment, retour_nom_xyz);

#ifndef SYMBOLIQUE_VERSION
  return groupe->nbre;
#else
  return -1;
#endif
}

/* on lit les champs d'un groupe se trouvant dans un tableau cree de maniere statique.*/
/* La fonction renvoie le nombre de neurones dans le groupe.                          */
/* A chaque relecture du script, les pointeurs vers les extensions sont remis a zero. */
/* ATTENTION c'est incorrect car on ne fait pas le free sur les structures de donnees.*/
/* Il faudrait prevoir un pointeur vers une fonction pour l'init et la fin (constructeur/destructeur)*/

/* Dans promethe, def_groupe est un tableau de groupe et non pas un pointeur sur un groupe comme dans leto. */
/* la fonction renvoie le numero du groupe lu car les groupes ne sont
 pas forcement dans l'ordre dans le script */

int read_one_group_promethe(FILE * f1, type_groupe def_groupe[], int nb_max)
{
  int no, type;
  int nbre;
  float seuil;
  int ret;
  static char ligne[TAILLE_CHAINE];
  type_noeud_comment *first_comment;
  ENTRY e, *ep;

#ifdef DEBUG
  char variable[5][TAILLE_CHAINE];
#endif

  first_comment = NULL; /* le champs comment est initialise a NULL */
  first_comment = read_line_with_comment(f1, first_comment, ligne);

  /*printf(":::%s\n",ligne); */
  dprints("nbre groupes lus = %d \n", nbre_groupes_lus);
  no = nbre_groupes_lus;
  ret = sscanf(ligne, "groupe = %s , type = %d , nbre neurones = %d , seuil = %f\n", def_groupe[no].no_name, &type, &nbre, &seuil);
  if (ret != 4)
  {
    EXIT_ON_ERROR("Reading first line of group in the script file line: '%s'", ligne);
  }
  dprints("groupe : %s , type = %d , nbre neurones = %d , seuil = %f \n", def_groupe[no].no_name, type, nbre, seuil);

  e.key = def_groupe[no].no_name;
  dprints("!%s!\n", e.key);
  e.data = NULL;
  ep = hsearch(e, ENTER); /* creation / verification */
  if (ep == NULL)
  {
    EXIT_ON_ERROR("(read_one_group_promethe) hash table full for group '%s'", e.key);
  }
  if (ep->data != NULL && def_groupe[no].no != no) /* 2eme test pour le cas ou l'on relit plusieur fois le script */
  {
    EXIT_ON_ERROR(" Erreur le label %s du groupe %d a deja ete utilise par le groupe entre en no %d \n", e.key, nbre_groupes_lus, ((type_groupe* ) ep->data)->no);
  }

  ep->data = (ENTRY*) (&def_groupe[no]);
  /*printf("---no %d nom du groupe %s \n",no,def_groupe[no].no_name);*/
  def_groupe[no].no = no; /* pas present dans l'ancienne version etrange... */
  def_groupe[no].type = type;
  MY_IntAffect(def_groupe[no].nbre, nbre);
  MY_FloatAffect(def_groupe[no].seuil, seuil);

  read_one_group_second_part(f1, &(def_groupe[no]), first_comment, NULL);
#ifdef DEBUG
  memcpy(variable[0], MY_Int2Str(def_groupe[no].taillex), (strlen(MY_Int2Str(def_groupe[no].taillex))+1) * sizeof(char));
  memcpy(variable[1], MY_Int2Str(def_groupe[no].tailley), (strlen(MY_Int2Str(def_groupe[no].tailley))+1) * sizeof(char));
  printf("+++ gpe = %d, taillex dx= %s , tailley dy =%s \n", no, variable[0], variable[1]);
#endif

  nbre_groupes_lus++;
  if (nbre_groupes_lus >= nb_max)
  {
    EXIT_ON_ERROR("basic_parser:  ERREUR, la version actuelle ne marche qu'avec moins de %d groupes \n", nb_max);
  }
  return no;
}

/*-------------------------------------------------------------------------------------------*/

void write_one_group(FILE * f1, type_groupe * groupe, int comment, char* xyz)
{
  char variable[5][TAILLE_CHAINE];

  if (comment == 1) save_comment(f1, groupe->comment);
  memcpy(variable[0], MY_Int2Str(groupe->nbre), (strlen(MY_Int2Str(groupe->nbre)) + 1) * sizeof(char));
  memcpy(variable[1], MY_Float2Str(groupe->seuil), (strlen(MY_Float2Str(groupe->seuil)) + 1) * sizeof(char));
  /*printf("groupe no : %s \n", groupe->no_name);*/

  fprintf(f1, "groupe = %s , type = %d , nbre neurones = %s , seuil = %s\n", groupe->no_name, groupe->type, variable[0], variable[1]);
  /*     fprintf(f1, "groupe = %s , type = %d , nbre neurones = %s , seuil = %s\n", */
  /*             groupe->no_name, groupe->type, variable[0], variable[1]); */

  if(xyz!=NULL) fprintf(f1,"infos_xyz = %s\n",xyz);

  memcpy(variable[0], MY_Int2Str(groupe->taillex), (strlen(MY_Int2Str(groupe->taillex)) + 1) * sizeof(char));
  memcpy(variable[1], MY_Int2Str(groupe->tailley), (strlen(MY_Int2Str(groupe->tailley)) + 1) * sizeof(char));
  fprintf(f1, "taillex = %s , tailley = %s\n", variable[0], variable[1]);
  fprintf(f1, "learning rate = %s \n", MY_Float2Str(groupe->learning_rate));
  if (groupe->type == No_Hebb || groupe->type == No_SAW || groupe->type == No_Kmean_R || groupe->type == No_CTRNN2 || groupe->type == No_NLMS) fprintf(f1, "alpha = %s \n", MY_Float2Str(groupe->alpha));
  if (groupe->type == No_Winner || groupe->type == No_Winner_Macro || groupe->type == No_Sutton_Barto || groupe->type == No_PCR) fprintf(f1, "noise_level = %s \n", MY_Float2Str(groupe->noise_level));
  fprintf(f1, "simulation speed = %s \n", MY_Float2Str(groupe->simulation_speed));

  if (groupe->type == No_Winner_Colonne || groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif) fprintf(f1, "tolerance = %s\n", MY_Float2Str(groupe->tolerance));

  if (groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif || groupe->type == No_Macro_Colonne || groupe->type == No_Winner || groupe->type == No_Winner_Macro)
  {
    memcpy(variable[0], MY_Int2Str(groupe->dvp), (strlen(MY_Int2Str(groupe->dvp)) + 1) * sizeof(char));
    memcpy(variable[1], MY_Int2Str(groupe->dvn), (strlen(MY_Int2Str(groupe->dvn)) + 1) * sizeof(char));
    fprintf(f1, "dvp       = %s , dvn = %s\n", variable[0], variable[1]);
    fprintf(f1, "alpha     = %s \n", MY_Float2Str(groupe->alpha));
  }
  if (groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif || groupe->type == No_Macro_Colonne || groupe->type == No_Winner || groupe->type == No_Winner_Macro || groupe->type == No_SAW || groupe->type == No_Kmean_R || groupe->type == No_CTRNN2)
  {
    memcpy(variable[0], MY_Int2Str(groupe->dvp), (strlen(MY_Int2Str(groupe->dvp)) + 1) * sizeof(char));
    memcpy(variable[1], MY_Int2Str(groupe->dvn), (strlen(MY_Int2Str(groupe->dvn)) + 1) * sizeof(char));
    fprintf(f1, "nbre_de_1 = %s \n", MY_Float2Str(groupe->nbre_de_1));
  }
  if (groupe->type == No_PTM || groupe->type == No_PLG || groupe->type == No_Winner_Selectif || groupe->type == No_Macro_Colonne || groupe->type == No_Winner || groupe->type == No_Winner_Macro)
  {
    fprintf(f1, "sigma_f   = %s \n", MY_Float2Str(groupe->sigma_f));
  }

  if (groupe->type == No_Kohonen)
  {
    memcpy(variable[0], MY_Int2Str(groupe->dvp), (strlen(MY_Int2Str(groupe->dvp)) + 1) * sizeof(char));
    memcpy(variable[1], MY_Int2Str(groupe->dvn), (strlen(MY_Int2Str(groupe->dvn)) + 1) * sizeof(char));
    fprintf(f1, "dvp       = %s , dvn = %s\n", variable[0], variable[1]);
  }

  if (groupe->type == No_Granular)
  {
    fprintf(f1, "time_spectrum min = %s \n", MY_Float2Str(groupe->tolerance));
    fprintf(f1, "time_spectrum max = %s \n", MY_Float2Str(groupe->alpha));
  }

  fprintf(f1, "type2  = %s \n", MY_Int2Str(groupe->type2));

  if (strlen(groupe->nom) >= 1) fprintf(f1, "groupe = %s\n", groupe->nom);
  else fprintf(f1, "groupe = ???\n");

  fprintf(f1, "posx = %d , posy = %d\n", groupe->posx, groupe->posy);
  fprintf(f1, "reverse = %d\n", groupe->reverse);
  fprintf(f1, "p_posx = %d , p_posy = %d\n", groupe->p_posx, groupe->p_posy);
  fprintf(f1, "debug = %d\n", groupe->debug);

  fprintf(f1, "ech_temps = %d\n\n", groupe->ech_temps);
  /*fprintf(f2,"%d\n",groupe->reverse); */
}

/*--------------------------------------------------------------------------------------*/
/*  traduit le nom symbolique d'un lien en numero pour les manipulations de leto        */
int link_name_2_no(type_liaison * liaison, char *macro_name, struct hsearch_data *hashtab)
{
  char no_name[SIZE_NO_NAME];
  void *exist = NULL;

  /*recherche le numero des groupes relies par le lien */
  memcpy(no_name, liaison->depart_name, (strlen(liaison->depart_name) + 1) * sizeof(char));
  if (macro_name != NULL) sprintf(liaison->depart_name, "%s_%s", macro_name, no_name);

  /* recherche si le label a bien ete declare pour un groupe */
  exist = lookup_hash_table(hashtab, liaison->depart_name);

  if (exist == NULL) /* probleme: il n'existe pas ! */
  {
    PRINT_WARNING("Erreur le label depart: %s utilise dans le lien %s n'a pas ete declare dans un groupe\n", liaison->depart_name, "erreur du au copier/coller");/* copie_ligne */
    PRINT_WARNING("Verifier le nombre de lien est peut être incorect (trop grand)\n");/* copie_ligne */
    return 0;
  }
  liaison->depart = ((type_groupe*) exist)->no;

  dprints("link_name_2_no:::: %s groupe depart %d trouve  \n", liaison->depart_name, liaison->depart);

  memcpy(no_name, liaison->arrivee_name, (strlen(liaison->arrivee_name) + 1) * sizeof(char));
  if (macro_name != NULL) sprintf(liaison->arrivee_name, "%s_%s", macro_name, no_name);

  /*idem pour le groupe d'arrivee du lien */
  exist = lookup_hash_table(hashtab, liaison->arrivee_name);

  if (exist == NULL)
  {
    fprintf(stderr, "basic_parser: (link_name_2_no) Erreur le label arrivee: %s utilise dans le lien %s n'a pas ete declare dans un groupe\n", liaison->arrivee_name, "copie_ligne");/*copie_ligne*/
    return 0;
  }
  liaison->arrivee = ((type_groupe*) exist)->no;
  dprints("--- %s groupe arrivee %d trouve  \n", liaison->arrivee_name, liaison->arrivee);
  return 1;
}

/*--------------------------------------------------------------------------------------*/
void read_one_link(FILE * f1, type_liaison * liaison, char *macro_name, struct hsearch_data *hashtab)
{
  char ligne[TAILLE_CHAINE];
  /*char copie_ligne[TAILLE_CHAINE];*/
  char variable[5][TAILLE_CHAINE];
  type_noeud_comment *first_comment;
  type_noeud_comment *first_comment_save;
  long pos;
  int ret;

  first_comment = (liaison->comment); /* le champs comment est initialise a NULL */
  first_comment = read_line_with_comment(f1, first_comment, ligne);

  /*  printf("+%s\n",ligne);*/
  /*strcpy(copie_ligne,ligne);*/
  dprints("%s pointeur liaison = %p sizeof %d\n", __FUNCTION__, liaison, sizeof(*liaison));
  sscanf(ligne, "liaison entre %s et %s , type = %s , nbre = %s , norme = %s\n", liaison->depart_name, liaison->arrivee_name, variable[0], variable[1], variable[2]);
  dprints(" liaison depart= %s , arrivee =  %s | %s | %s | %s\n", liaison->depart_name, liaison->arrivee_name, variable[0], variable[1], variable[2]);
  if (strcmp(liaison->depart_name, "-1") == 0 && strcmp(liaison->arrivee_name, "-1") == 0)
  {
    liaison->depart = -1;
    liaison->arrivee = -1;
  } /* pour les liens micro-macro */
  else
  {
    if (link_name_2_no(liaison, macro_name, hashtab) == 0) exit(1);
  }

  liaison->type = atoi(variable[0]);
  Str2MY_Int(liaison->nbre, variable[1]);
  Str2MY_Float(liaison->norme, variable[2]);
  dprints("liaison entre %s et %s , type = %s , nbre = %s , norme = %s\n", liaison->depart_name, liaison->arrivee_name, variable[0], variable[1], variable[2]);
  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "                         temps de memorisation entree= %s \n", variable[0]);
  Str2MY_Float(liaison->temps, variable[0]);
  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "                         temps de memorisation sortie= %s \n", variable[0]);
  Str2MY_Float(liaison->stemps, variable[0]);

  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "                         mode de calcul        = %d \n", &liaison->mode);
  first_comment = read_line_with_comment(f1, first_comment, ligne);
  sscanf(ligne, "                         secondaire            = %d \n", &liaison->secondaire);
  if (liaison->type == No_l_algorithmique || liaison->type == No_l_neuro_mod || liaison->type == No_l_1_1_non_modif_bloqueur || liaison->type == No_l_1_patern_modif || liaison->type == No_l_1_patern_non_modif)
  {
    first_comment = read_line_with_comment(f1, first_comment, ligne);
    sscanf(ligne, "                         nom = %s\n", liaison->nom);
    dprints("liaison->nom = %s \n", liaison->nom);
  }
  else /* sprintf(liaison->nom,"%.3f",liaison->norme); */
  memcpy(liaison->nom, MY_Float2Str(liaison->norme), (strlen(MY_Float2Str(liaison->norme)) + 1) * sizeof(char));

  if (liaison->type == No_l_1_v || liaison->type == No_l_1_v_non_modif)
  {
    first_comment = read_line_with_comment(f1, first_comment, ligne);
    sscanf(ligne, "                         dv_x = %s\n", variable[0]);
    Str2MY_Float(liaison->dv_x, variable[0]);
    first_comment = read_line_with_comment(f1, first_comment, ligne);
    sscanf(ligne, "                         dv_y = %s\n", variable[0]);
    Str2MY_Float(liaison->dv_y, variable[0]);
  }

  pos = ftell(f1);
  first_comment_save = first_comment;
  first_comment = read_line_with_comment(f1, first_comment, ligne);
  ret = sscanf(ligne, "                         proba = %s \n", variable[0]);
  switch (ret)
  {
  case 1: /* variable trouvee */
    Str2MY_Float(liaison->proba, variable[0]);
    break;
  case 0: /* variable absente */
    fseek(f1, pos, SEEK_SET);
    first_comment = first_comment_save;
    break;
  default: /* variable trouvee mais pas de valeur */
    break;
  }
  liaison->comment = first_comment;
}

void write_one_link(FILE * f1, type_liaison * liaison, int comment)
{
  char variable[5][TAILLE_CHAINE];
  char num_depart[1024], num_arrivee[1024];
  int i;

  for (i = 0; i < 5; i++)
    memset(variable[i], 0, TAILLE_CHAINE * sizeof(char));

  memset(num_depart, 0, 1024 * sizeof(char));
  memset(num_arrivee, 0, 1024 * sizeof(char));

  if (comment == 1) save_comment(f1, liaison->comment);

  memcpy(variable[1], MY_Int2Str(liaison->nbre), (strlen(MY_Int2Str(liaison->nbre)) + 1) * sizeof(char));
  memcpy(variable[2], MY_Float2Str(liaison->norme), (strlen(MY_Float2Str(liaison->norme)) + 1) * sizeof(char));

  fprintf(f1, "liaison entre %s   et %s , type = %d , nbre = %s , norme = %s\n", liaison->depart_name, liaison->arrivee_name, liaison->type, variable[1], variable[2]);

  /*      fprintf(f1, "liaison entre %s   et %s , type = %d , nbre = %s , norme = %s\n",  */
  /*              liaison->depart_name, liaison->arrivee_name, liaison->type, variable[1], */
  /*              variable[2]); */

  fprintf(f1, "                         temps de memorisation entree= %s \n", MY_Float2Str(liaison->temps));
  fprintf(f1, "                         temps de memorisation sortie= %s \n", MY_Float2Str(liaison->stemps));
  fprintf(f1, "                         mode de calcul        = %d \n", liaison->mode);
  fprintf(f1, "                         secondaire            = %d \n", liaison->secondaire);
  if (liaison->type == No_l_algorithmique || liaison->type == No_l_neuro_mod || liaison->type == No_l_1_1_non_modif_bloqueur || liaison->type == No_l_1_patern_modif || liaison->type == No_l_1_patern_non_modif)
  {
    if (strlen(liaison->nom) >= 1) fprintf(f1, "                         nom = %s\n", liaison->nom);
    else
    {
      fprintf(f1, "                         nom = ???\n");
      printf("WARNING : the field name of link between groups %d and %d was empty \n", liaison->depart, liaison->arrivee);
    }
  }
  if (liaison->type == No_l_1_v || liaison->type == No_l_1_v_non_modif)
  {
    fprintf(f1, "                         dv_x = %s \n", MY_Float2Str(liaison->dv_x));
    fprintf(f1, "                         dv_y = %s \n", MY_Float2Str(liaison->dv_y));
  }
  fprintf(f1, "                         proba = %s \n", MY_Float2Str(liaison->proba));
}

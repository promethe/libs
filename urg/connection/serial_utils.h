#ifndef SERIAL_UTILS_H
#define SERIAL_UTILS_H

/*!
  \file
  \brief  Supplementary functions of serial sending and receiving

  \author Satofumi KAMIMURA

  $Id: serial_utils.h 143 2008-08-05 18:39:57Z satofumi $

  \todo Const is to be add to the input argument.
*/

#include "serial_t.h"


/*!
  \brief The line feed code is returned.

  \retval true If character is LF or CR
  \retval false If character is not LF or CR
*/
extern int serial_isLF(const char ch);


/*!
  \brief Skip the receive data

  ConnectionInterface::clear() とは、タイムアウト時間を指定して読み飛ばせる点が異なる

  \param[in,out] serial Structure of serial control
  \param[in] total_timeout Upper bound value of timeout  [msec]
  \param[in] each_timeout Upper bound value of timeout occur between reception of each data [msec]
*/
extern void serial_skip(serial_t *serial, int total_timeout,
                        int each_timeout);


/*!
  \brief Read upto line feed

  Add "\\0" at the end of character array and return

  \param[in,out] serial Structure of serial control
  \param[in] data Buffer to store received data
  \param[in] data_size_max Maximum size of receive buffer
  \param[in] timeout Time out [msec]

  \return Number of received bytes
*/
extern int serial_getLine(serial_t *serial,
                          char *data, int data_size_max, int timeout);

#endif /* !SERIAL_UTILS_H */

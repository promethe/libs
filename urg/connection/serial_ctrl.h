#ifndef C_SERIAL_CTRL_H
#define C_SERIAL_CTRL_H

/*!
  \file
  \brief Serial communication

  Serial Communication Interface 

  \author Satofumi KAMIMURA

  $Id: serial_ctrl.h 143 2008-08-05 18:39:57Z satofumi $
*/

#include "serial_t.h"
/*!
  \brief Connection

  \param[in,out] serial Struct of serial handle
  \param[in] device Connected device
  \param[in] baudrate Connected baud rate

  \retval 0 Normal
  \retval < 0 Error
*/
extern int serial_connect(serial_t *serial, const char *device, long baudrate);


/*!
  \brief Disconnect

  \param[in,out] serial Struct of serial handle
*/
extern void serial_disconnect(serial_t *serial);


/*!
  \brief Checks whether device is connected or not and returns the result.

  \param[in,out] serial Struct of serial handle

  \retval 1 Connected
  \retval 0 Disconnected
*/
extern int serial_isConnected(serial_t *serial);


/*!
  \brief Change baudrate

  \param[in,out] serial Struct of serial handle
  \param[in] baudrate Baud rate

  \retval 0 Normal
  \retval < 0 Error
*/
extern int serial_setBaudrate(serial_t *serial, long baudrate);


/*!
  \brief Communication

  \param[in,out] serial Struct of serial handle
  \param[in] data Data to be transmitted.
  \param[in] data_size Data size

  \retval >= 0 Transmitted data size
  \retval < 0 Error
*/
extern int urg_serial_send(serial_t *serial, const char *data, int data_size);


/*!
  \brief Reception

  \param[in,out] serial Struct of serial handle
  \param[in] data Buffer to store received data
  \param[in] data_size_max Maximum size of receive buffer
  \param[in] timeout Time out [msec]

  \retval >= 0 Received data size
  \retval < 0 Error
*/
extern int serial_recv(serial_t *serial,
                       char *data, int data_size_max, int timeout);


/*!
  \brief pushes a read byte back to  buffer

  \param[in,out] serial Struct of serial handle
  \param[in] ch pushed back character

  \attention Should NOT push the character back to buffer without reading the buffer.
*/
extern void serial_ungetc(serial_t *serial, char ch);


#endif /* !C_SERIAL_CTRL_H */

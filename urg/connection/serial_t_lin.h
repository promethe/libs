#ifndef C_SERIAL_T_LIN_H
#define C_SERIAL_T_LIN_H

/*!
  \file
  \brief serial handle structure (Linux, Mac)

  \author Satofumi KAMIMURA

  $Id: serial_t_lin.h 136 2008-07-29 10:27:20Z satofumi $
*/

#include <termios.h>


enum {
  SerialErrorStringSize = 256
};


/*!
  \brief serial handle structure
*/
typedef struct {

  int errno_;                                /*!< Error number*/
  char error_string_[SerialErrorStringSize]; /*!< Error message*/
  int fd_;                                   /*!< Communication resource*/
  struct termios sio_;                       /*!< Terminal control*/
  char last_ch_;                             /*!< pushed back character*/

} serial_t;

#endif /*! C_SERIAL_T_LIN_H */

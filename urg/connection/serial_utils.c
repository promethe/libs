/*!
  \file
  \brief Supplementary functions of serial sending and receiving

  \author Satofumi KAMIMURA

  $Id: serial_utils.c 542 2009-02-03 07:35:52Z satofumi $
*/

#include "serial_utils.h"
#include "serial_ctrl.h"

#include <stdio.h>
#include <ctype.h>


/* Check whether new line or not */
int serial_isLF(const char ch)
{
  return ((ch == '\r') || (ch == '\n')) ? 1 : 0;
}


/* Skipping of receive data */
void serial_skip(serial_t *serial, int total_timeout, int each_timeout)
{
  char recv_ch;

  /* Clear the character that is pushed back to buffer */
  serial->last_ch_ = '\0';

  if (each_timeout <= 0) {
    each_timeout = total_timeout;
  }

  while (1) {
    int n = serial_recv(serial, &recv_ch, 1, each_timeout);
    if (n <= 0) {
      break;
    }
  }
}


/* Read one line */
int serial_getLine(serial_t *serial, char* data, int data_size_max,
                   int timeout)
{
  /* Evaluate by reading one by one character. */
  int filled = 0;
  while (filled < data_size_max) {
    char recv_ch;
    int n = serial_recv(serial, &recv_ch, 1, timeout);
    if ((n <= 0) || serial_isLF(recv_ch)) {
      break;
    }
    data[filled++] = recv_ch;
  }
  if (filled == data_size_max) {
    --filled;
    serial_ungetc(serial, data[filled]);
  }
  data[filled] = '\0';

  return filled;
}

#ifndef C_SERIAL_T_H
#define C_SERIAL_T_H

/*!
  \file
  \brief serial handle structure

  \author Satofumi KAMIMURA

  $Id: serial_t.h 498 2009-01-21 23:05:49Z satofumi $
*/

#ifndef NO_DETECT_OS_H
#include "detect_os.h"
#endif

#if defined(WINDOWS_OS)
#include "serial_t_win.h"

#elif defined(LINUX_OS) || defined(MAC_OS)
#include "serial_t_lin.h"

#else
#include "serial_t_other.h"

#endif

#endif /* !C_SERIAL_T_H */

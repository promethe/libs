/*!
  \file
  \brief Serial Communication (Linux, Mac )

  Serial Communication Interface 


  \author Satofumi KAMIMURA

  $Id: serial_ctrl_lin.c 565 2009-02-09 05:45:14Z satofumi $
*/

#include "serial_errno.h"
#include "delay.h"
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>


enum {
  InvalidFd = -1
};


/* Connec */
int serial_connect(serial_t *serial, const char *device, long baudrate)
{
  int flags = 0;
  int ret = 0;
#ifndef MAC_OS
  enum { O_EXLOCK = 0x0 }; /* Linux では使えないのでダミーを作成しておく */
#endif
  serial->fd_ = open(device, O_RDWR | O_EXLOCK | O_NONBLOCK | O_NOCTTY);
  if (serial->fd_ < 0) {
    /* Fail to connect */
    strerror_r(errno, serial->error_string_, SerialErrorStringSize);
    return SerialConnectionFail;
  }

  flags = fcntl(serial->fd_, F_GETFL, 0);
  fcntl(serial->fd_, F_SETFL, flags & ~O_NONBLOCK);

  /* Initialization of serial communication */
  tcgetattr(serial->fd_, &serial->sio_);
  serial->sio_.c_iflag = 0;
  serial->sio_.c_oflag = 0;
  serial->sio_.c_cflag &= ~(CSIZE | PARENB | CSTOPB);
  serial->sio_.c_cflag |= CS8 | CREAD | CLOCAL;
  serial->sio_.c_lflag &= ~(ICANON | ECHO | ISIG | IEXTEN);

  serial->sio_.c_cc[VMIN] = 0;
  serial->sio_.c_cc[VTIME] = 0;

  /* Change baudrate */
  ret = serial_setBaudrate(serial, baudrate);
  if (ret < 0) {
    return ret;
  }

  /* Initialize the serial handle structure */
  serial->last_ch_ = '\0';

  return 0;
}


/* Disconnect */
void serial_disconnect(serial_t *serial)
{
  if (serial->fd_ >= 0) {
    close(serial->fd_);
    serial->fd_ = InvalidFd;
  }
}


int serial_isConnected(serial_t *serial)
{
  return ((serial == NULL) || (serial->fd_ == InvalidFd)) ? 0 : 1;
}


/* Baudrate setting */
int serial_setBaudrate(serial_t *serial, long baudrate)
{
  long baudrate_value = -1;

  switch (baudrate) {
  case 4800:
    baudrate_value = B4800;
    break;

  case 9600:
    baudrate_value = B9600;
    break;

  case 19200:
    baudrate_value = B19200;
    break;

  case 38400:
    baudrate_value = B38400;
    break;

  case 57600:
    baudrate_value = B57600;
    break;

  case 115200:
    baudrate_value = B115200;
    break;

  default:
#if 0
    snprintf(serial->error_message, SerialErrorStringSize,
             "No handle baudrate value: %ld", baudrate);
#endif
    return SerialSetBaudrateFail;
  }

  /* Change baudrate */
  cfsetospeed(&serial->sio_, baudrate_value);
  cfsetispeed(&serial->sio_, baudrate_value);
  tcsetattr(serial->fd_, TCSADRAIN, &serial->sio_);

  return 0;
}


/* Transmission */
int urg_serial_send(serial_t *serial, const char *data, int data_size)
{
  if (! serial_isConnected(serial)) {
    return SerialConnectionFail;
  }
  return write(serial->fd_, data, data_size);
}


static int waitReceive(serial_t* serial, int timeout)
{
  fd_set rfds;
  struct timeval tv;

  /* Set timeout*/
  FD_ZERO(&rfds);
  FD_SET(serial->fd_, &rfds);

  tv.tv_sec = timeout / 1000;
  tv.tv_usec = (timeout % 1000) * 1000;

  if (select(serial->fd_ + 1, &rfds, NULL, NULL, &tv) <= 0) {
    /* Timeout occurs */
    return 0;
  }
  return 1;
}


/* Reception */
int serial_recv(serial_t *serial, char* data, int data_size_max, int timeout)
{
  int filled;
  int read_n;
  int require_n;

  if (data_size_max <= 0) {
    return 0;
  }

  /* If pushed character(last character) is available in buffer,write out that character.*/
  filled = 0;
  if (serial->last_ch_ != '\0') {
    data[0] = serial->last_ch_;
    serial->last_ch_ = '\0';
    ++filled;
  }

  if (! serial_isConnected(serial)) {
    return SerialConnectionFail;
  }

  while (filled < data_size_max) {
    if ((timeout > 0) && (! waitReceive(serial, 0))) {
      /* Stimulate thread switching*/
      delay(1);
    }
    if (! waitReceive(serial, timeout)) {
      break;
    }

    require_n = data_size_max - filled;
    read_n = read(serial->fd_, &data[filled], require_n);
    if (read_n <= 0) {
      /* Read error. Returns with  the read content */
      break;
    }
    filled += read_n;
  }
  return filled;
}


/* pushes a byte back to  buffer */
void serial_ungetc(serial_t *serial, char ch)
{
  serial->last_ch_ = ch;
}

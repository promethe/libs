/*!
  \file
  \brief Serial Communication

  Serial Communication Interface 


  \author Satofumi KAMIMURA

  $Id: serial_ctrl.c 174 2008-08-25 22:38:38Z satofumi $
*/

#include "serial_ctrl.h"

#if defined(WINDOWS_OS)
/* Windows (win32)  */
#include "serial_ctrl_win.c"

#else
/* Linux, Mac  (Common) */
#include "serial_ctrl_lin.c"
#endif

/*!
  \file
  \brief Serial Communication (Windows)

  Serial Communication Interface 


  \author Satofumi KAMIMURA

  $Id: serial_ctrl_win.c 596 2009-02-19 10:35:40Z satofumi $

  \todo No need to set the timeout again, if the value of new timeout and previous timeout are same.
*/

#include "serial_ctrl.h"
#include "serial_errno.h"
#include "delay.h"
#include <stdio.h>


/* Connection */
int serial_connect(serial_t *serial, const char *device, long baudrate)
{
  char adjusted_device[16];

  /* Open COM port */
  _snprintf(adjusted_device, 16, "\\\\.\\%s", device);
  serial->hCom_ = CreateFileA(adjusted_device, GENERIC_READ | GENERIC_WRITE, 0,
			      NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (serial->hCom_ == INVALID_HANDLE_VALUE) {
    fprintf(stderr, "open failed: %s\n", device);
    return -1;
  }

  /* Update communication size */
  SetupComm(serial->hCom_, 4096 * 4, 4096);
  serial->readable_size_ = 0;

  /* Change baudrate */
  serial_setBaudrate(serial, baudrate);

  /* Initialize the serial handle structure */
  serial->last_ch_ = '\0';
  serial->current_timeout_ = 0;

  return 0;
}


/* Disconnect */
void serial_disconnect(serial_t *serial)
{
  if (serial->hCom_) {
    CloseHandle(serial->hCom_);
    serial->hCom_ = INVALID_HANDLE_VALUE;
  }
}


int serial_isConnected(serial_t *serial)
{
  return (serial->hCom_ == INVALID_HANDLE_VALUE) ? 0 : 1;
}


/* Change baudrate */
int serial_setBaudrate(serial_t *serial, long baudrate)
{
  DCB dcb;

  /* Seach fd from HandleTerminals and Change baudrate */

  GetCommState(serial->hCom_, &dcb);
  dcb.BaudRate = baudrate;
  dcb.ByteSize = 8;
  dcb.Parity = NOPARITY;
  dcb.fParity = FALSE;
  dcb.StopBits = ONESTOPBIT;
  SetCommState(serial->hCom_, &dcb);

  return 0;
}


/* Transmission */
int serial_send(serial_t *serial, const char *data, int data_size)
{
  DWORD n;

  if (! serial_isConnected(serial)) {
    return SerialConnectionFail;
  }

  WriteFile(serial->hCom_, data, (DWORD)data_size, &n, NULL);

  return n;
}


/* Reception */
int serial_recv(serial_t *serial, char* data, int data_size_max, int timeout)
{
  DWORD n;
  DWORD dwErrors;
  COMSTAT ComStat;
  int filled = 0;

  if (data_size_max <= 0) {
    return 0;
  }

  /*  If pushed character(last character) is available in buffer,write out that character. */
  filled = 0;
  if (serial->last_ch_ != '\0') {
    data[0] = serial->last_ch_;
    serial->last_ch_ = '\0';
    ++filled;
    --data_size_max;
  }

  if (! serial_isConnected(serial)) {
    return SerialConnectionFail;
  }

  /* If receiption of required data size is possible then read the data and return */
  if (serial->readable_size_ < data_size_max) {
    ClearCommError(serial->hCom_, &dwErrors, &ComStat);
    serial->readable_size_ = ComStat.cbInQue;
  }

  if (data_size_max > serial->readable_size_) {
    COMMTIMEOUTS pcto;
    int each_timeout = 2;

    if (timeout == 0) {
      /* Read only existing data and return */
      data_size_max = serial->readable_size_;

    } else {
      if (timeout < 0) {
	/* If timeout value is zero, then timeout dont work,
	   instead continue to wait untill data is received  */
	timeout = 0;
	each_timeout = 0;
      }

      /*  Set time out */
      if (timeout != serial->current_timeout_) {
        GetCommTimeouts(serial->hCom_, &pcto);
        pcto.ReadIntervalTimeout = timeout;
        pcto.ReadTotalTimeoutMultiplier = each_timeout;
        pcto.ReadTotalTimeoutConstant = timeout;
        SetCommTimeouts(serial->hCom_, &pcto);
        serial->current_timeout_ = timeout;
      }

      /* Stimulate thread switching */
      delay(1);
    }
  }
  ReadFile(serial->hCom_, &data[filled], (DWORD)data_size_max, &n, NULL);
  if (n > 0) {
    serial->readable_size_ -= n;
  }

  return (n + filled);
}


/* pushes a byte back to  buffer */
void serial_ungetc(serial_t *serial, char ch)
{
  serial->last_ch_ = ch;
}


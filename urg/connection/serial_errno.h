#ifndef C_SERIAL_ERRNO_H
#define C_SERIAL_ERRNO_H

/*!
  \file
  \brief Error definition of serial sending and receiving

  \author Satofumi KAMIMURA

  $Id: serial_errno.h 130 2008-07-25 06:00:54Z satofumi $
*/

enum {
  SerialNoError = 0,            /*!< No error */
  SerialNotImplemented = -1,    /*!< Not implemented */
  SerialConnectionFail = -2,    /*!< Connection error */
  SerialSendFail = -3,          /*!< Transmission error */
  SerialRecvFail = -4,          /*!< Reception error */
  SerialSetBaudrateFail = -5   /*!< Baudrate setting error */

  /* !!!*/
};

#endif /* !C_SERIAL_ERRNO_H */

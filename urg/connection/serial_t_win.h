#ifndef C_SERIAL_T_WIN_H
#define C_SERIAL_T_WIN_H

/*!
  \file
  \brief serial handle structure (Windows ����)

  \author Satofumi KAMIMURA

  $Id: serial_t_win.h 596 2009-02-19 10:35:40Z satofumi $
*/

#include <windows.h>


enum {
  SerialErrorStringSize = 256,
};


/*!
  \brief serial handle structure
*/
typedef struct {

  HANDLE hCom_;                 /*!< Communication resource */
  char last_ch_;                /*!< pushed back character */
  int readable_size_;           /*!< readable buffer size*/
  int current_timeout_;          /*!< Current timeout setting */

} serial_t;

#endif /*! C_SERIAL_T_LIN_H */

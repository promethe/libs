#ifndef C_MATH_UTILS_H
#define C_MATH_UTILS_H

/*!
  \file
  \brief 数学関数の補助ファイル

  \author Satofumi KAMIMURA

  $Id: math_utils.h 482 2009-01-19 13:38:18Z satofumi $
*/

#include "detect_os.h"
#if defined(WINDOWS_OS)
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#endif /* ! C_MATH_UTILS_H */

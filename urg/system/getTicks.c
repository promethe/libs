/*!
  \file
  \brief タイムスタンプ取得関数

  \author Satofumi KAMIMURA

  $Id: getTicks.c 412 2008-12-22 08:26:06Z satofumi $
*/

#include "getTicks.h"
#include "detect_os.h"
#if defined WINDOWS_OS
#include <time.h>
#else
#include <sys/time.h>
#include <stdio.h>
#endif


int getTicks(void)
{
  int ticks = 0;
  int global_ticks;
#if defined LINUX_OS
  /* Linux で SDL がない場合の実装。最初の呼び出しは 0 を返す*/
  static int first_ticks = 0;
  struct timeval tvp;
  gettimeofday(&tvp, NULL);
  global_ticks = tvp.tv_sec * 1000 + tvp.tv_usec / 1000;
  if (first_ticks == 0) {
    first_ticks = global_ticks;
  }
  ticks = global_ticks - first_ticks;

#else
  ticks = (int)(clock() / (CLOCKS_PER_SEC / 1000.0));
#endif
  return ticks;
}

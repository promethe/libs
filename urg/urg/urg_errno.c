/*!
  \file
  \brief Error code of URG

  \author Satofumi KAMIMURA

  $Id: urg_errno.c 554 2009-02-05 00:25:28Z satofumi $
*/

#include "urg_errno.h"


/* Returns error message */
const char* urg_strerror(int errno)
{

  const char *errorStr[] = {
    "No Error.",
    "Not Implemented.",
    "Send fail.",
    "Receive fail.",
    "SCIP1.1 protocol is not supported. Please update URG firmware.",
    "SS fail.",
    "Adjust baudrate fail.",
    "Invalid parameters.",
    "Urg invalid response.",
    "Serial connection fail.",
    "Serial receive fail.",
    "Response mismatch.",
    "No Response.",
    "dummy.",

    /* !!! */
  };

  if (errno > 0) {
    /* エラー応答以外の場合は、正常扱いにする */
    errno = 0;
  }

  return errorStr[-errno];
}

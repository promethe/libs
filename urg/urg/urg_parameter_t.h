#ifndef URG_PARAMETER_T_H
#define URG_PARAMETER_T_H

/*!
  \file
  \brief Parameter information of URG

  \author Satofumi KAMIMURA

  $Id: urg_parameter_t.h 534 2009-02-01 15:48:15Z satofumi $
*/


enum {
  UrgParameterLines = 8 + 1 + 1
};


/*!
  \brief Parameter Information of URG
*/
typedef struct {
  long distance_min_;		/*!< DMIN Information */
  long distance_max_;		/*!< DMAX Information */
  int area_total_;		/*!< ARES Information */
  int area_min_;		/*!< AMIN Information */
  int area_max_;		/*!< AMAX Information */
  int area_front_;		/*!< AFRT Information */
  int scan_rpm_;		/*!< SCAN ��� */
} urg_parameter_t;

#endif /* !URG_PARAMETER_T_H */

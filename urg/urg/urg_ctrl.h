#ifndef C_URG_CTRL_H
#define C_URG_CTRL_H

/*!
  \file
  \brief URG control

  \author Satofumi KAMIMURA

  $Id: urg_ctrl.h 534 2009-02-01 15:48:15Z satofumi $

  \todo Define examples for each functions
*/

/*#define USE_INTENSITY*/

#include "urg_t.h"


/*!
  \brief Parameter for object
*/
enum {
  UrgLineWidth = 64 + 1 + 1,    /*!< Maximum length of a line */
  UrgInfinityTimes = 0        /*!< continuous data transmission */
};


/*!
  \brief Command type of URG
*/
typedef enum {
  URG_GD,                       /*!< GD command */
  URG_GD_INTENSITY,             /*!< GD command(Inclusing intensity data) */
  URG_GS,                       /*!< GS command */
  URG_MD,                       /*!< MD command */
  URG_MD_INTENSITY,             /*!< MD command(Including intensity data) */
  URG_MS                       /*!< MS command */
} urg_request_type;


/*!
  \brief To omit URG data range specification
*/
enum {
  URG_FIRST = 10,          /*!< starting position when complete data is to be acquired  */
  URG_LAST = 744,           /*!< end position when complete data is to be acquired  */

  UrgInvalidTimestamp = -1     /*!< Error value of timestamp */
};


/*!
  \brief Connection

  \param[in,out] urg Structure of URG control
  \param[in] device Connection device
  \param[in] baudrate Baudrate

  \retval 0 Normal
  \retval <0 Error

  \see gd_scan.c, md_scan.c

  Usage example
\code
urg_t urg;

// Coonnection
if (urg_connect(&urg, "COM3", 115200) < 0) {
  printf("urg_connect: %s\n", urg_error(&urg));
  return -1;
}

...

urg_disconnect(&urg); \endcode
*/
extern int urg_connect(urg_t *urg, const char *device, long baudrate);


/*!
  \brief Disconnection

  \param[in,out] urg Structure of URG control

  \see urg_connect()
  \see gd_scan.c, md_scan.c
*/
extern void urg_disconnect(urg_t *urg);


/*!
  \brief Checks whether connected or not and returns the result

  \param[in,out] urg Structure of URG control

  \retval 0 if connected
  \retval <0 if disconnected

  \see urg_connect(), urg_disconnect()

  Usage example
\code
if (urg_isConnected(&urg) < 0) {
  printf("not connected.\n");
} else {
  printf("connected.\n");
} \endcode
*/
extern int urg_isConnected(urg_t *urg);


/*!
  \brief Get error message

  \param[in,out] urg Structure of URG control

  \return Error message

  \see urg_connect()
  \see gd_scan.c, md_scan.c
*/
extern const char *urg_error(urg_t *urg);


/*!
  \brief Get string containing version information

  \param[in,out] urg Structure of URG control
  \param[out] lines Buffer having version information
  \param[in] lines_max Maximum lines in buffer

  \retval 0 Normal
  \retval <0 Error

  \attention The length of a line in the buffer should be equal to more than # UrgLineWidth[byte].

  \see get_version_lines.c
*/
extern int urg_versionLines(urg_t *urg, char* lines[], int lines_max);


/*!
  \brief URG Returns parameter

  \param[in,out] urg Structure of URG control
  \param[out] parameters Structure of URG parameter

  \retval 0 Normal
  \retval <0 Error

  \see urg_maxDistance(), urg_minDistance(), urg_scanMsec(), urg_dataMax()
  \see get_parameters.c

  Execution example of get_parameters.c  (Classic-URG)
  \verbatim
% ./get_parameters
urg_getParameters: No Error.
distance_min: 20
distance_max: 5600
area_total: 1024
area_min: 44
area_max: 725
area_front: 384
scan_rpm: 600

urg_getDistanceMax(): 5600
urg_getDistanceMin(): 20
urg_getScanMsec(): 100
urg_getDataMax(): 726 \endverbatim
 */
extern int urg_parameters(urg_t *urg, urg_parameter_t* parameters);


/*!
  \brief Returns the number of maximum data obtained in one scan

  \param[in,out] urg Structure of URG control

  \retval >=0 number of maximum data obtained in one scan
  \retval <0 Error

  \see gd_scan.c

Usage example
\code
enum { BufferSize = 2048 };
long data[BufferSize];

...

// Checks whether number of maximum data obtained by URG sensor does not exceeds receive buffer
// (This is not necessary if size of buffer is dynamically allocated.)
int data_max = urg_dataMax(&urg);
ASSERT(BufferSize >= data_max);
\endcode
*/
extern int urg_dataMax(urg_t *urg);


/*!
  \brief Returns measurement time taken for one scan

  Returns measurement time when motor speed is 100% as specified.

  \param[in,out] urg Structure of URG control

  \retval >=0 measurement time taken for one scan [msec]
  \retval <0 Error

  \see urg_setMotorSpeed()

  \see md_scan.c
*/
extern int urg_scanMsec(urg_t *urg);


/*!
  \brief Maximum measurable distance

  \param[in,out] urg Structure of URG control

  \retval >=0 Maximum measurable distance [mm]
  \retval <0 Error

  \see expand_2d.c

Usage example
\code
...
n = urg_receiveData(&urg, data, data_max);

min_distance = urg_minDistance(&urg);
max_distance = urg_minDistance(&urg);

// Output only valid data
for (i = 0; i < n; ++i) {
  long length = data[i];
  if ((length > min_distance) && (length < max_distance)) {
    printf("%d:%d\n", i, length);
  }
}
\endcode
*/
extern long urg_maxDistance(urg_t *urg);


/*!
  \brief Minimum measureable distance

  \param[in,out] urg Structure of URG control

  \retval >=0 Minimum measurable distance [mm]
  \retval <0 Error

  \see expand_2d.c
*/
extern long urg_minDistance(urg_t *urg);


/*...................................*/


/*!
  \brief Sets the number of lines to be skiped.

  The volume of acquire data can be reduced by skipping the lines .

  \param[in,out] urg Structure of URG control
  \param[in] lines Number of lines to be skiped.

  \retval 0 Normal
  \retval < Error
*/
extern int urg_setSkipLines(urg_t *urg, int lines);


/*!
  \brief Sets number of scans to be skipped.

  \param[in,out] urg Structure of URG control
  \param[in] frames Number of skipped frames.

  \retval 0 Normal
  \retval <0 Error

  \attention Valid only with MD/MS command.
*/
extern int urg_setSkipFrames(urg_t *urg, int frames);


/*!
  \brief Sets  number of times the data to be acquired .

  \param[in,out] urg Structure of URG control
  \param[in] times Number of scan data

  \retval 0 Normal
  \retval <0 Error

  \attention Valid only with MD/MS command
  \attention Specify #UrgInfinityTimes to acquire data more than 100 times

  Usage example
  \code
// Data is supplied indefinitely
urg_setCaptureTimes(&urg, UrgInfinityTimes);

...

// Data acquistion is stopped if laser is switched off.
urg_laserOff(&urg);
  \endcode
*/
extern int urg_setCaptureTimes(urg_t *urg, int times);


/*!
  \brief MD/MS データ取得における残りスキャン数を取得

  \param[in,out] urg Structure of URG control

  \retval 残りスキャン数。ただし、無限回のデータ取得のときは 100 を返す

  \see md_scan.c
*/
extern int urg_remainCaptureTimes(urg_t *urg);


/*!
  \brief Request for distance data

  Request for distance data of [first_index, last_index].first_index, last_index にそれぞれ URG_FIRST, URG_LAST を指定することで、全範囲のデータ取得を行わせることができる。

  \param[in,out] urg Structure of URG control
  \param[in] request_type Received data type.
  \param[in] first_index Index of the first data stored
  \param[in] last_index Index of the last received data stored.

  \retval 0 Normal
  \retval <0 Error

  \see urg_receiveData()
  \see gd_scan.c, md_scan.c

Usage example
\code
// Get one scan data from GD command
urg_requestData(&urg, URG_GD, URG_FIRST, URG_LAST);
n = urg_receiveData(&urg, data, data_max);

// Get data continuously from MD scan
urg_requestData(&urg, URG_MD, URG_FIRST, URG_LAST);
while (1) {
  n = urg_receiveData(&urg, data, data_max);
  if (n > 0) {
    // Display data etc
    ...
  }
}
\endcode
*/
extern int urg_requestData(urg_t *urg,
                           urg_request_type request_type,
                           int first_index,
                           int last_index);


/*!
  \brief Receive URG data

  \param[in,out] urg Structure of URG control
  \param[out] data Storage location of received data
  \param[in] data_max Maximum number of data that can be received 

  \retval 0 > Number of data received
  \retval <0 Error

  \see urg_requestData()
*/
extern int urg_receiveData(urg_t *urg, long data[], int data_max);


#if defined(USE_INTENSITY)
/*!
  \brief Get data with intensity.

  \param[in,out] urg Structure of URG control
  \param[out] data Storage location of received data
  \param[in] data_max Maximum number of data that can be received
  \param[out] intensity Storage location of intensity of received data.

  \attention Applicable only to Classic-URG  (currently 2008-12-24)
*/
extern int urg_receiveDataWithIntensity(urg_t *urg, long data[], int data_max,
                                        long intensity[]);
#endif


/*!
  \brief Get partial URG data

  \param[in,out] urg Structure of URG control
  \param[out] data Storage location of received data
  \param[in] data_max Maximum number of data that can be received
  \param[in] first_index Index of the first data stored.
  \param[in] last_index Index of the last data stored

  \retval 0 > Number of data received
  \retval <0 Error

  \attention 未実装

  \see gd_scan.c, md_scan.c
*/
extern int urg_receivePartialData(urg_t *urg, long data[], int data_max,
                                  int first_index, int last_index);


/*!
  \brief Receive time stamp

  \param[in,out] urg Structure of URG control

  \retval Time stamp [msec]

  \see md_scan.c

Usage example
\code
urg_requestData(&urg, URG_GD, URG_FIRST, URG_LAST);
n = urg_receiveData(&urg, data, data_max);
if (n > 0) {
  long timestamp = urg_recentTimestamp(&urg);
  printf("timestamp: %d\n", timestamp);

  // Display data etc
  // !!!
}
\endcode
*/
extern long urg_recentTimestamp(urg_t *urg);


/*....................................*/


/*!
  \brief  Change index value into angle (radian)

  \image html urg_sensor_radian.png Front of the sensor is a positive in X axis

  \param[in,out] urg Structure of URG control
  \param[in] index Index value

  \return angle[radian]

  \see index_convert.c
*/
extern double urg_index2rad(urg_t *urg, int index);


/*!
  \brief Change index into angle(degree)

  \param[in,out] urg Structure of URG control
  \param[in] index Index value

  \return Angle [degree]

  \see index_convert.c
*/
extern int urg_index2deg(urg_t *urg, int index);


/*!
  \brief Angle(radian) is converted to index value

  \image html urg_sensor_radian.png Front of the sensor is a positive in X axis

  \param[in,out] urg Structure of URG control
  \param[in] Angle(radian)

  \return Index 

  \see index_convert.c
*/
extern int urg_rad2index(urg_t *urg, double radian);


/*!
  \brief Angle(degree) is converted into index

  \param[in,out] urg Structure of URG control
  \param[in] Angle(degre)

  \return Index value

  \see index_convert.c
*/
extern int urg_deg2index(urg_t *urg, int degree);


/*...........................................*/


/*!
  \brief Directs laser to switch on

  \param[in,out] urg Structure of URG control

  \retval 0 Normal
  \retval <0 Error

  \see gd_scan.c
*/
extern int urg_laserOn(urg_t *urg);


/*!
  \brief Directs laser to switch off

  \param[in,out] urg Structure of URG control

  \retval 0 Normal
  \retval <0 Error
*/
extern int urg_laserOff(urg_t *urg);


/*.........................................*/


/*!
  \brief Enters into time stamp mode

  \param[in,out] urg Structure of URG control

  \retval 0 Normal
  \retval <0 Error
*/
extern int urg_enableTimestampMode(urg_t *urg);


/*!
  \brief Comes out of time stamp mode

  \param[in,out] urg Structure of URG control

  \retval 0 Normal
  \retval <0 Error
*/
extern int urg_disableTimestampMode(urg_t *urg);


/*!
  \brief Get time stamp

  Returns TM1 response.

  \param[in,out] urg Structure of URG control

  \retval >=0 Timestamp [msec]
  \retval <0 Error

Usage example
\code
// Enters into time stamp
urg_enableTimestampMode(&urg);

// Get URG time stamp continuously.
for (i = 0; i < 5; ++i) {
  long timestamp = urg_currentTimestamp(&urg);
  printf("timestamp: %ld\n", timestamp)
}

// タイムスタンプモードから抜ける
urg_disableTimestampMode(&urg);
\endcode
*/
extern long urg_currentTimestamp(urg_t *urg);

#endif /* !C_URG_CTRL_H */

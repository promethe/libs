#ifndef C_URG_ERRNO_H
#define C_URG_ERRNO_H

/*!
  \file
  \brief Error code of URG

  \author Satofumi KAMIMURA

  $Id: urg_errno.h 554 2009-02-05 00:25:28Z satofumi $
*/


enum {
  UrgNoError = 0,               /*!< Normal */
  UrgNotImplemented = -1,       /*!< Not implemented */
  UrgSendFail = -2,
  UrgRecvFail = -3,
  UrgScip10 = -4,               /*!< Response from SCIP1.0  */
  UrgSsFail = -5,               /*!< Error in response from SS command */
  UrgAdjustBaudrateFail = -6,   /*!< Fails to adjust baudrate */
  UrgInvalidArgs = -7,          /*!< Invalid argument specification */
  UrgInvalidResponse = -8,      /*!< Response error from URG side */
  UrgSerialConnectionFail = -9, /*!< Fail to establish serial connection */
  UrgSerialRecvFail = -10,      /*!< シリアル接続に失敗 */
  UrgMismatchResponse = -11,    /*!< Mismatch in echoback in response */
  UrgNoResponse = -12          /*!< No response */


};


/*!
  \brief Returns error message

  \param[in] urg_errno Error value of URG

  \return error message
*/
extern const char* urg_strerror(int urg_errno);

#endif /* !C_URG_ERRNO_H */

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _GRAPHIC_TX_H
#define _GRAPHIC_TX_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif /* _GNU_SOURCE */
#include <search.h>
#include <semaphore.h>

#ifdef LIB_XVIEW
#include <X11/X.h>
#include <X11/Xlib.h>
#include <xview/xview.h>
#include <xview/frame.h>
#include <xview/panel.h>
#include <xview/font.h>
#include <xview/cms.h>
#include <xview/scrollbar.h>
#include <xview/xv_xrect.h>
#include <xview/scrollbar.h>
#include <xview/notice.h>
#define TxPoint XPoint
#define TxCouleur XColor
#else
#include <gtk/gtk.h>
#define TxPoint GdkPoint
#define TxRectangle GdkRectangle
#define TxCouleur GdkColor      /* comprend aussi le no de la couleur si il existe */
#define TxGraphic cairo_t
#define TxWidget GtkWidget
#endif

#define TxVide 0
#define TxPlein 1

#define TX_STYLE_DASH 0
#define TX_STYLE_PLAIN 1

#define noir 0
#define blanc 16
#define gris 8
#define rouge 17
#define vert 18
#define bleu 19
#define ciel 20
#define jaune 21
#define violet 22
#define marine 23
#define pelouse 24
#define bordeau 25
#define kaki 26

/*#define NB_Rectangle 2*/

#define MAX_VALUE_GDKCOLOR 65535
#define STRING_MAX 256



/*----------------------------------------------------------------------*/
/* le choix de l'initialisation depend du programme (choix des couleurs */
/*----------------------------------------------------------------------*/

extern int nb_max_couleurs; /* 34 utilisees dans couleurs[] */
extern TxCouleur couleurs[];


extern TxPoint un_point, point1, point2, point_courant, point_precedent;
extern int couleur, couleur2, couleur_fond, couleur_dessin, couleur_texte,
    coul_ombre1, coul_ombre2;
extern int lut_g[];
extern int lut_l[];
extern int nbre_couleurs;
extern int tab_couleur[], tab_gris[];
            /* numero des couleurs associes a la colormap couleurs */

/*---------------------------------------------------------------------*/
/* variables et structures liees au graphisme                          */
/*---------------------------------------------------------------------*/


#define GC_KEY 10

typedef struct TxDonneesFenetre
{
    char name[255];
    GtkWidget *window;
    GtkWidget *widget;
    GtkWidget *frame;
    GdkPixmap *pixmap;          /* dessin */
    GdkWindow *window_pixmap;
    GtkWidget *widget_pixmap;
    GtkWidget *box1;            /* boite dans laquelle on peut rajouter des VuMetre par ex. */
    GdkGC *gc;                  /* contexte graphique courant */
    GtkWidget *da;
  GtkAdjustment *adj_h,*adj_v;/* les pointeurs sur les ascenseurs */
    GdkFont *font;
    /*  GdkRectangle update_rect; *//* partie a mettre a jour */
    int xmin, xmax, ymin, ymax; /* partie a mettre a jour mais directement sous forme de bornes */
    /* permet de simplifier les calculs */
    GdkRegion *region_rectangle;    /*pour le double buffering ... si ca marche! */
    GdkColor *colormap;         /* tableau des couleurs */
    TxPoint curseur_courant;
    GtkWidget *text;            /* zone affichage texte */
    GtkWidget *entry;           /* une zone saisie de texte */
    GtkWidget *filew;           /* acces exploration repertoire fichiers */

    TxGraphic *graphic;                /* cairo context */
    cairo_surface_t *surface;   /* surface graphique pour cairo / liee au contexte cairo*/

    int width, height;          /* taille de la fenetre */

    /* GHashTable *gHashTable; */     /* table de hachage avec la lib gtk */
  struct hsearch_data *hashtab; /* table de hachage UNIX */

} TxDonneesFenetre;


/* structure permettant de definir 2 rectangles associes a 2 boutons dans une image */
typedef struct type_image_obj
{
  TxPoint start_pos1;
  TxPoint start_pos2;
  TxPoint end_pos1;
  TxPoint end_pos2;
  int etat;
  sem_t sem_fini;
}type_image_obj;


typedef struct TxChampsFormulaire
{
    int x, y;
    char nom_champs[255]; /*text du label*/
    char contenu[255];   /*text selectionne*/
    void (*entry_widget_callback) (GtkWidget * widget, GtkWidget * entry, void* script_gui);
    char event[255];
    GtkWidget *print_widget_entry; /*label*/
    GtkWidget *widget_entry; /*zone de text*/
    GtkWidget *fenetre;
    GtkWidget *table;
    GtkWidget *combo; /*combobox*/
    GList* list_items; /*la liste des items*/
    int nb_items; /*nombre d'items du combobox*/
} TxChampsFormulaire;



extern void TxSaveWindowImage(TxDonneesFenetre * une_fenetre, char *name);
extern void TxCopyWindowImage(TxDonneesFenetre *window, unsigned char *tab, int x, int y, int width, int height);
extern void TxLoadWindowImage(TxDonneesFenetre * une_fenetre, char *name);
extern void TxAfficheImageCouleur(TxDonneesFenetre * une_fenetre,
                                  unsigned char *im, int xmax, int ymax,
                                  int posx, int posy);

extern void TxAfficheImageNB(TxDonneesFenetre * une_fenetre,
                             unsigned char *im, int xmax, int ymax, int posx,
                             int posy);

/* a ne plus utiliser - compatibilité*/
extern void affiche_ng(TxDonneesFenetre * une_fenetre, unsigned char *im,
                       int xmax, int ymax, int effaceaire, int posx, int posy,
                       int if_inverse_video);

extern void TxDessinerRectangle(TxDonneesFenetre * une_fenetre,
                                int numero_couleur, int plein_vide,
                                TxPoint sup_gauche, unsigned int largeur,
                                unsigned int hauteur, unsigned int epaisseur);
                                
extern void TxDessinerRectangle_unsafe(TxDonneesFenetre * une_fenetre,
                                int numero_couleur, int plein_vide,
                                TxPoint sup_gauche, unsigned int largeur,
                                unsigned int hauteur, unsigned int epaisseur);
                                

extern void TxDessinerRectangle_color(TxDonneesFenetre * une_fenetre, 
				TxCouleur *color, int plein_vide,
				TxPoint sup_gauche, unsigned int largeur,
				unsigned int hauteur, unsigned int epaisseur);

/* NEVER USED - NEVER DEFINED ? */
extern void TxDessinerRectangle_color_modif(TxDonneesFenetre * une_fenetre,
                                int numero_couleur, int plein_vide,
                                TxPoint sup_gauche, unsigned int largeur,
                                unsigned int hauteur, unsigned int epaisseur);


/*-----------------------------------------------------------------------*/
/* dessine un point aux coordonnees contenues dans <local_un_point> et         */
/* dans la couleur  specifiee.                                           */
/*-----------------------------------------------------------------------*/

extern void TxDessinerPoint(TxDonneesFenetre * une_fenetre,
                            int numero_couleur, TxPoint local_un_point);


/*-----------------------------------------------------------------------*/
/* dessine un segment entre les points <origine> et <extremite> dans la  */
/*  couleur et epaisseur (en pixels) specifies.                          */
/*-----------------------------------------------------------------------*/


extern void TxDessinerSegment_rgb(TxDonneesFenetre * une_fenetre,
                              int numero_couleur, TxPoint origine,
                              TxPoint extremite, unsigned int epaisseur);

/*-----------------------------------------------------------------------*/
/* dessine un segment entre les points <origine> et <extremite> dans la  */
/*  couleur et epaisseur (en pixels) specifies.                          */
/*-----------------------------------------------------------------------*/


extern void TxDessinerSegment(TxDonneesFenetre * une_fenetre,
                              int numero_couleur, TxPoint origine,
                              TxPoint extremite, unsigned int epaisseur);

extern void TxDessinerSegmentb(TxDonneesFenetre * une_fenetre, TxPoint origine, TxPoint extremite);

/*------------------------------------------------------------------------------------*/
/* dessine un segment en pointille entre les points <origine> et <extremite> dans la  */
/*  couleur et epaisseur (en pixels) specifies.                                       */
/*------------------------------------------------------------------------------------*/


extern void TxDessinerSegmentPointille(TxDonneesFenetre * une_fenetre,
                              int numero_couleur, TxPoint origine,
                              TxPoint extremite, unsigned int epaisseur);

/*-----------------------------------------------------------------------*/
/* dessine un polygone dont les <nb_points> sommets sont contenus dans le
   tableau <tab_points>. Le polygone est rempli si <plein_vide> est egal a
   TxPlein, dans ce cas il est ferme meme si le dernier point n'est pas egal
   au premier. Si <plein_vide> est egal a TxVide, le polugone n'est pas ferme
   <et forme donc une ligne polygonale non fermee> si le dernier point est
   different du premier. */
/*-----------------------------------------------------------------------*/


extern void TxDessinerPolygone(TxDonneesFenetre * une_fenetre,
                               int numero_couleur, int plein_vide,
                               TxPoint tab_points[], unsigned int nb_points,
                               unsigned int epaisseur);

extern void TxDessinerSegmentBrise(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint *tab_points, unsigned int nb_points, unsigned int epaisseur);

extern void TxDessinerSegmentPointilleBrise(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint *tab_points, unsigned int nb_points, unsigned int epaisseur);


/*-----------------------------------------------------------------------*/
/* dessine un cercle ou un disque centre en <centre> et de rayon <rayon> avec
   les couleur et epaisseur specifies. */
/*-----------------------------------------------------------------------*/

extern void TxDessinerCercle(TxDonneesFenetre * une_fenetre,
                             int numero_couleur, int plein_vide,
                             TxPoint centre, int rayon, int epaisseur);

extern void TxDessinerCercle_rgb(TxDonneesFenetre * une_fenetre, int numero_couleur,
                                 int plein_vide, TxPoint centre, int rayon,
                                 int epaisseur);


/* -----------------------------------------------------------------------
   Dessine une fleche entre les points origin et destination
   -----------------------------------------------------------------------*/

void TxDessinerFleche_color(TxDonneesFenetre *window, TxCouleur *color, TxPoint origin, TxPoint destination, double thickness);

void TxDessinerFleche(TxDonneesFenetre *window, int color_index, TxPoint origin, TxPoint destination, double thickness);

void TxDessinerFleche_rgb(TxDonneesFenetre *window, int color_index, TxPoint origin, TxPoint destination, double thickness);

void TxDessinerFleche_rgb16M(TxDonneesFenetre *window, int color_index, TxPoint origin, TxPoint destination, double thickness);

/*-----------------------------------------------------------------------*/
/* renvoie la position finale de la derniere chaine ayant ete affichee   */
/* dans cette la TxDonneesFenetre donnee. Si aucune chaine n'a ete       */
/* affichee le point retourne est le point (0,0)                         */
/* (coin superieur gauche de la fenetre)                                 */
/*-----------------------------------------------------------------------*/

extern TxPoint TxCurseurCourant(TxDonneesFenetre * une_fenetre);


/*-----------------------------------------------------------------------*/

extern void TxEffacerAireDessin(TxDonneesFenetre * une_fenetre, int couleur_fond);

extern void TxEcrireChaine(TxDonneesFenetre * une_fenetre, int numero_couleur,
                           TxPoint local_un_point, const char *une_chaine,
                           char *fonte);

extern void TxEcrireChaineSized(TxDonneesFenetre * une_fenetre, int numero_couleur,
                                TxPoint local_un_point, char *une_chaine, int size,
                                char *fonte);

extern void TxFlushDessin(TxDonneesFenetre * fenetre1, int x, int y,
                          int largeur, int hauteur);

/* dessine une fleche horizontale */
extern void FlecheHorizontale(TxDonneesFenetre * fenetre, int local_couleur,
                              TxPoint origine, int largeur, int epaisseur);

/* dessine une fleche verticale */
extern void FlecheVerticale(TxDonneesFenetre * fenetre, int local_couleur,
                            TxPoint origine, int hauteur, int epaisseur);

extern void TracerAxe(TxDonneesFenetre * fenetre, int local_couleur1,
                      int local_couleur2, int local_x1, int local_y1,
                      int local_x2, int local_y2, int maxx, int maxy);

extern void affiche_coord(TxDonneesFenetre * fenetre, TxPoint point);

/*
extern void
gdk_window_iconify (GdkWindow *window);
*/


/* insersion de donnees accessibles en entrees sortie dans une IHM de type tableau
   fenetre : correspond a la fenetre a laquelle seront declares les elements
   formulaire: c'est le tableau correspondant permettant de stocker le formulaire (appartient a la fenetre)
   x,y : coordonnes ligne colone du champs a inserer. Le champs comprend 2 parties affichage et entree.
*/

extern void TxInsereChampsdansFormulaire(GtkWidget * fenetre,
                                         GtkWidget * table,
                                         TxChampsFormulaire * champs);

extern void TxUpdateChampsdansFormulaire(TxChampsFormulaire * champs,
                                         char *nouveau_contenu, int editable);

extern void TxResetDisplayPosition(TxDonneesFenetre * fenetre);
extern void TxFlush(TxDonneesFenetre * fenetre);

extern void TxDisplay_without_thread(TxDonneesFenetre * fenetre);
extern void TxDisplay(TxDonneesFenetre * fenetre);
extern void TxUpdateDisplay(TxDonneesFenetre * une_fenetre, int xmin, int ymin,
                     int xmax, int ymax);

extern void TxQuitter(void);

/*------------------------------------------------------------------*/
/* gestion des Checkbox                                             */
/*------------------------------------------------------------------*/

typedef struct TxCheckBox
{
    char nom[255];              /* nom de la checkbox */
    int *val;                 /* pointeur sur la variable globale qui sera modifiee par la checkbox */
    int nb_boxes;
    GtkWidget **widgets;             /* pointeur sur la checkbox consideree - initialise a NULL au depart */
} TxCheckBox;

extern void TxAddCheckBoxes(TxDonneesFenetre * fenetre, TxCheckBox * checkbox);

/*------------------------------------------------------------------*/
/* gestion des Vumetres                                             */
/*------------------------------------------------------------------*/

typedef struct TxFloatVuMetre
{
    char nom[255];              /* nom du vumetre */
    float *val;                 /* pointeur sur la variable globale qui sera modifiee par le vumetre */
    GtkRange *gtk_range;             /* pointeur sur le vumetre considere- initialise a NULL au depart */
} TxFloatVuMetre;

extern void TxAddVuMetre_opt(TxDonneesFenetre * fenetre,
                             TxFloatVuMetre * VuMetre, float min, float max,
                             float step);
extern void TxAddVuMetre(TxDonneesFenetre * fenetre,
                         TxFloatVuMetre * VuMetre);
extern void add_button(char *name, TxDonneesFenetre * fenetre,
                       void *fct(void), int param);

#define nb_color_max 19683
extern TxCouleur rgb_colormap[nb_color_max];
extern void TxDessinerRectangle_rgb(TxDonneesFenetre * une_fenetre,
                                    int numero_couleur, int plein_vide,
                                    TxPoint sup_gauche, unsigned int largeur,
                                    unsigned int hauteur,
                                    unsigned int epaisseur);

extern int colormap_2(float f);
extern int colormap_1(float f);
extern int rgb16M(int r,int g,int b);

extern void init_rgb_colormap16M(void);
extern void init_rgb_colormap(void);

extern void Tx_Set_RGB(TxDonneesFenetre *fenetre, TxCouleur *couleur);

extern void Tx_Set_Style_Trait(TxDonneesFenetre *fenetre, int style_trait, int epaisseur);

extern void TxDessinerCourbe(TxDonneesFenetre *une_fenetre, TxPoint origine, TxPoint pointa, TxPoint pointb, TxPoint extremite);
extern void TxDessinerCourbe5Points(TxDonneesFenetre *fenetre,TxPoint point1,TxPoint pointa, TxPoint pointb, TxPoint pointc,TxPoint point2);
extern void TxDessinerCourbeComplexe(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint *tab_points, unsigned int nb_points, unsigned int epaisseur);

extern void init_cairo(TxDonneesFenetre *fenetre);

extern void TxDessinerSegment_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur,
                       TxPoint origine, TxPoint extremite,
                       unsigned int epaisseur) ;

extern void TxDessinerSegmentPointille_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur,
                       TxPoint origine, TxPoint extremite,
                       unsigned int epaisseur);

extern void TxDessinerCercle_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur,
                      int plein_vide, TxPoint centre, int rayon,
                      int epaisseur);

extern void TxDessinerPoint_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur,
                       TxPoint local_un_point);

void TxDessinerRectangle_rgb16M(TxDonneesFenetre * une_fenetre,
                             int numero_couleur, int plein_vide,
                             TxPoint sup_gauche, unsigned int largeur,
                             unsigned int hauteur, unsigned int epaisseur);

extern gboolean fenetre_configure_event(TxDonneesFenetre * fenetre,
                                 GtkWidget * widget,
                                 GdkEventConfigure * event, gpointer data);

extern gboolean fenetre_expose_event(TxDonneesFenetre * fenetre, GtkWidget * widget,
                              GdkEventExpose * event, gpointer data);

void tx_printf(TxGraphic *my_graphic, TxPoint point, const char *format, ...);

#endif

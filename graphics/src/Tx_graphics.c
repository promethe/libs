/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <math.h>

#include <net_message_debug_dist.h>
#include <graphic_cairo_tools.h>
#include "graphic_Tx.h"

#define NUMBER_OF_16M_COLORS 16777216

/*#define DEBUG 1*/

/*
 gcc `pkg-config --cflags gtk+-2.0 gthread-2.0` -g Tx_graphics2.c -c

 pour leto ???
 gcc `pkg-config --cflags gtk+-2.0 gthread-2.0` -g Tx_graphics2.c -DTX_WITHOUT_THREADS -c



 gcc Tx_graphics2.c -g -o Tx_graphics2 -I/usr/include/gtk-2.0 -I/usr/lib/gtk-2.0/include -I/usr/include/atk-1.0 -I/usr/include/pango-1.0 -I/usr/X11R6/include -I/usr/include/freetype2 -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include   -lgtk-x11-2.0 -lgdk-x11-2.0 -latk-1.0 -lgdk_pixbuf-2.0 -lm -lpangoxft-1.0 -lpangox-1.0 -lpango-1.0 -lgobject-2.0 -lgmodule-2.0 -ldl -lglib-2.0  -lgthread
 */

/* Par defaut la librairie graphique est compilee pour fonctionner dans un environnement
 threade. Pour la version sans threads compiler avec -DTX_WITHOUT_THREADS */

int nb_max_couleurs = 37; /* utilisees dans couleurs[] */
TxCouleur couleurs[] =
  {
    { 0, 0, 0, 0 },
    { 1, 1600, 1600, 1600 },
    { 2, 3200, 3200, 3200 },
    { 3, 4800, 4800, 4800 },
    { 4, 6400, 6400, 6400 },
    { 5, 8000, 8000, 8000 },
    { 6, 9600, 9600, 9600 },
    { 7, 11200, 11200, 11200 },
    { 8, 12800, 12800, 12800 },
    { 9, 14400, 14400, 14400 },
    { 10, 16000, 16000, 16000 },
    { 11, 17600, 17600, 17600 },
    { 12, 19200, 19200, 19200 },
    { 13, 20800, 20800, 20800 },
    { 14, 22400, 22400, 22400 },
    { 15, 24000, 24000, 24000 },
    { 16, 65000, 65000, 65000 },
    { 17, 40000, 0, 0 }, /* 17 */
    { 18, 0, 40000, 0 }, /* indice 18 equ 4 dans leto */
    { 19, 0, 0, 40000 },
    { 20, 0, 40000, 40000 },
    { 21, 40000, 40000, 0 },
    { 22, 40000, 0, 40000 },
    { 23, 12800, 12800, 25500 },
    { 24, 12800, 25500, 12800 },
    { 25, 25500, 12800, 12800 },
    { 26, 6000, 20000, 5000 },
    { 27, 20000, 6000, 5000 },
    { 28, 6000, 5000, 20000 },
    { 29, 13000, 23000, 7000 },
    { 30, 6000, 23000, 17000 },
    { 31, 20000, 13000, 15000 },
    { 32, 13000, 20000, 16000 },
    { 33, 0, 20000, 25000 },
    { 34, 25000, 15000, 0 },
    { 35, 25000, 0, 15000 },
    { 36, 15000, 0, 25000 } };

TxCouleur rgb_colormap[nb_color_max];
TxCouleur rgb_colormap16M[NUMBER_OF_16M_COLORS];

int lut_g[] =
  { 18, 15, 28, 17, 19, 20, 21, 22, 23, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 };
int lut_l[] =
  { 18, 15, 28, 17, 19, 20, 21, 22, 23, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 };
TxPoint point_courant, point_precedent;

void init_rgb_colormap(void)
{
  int n, ech;
  float imax = 65535. / 2.;
  float w, ag, ab;
  float c, norme;

  w = 3.14159 / ((float) nb_color_max);
  ag = 3.14159 / 3.;
  ab = 2. * ag;
  for (n = 0; n < nb_color_max; n++)
  {
    rgb_colormap[n].pixel = n;
    c = cos(((float) n) * w);
    rgb_colormap[n].red = (int) (imax * (1. + c * c * c));
    c = cos(((float) n) * w - ag);
    rgb_colormap[n].green = (int) (imax * (1. + c * c * c));
    c = cos(((float) n) * w - ab);
    rgb_colormap[n].blue = (int) (imax * (1. + c * c * c));
    norme = 1. + rgb_colormap[n].red * rgb_colormap[n].red + rgb_colormap[n].green * rgb_colormap[n].green + rgb_colormap[n].blue * rgb_colormap[n].blue;
    ech = (int) (imax * imax * 4 * 3 / norme);
    rgb_colormap[n].red *= ech;
    rgb_colormap[n].green *= ech;
    rgb_colormap[n].blue *= ech;
    /*      printf("%d %d %d %d \n", rgb_colormap[n].pixel,rgb_colormap[n].red,rgb_colormap[n].green,rgb_colormap[n].blue); */
  }
}

void init_rgb_colormap16M()
{
  int need_to_generate_the_colormap;
  int ib, ir, ig, n = 0;
  static int init = 0;
  FILE *file_of_colormap;

  if (init == 1)
  {
    return;
  }

  init = 1;

  /** Pour gagner du temps on recherche si un fichier rgb_colormap16M existe.*/
  file_of_colormap = fopen("/tmp/rgb_colormap16M.bin", "r");
  need_to_generate_the_colormap = 1;
  if (file_of_colormap != NULL) /** Il existe on lit la carte des couleurs. */
  {
    if (fread(rgb_colormap16M, sizeof(TxCouleur), NUMBER_OF_16M_COLORS, file_of_colormap) == NUMBER_OF_16M_COLORS) need_to_generate_the_colormap = 0;
    else dprints("rgb_colormap16M.bin has not been read properly (this just slows donwn a bit the start up of Promethe but it is not normal).\n");
  }

  if (need_to_generate_the_colormap) /* Il n'existe pas ou a eu un probleme et on cree la carte des couleurs et on la sauvegrde pour la prochaine fois. */
  {
    for (ib = 0; ib < 256; ib++)
    {
      for (ig = 0; ig < 256; ig++)
      {
        for (ir = 0; ir < 256; ir++)
        {
          rgb_colormap16M[n].pixel = n;
          rgb_colormap16M[n].red = ir * 256;
          rgb_colormap16M[n].green = ig * 256;
          rgb_colormap16M[n].blue = ib * 256;
          n++;
        }
      }
    }
    file_of_colormap = fopen("/tmp/rgb_colormap16M.bin", "w");

    if (file_of_colormap == NULL) dprints("Cannot write on /tmp to cash rgb_colormap16M.bin (this just slows donwn a bit the start up of Promethe).\n");
    else fwrite(rgb_colormap16M, sizeof(TxCouleur), NUMBER_OF_16M_COLORS, file_of_colormap);
  }
  fclose(file_of_colormap);

  dprints("init_rgb_colormap16M(libgraphique): Ending initialization\n");
}

int colormap_2(float f)
{
  int r = (int) (128. * f) + 127;
  int g = (int) (64. * f);
  int b = (int) (32. * f);
  return rgb16M(r, g, b);
}
int colormap_1(float f)
{
  int r = 255;
  int g = (int) (255. * (1. - f));
  int b = 0;
  if (f < 0.3) b = (int) ((0.3 - f) / 0.3 * 255);
  if (f > 0.7) r = (int) ((1 - f) / 0.3 * 255);
  return rgb16M(r, g, b);
}

int rgb16M(int r, int g, int b)
{
  return (r + g * 256 + b * 256 * 256);
}

/************************************************************************/
/* mise a jour des bornes du cadre de la zone devant etre redessinee    */
/* min/max entre la zone precedente et celle avec l'ajout du nouvel ele.*/
/************************************************************************/
void TxUpdateDisplay(TxDonneesFenetre * une_fenetre, int xmin, int ymin, int xmax, int ymax)
{
  int temp;

  /* s'assurer avant que les points sont bien dans l'ordre voulu pour definir notre rectangle */
  if (xmax < xmin)
  {
    temp = xmax;
    xmax = xmin;
    xmin = temp;
  }
  if (ymax < ymin)
  {
    temp = ymax;
    ymax = ymin;
    ymin = temp;
  }

  if (xmin < une_fenetre->xmin) une_fenetre->xmin = xmin;
  if (ymin < une_fenetre->ymin) une_fenetre->ymin = ymin;

  if (xmax > une_fenetre->xmax) une_fenetre->xmax = xmax;
  if (ymax > une_fenetre->ymax) une_fenetre->ymax = ymax;

}

/*************************************************************************/
/* Affiche une image couleur dans la fenetre une_fenetre                 */
/* L'image est a l'adr im avec le codage R, G, B habituel sur 3 octets a */
/* la suite (format par defaut utilise dans les images de promethe.      */
/* xmax et ymax correspondent a la taille de l'image et posx posy a la   */
/* position du coin haut gauche de l'image dans la fenetre.              */
/* Penser a mettre un TxFlush(une_fenetre) a la suite pour forcer        */
/* l'affichage.                                                          */
/*************************************************************************/

void TxAfficheImageCouleur(TxDonneesFenetre * une_fenetre, unsigned char *im, int xmax, int ymax, int posx, int posy)
{

  TxUpdateDisplay(une_fenetre, posx, posy, posx + xmax, posy + ymax);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif
  if (!GDK_IS_GC(une_fenetre->gc)) une_fenetre->gc = gdk_gc_new(une_fenetre->window->window);
  gdk_draw_rgb_image(une_fenetre->pixmap, une_fenetre->gc, posx, posy, /* coord coin gauche image */
  xmax, ymax, GDK_RGB_DITHER_NONE, im, 3 * xmax);
  /* g_object_unref(G_OBJECT(une_fenetre->gc)); */
#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
  TxDisplay(une_fenetre);
  /*  TxFlush(une_fenetre); */
}

/* affichage d'une image en niveau de gris (idem que la couleur) */
void TxAfficheImageNB(TxDonneesFenetre * une_fenetre, unsigned char *im, int xmax, int ymax, int posx, int posy)
{

  TxUpdateDisplay(une_fenetre, posx, posy, posx + xmax, posy + ymax);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif
  if (!GDK_IS_GC(une_fenetre->gc)) une_fenetre->gc = gdk_gc_new(une_fenetre->window->window);
  gdk_draw_gray_image(une_fenetre->pixmap, une_fenetre->gc, posx, posy, /* coord coin gauche image */
  xmax, ymax, GDK_RGB_DITHER_NONE, im, xmax);
  /* g_object_unref(G_OBJECT(une_fenetre->gc)); */
#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
  TxDisplay(une_fenetre);
  /*  TxFlush(une_fenetre);*/
}

/* pour la compatibilite avec les vieilles fonctions - a ne plus utiliser */

void affiche_ng(TxDonneesFenetre * une_fenetre, unsigned char *im, int xmax, int ymax, int effaceaire, int posx, int posy, int if_inverse_video)
{
  (void) effaceaire;
  (void) if_inverse_video;

  TxAfficheImageNB(une_fenetre, im, xmax, ymax, posx, posy);
}

void Tx_Set_RGB(TxDonneesFenetre *fenetre, TxCouleur *couleur)
{
  cairo_t *cr;
  cr = fenetre->graphic;

  cairo_stroke(cr); /* impose l'affichage de ce qui precede */
  cairo_set_source_rgb(cr, couleur->red / 64500., couleur->green / 64500., couleur->blue / 64500.);
}

void Tx_Set_Style_Trait(TxDonneesFenetre *fenetre, int style_trait, int epaisseur)
{

  double dashes[] =
    { 10.0, /* ink */
    2.0, /* skip */
    2.0, /* ink */
    4.0 /* skip*/
    };
  int ndash = sizeof(dashes) / sizeof(dashes[0]);
  double offset = -50.0;
  cairo_t *cr;

  cr = fenetre->graphic;
  cairo_set_line_width(cr, (double) epaisseur);

  if (style_trait == 1) /* ligne pleine */
  {

    cairo_set_dash(cr, NULL, 0, 0);
  }
  else /* ligne pointillee*/
  {
    cairo_set_dash(cr, dashes, ndash, offset);
  }
}

void TxDessinerRectangle_color_unsafe(TxDonneesFenetre * une_fenetre, TxCouleur *color, int plein_vide, TxPoint sup_gauche, unsigned int largeur, unsigned int hauteur, unsigned int epaisseur)
{
	gboolean fill = TRUE;

	dprints("TxDessinerRectangle_color(libgraphique): Printing rectangle, fill = %i, width = %u, height = %u\n", plein_vide, largeur, hauteur);
	TxUpdateDisplay(une_fenetre, sup_gauche.x, sup_gauche.y, sup_gauche.x + largeur, sup_gauche.y + hauteur);

	if (!GDK_IS_GC(une_fenetre->gc))
	{
		une_fenetre->gc = gdk_gc_new(une_fenetre->window->window);
	}

	gdk_gc_set_rgb_fg_color(une_fenetre->gc, color);
	gdk_gc_set_line_attributes(une_fenetre->gc, epaisseur, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);

	if (plein_vide == TxVide)
	{
		fill = FALSE;
	}

	gdk_draw_rectangle(une_fenetre->pixmap, une_fenetre->gc, fill, sup_gauche.x, sup_gauche.y, largeur, hauteur);

}

void TxDessinerRectangle_color(TxDonneesFenetre * une_fenetre, TxCouleur *color, int plein_vide, TxPoint sup_gauche, unsigned int largeur, unsigned int hauteur, unsigned int epaisseur)
{
  gboolean fill = TRUE;

  dprints("TxDessinerRectangle_color(libgraphique): Printing rectangle, fill = %i, width = %u, height = %u\n", plein_vide, largeur, hauteur);
  TxUpdateDisplay(une_fenetre, sup_gauche.x, sup_gauche.y, sup_gauche.x + largeur, sup_gauche.y + hauteur);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  if (!GDK_IS_GC(une_fenetre->gc))
  {
    une_fenetre->gc = gdk_gc_new(une_fenetre->window->window);
  }

  gdk_gc_set_rgb_fg_color(une_fenetre->gc, color);
  gdk_gc_set_line_attributes(une_fenetre->gc, epaisseur, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);

  if (plein_vide == TxVide)
  {
    fill = FALSE;
  }

  gdk_draw_rectangle(une_fenetre->pixmap, une_fenetre->gc, fill, sup_gauche.x, sup_gauche.y, largeur, hauteur);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

void TxDessinerRectangle(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint sup_gauche, unsigned int largeur, unsigned int hauteur, unsigned int epaisseur)
{
  TxDessinerRectangle_color(une_fenetre, &couleurs[numero_couleur % nb_max_couleurs], plein_vide, sup_gauche, largeur, hauteur, epaisseur);
}

void TxDessinerRectangle_unsafe(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint sup_gauche, unsigned int largeur, unsigned int hauteur, unsigned int epaisseur)
{
	TxDessinerRectangle_color_unsafe(une_fenetre, &couleurs[numero_couleur % nb_max_couleurs], plein_vide, sup_gauche, largeur, hauteur, epaisseur);
}


void TxDessinerRectangle_rgb(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint sup_gauche, unsigned int largeur, unsigned int hauteur, unsigned int epaisseur)
{
  TxDessinerRectangle_color(une_fenetre, &rgb_colormap[numero_couleur % nb_color_max], plein_vide, sup_gauche, largeur, hauteur, epaisseur);
}

void TxDessinerRectangle_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint sup_gauche, unsigned int largeur, unsigned int hauteur, unsigned int epaisseur)
{
  TxDessinerRectangle_color(une_fenetre, &rgb_colormap16M[numero_couleur % NUMBER_OF_16M_COLORS], plein_vide, sup_gauche, largeur, hauteur, epaisseur);
}

/*-----------------------------------------------------------------------*/
/* dessine un point aux coordonnees contenues dans <local_un_point> et         */
/* dans la couleur  specifiee.                                           */
/*-----------------------------------------------------------------------*/

void TxDessinerPoint_color(TxDonneesFenetre * une_fenetre, TxCouleur *color, TxPoint local_point)
{
  dprints("TxDessinerPoint(libgraphique): Printing point, x = %i, y = %i\n", local_point.x, local_point.y);
  TxUpdateDisplay(une_fenetre, local_point.x, local_point.y, local_point.x, local_point.y);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  if (!GDK_IS_GC(une_fenetre->gc))
  {
    une_fenetre->gc = gdk_gc_new(une_fenetre->window->window);
  }

  gdk_gc_set_rgb_fg_color(une_fenetre->gc, color);
  gdk_draw_point(une_fenetre->pixmap, une_fenetre->gc, local_point.x, local_point.y);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

void TxDessinerPoint(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint local_un_point)
{
  TxDessinerPoint_color(une_fenetre, &couleurs[numero_couleur], local_un_point);
}

void TxDessinerPoint_rgb(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint local_un_point)
{
  TxDessinerPoint_color(une_fenetre, &rgb_colormap[numero_couleur], local_un_point);
}

void TxDessinerPoint_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint local_un_point)
{
  TxDessinerPoint_color(une_fenetre, &rgb_colormap16M[numero_couleur % NUMBER_OF_16M_COLORS], local_un_point);
}

/*-----------------------------------------------------------------------*/
/* Loads and saves window images                                         */
/*-----------------------------------------------------------------------*/

void TxSaveWindowImage(TxDonneesFenetre * une_fenetre, char *name)
{
  GError *error = NULL;
  /*TxUpdateDisplay(une_fenetre,local_un_point.x,local_un_point.y,local_un_point.x,local_un_point.y); */
  GdkPixbuf *dest;
#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif
  dest = gdk_pixbuf_get_from_drawable(NULL, une_fenetre->pixmap, gdk_drawable_get_colormap(une_fenetre->pixmap), 0, 0, 0, 0, -1, -1);
  gdk_pixbuf_save(dest, name, "jpeg", &error, "quality", "100", NULL);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif

  /*   gdk_window_invalidate_rect (une_fenetre->pixmap, */
  /* 			      &une_fenetre->update_rect, FALSE); */

  /* pas d'affichage si pas de invalidate... pas efficace ici */
}

void TxCopyWindowImage(TxDonneesFenetre *window, unsigned char *tab, int x, int y, int width, int height)
{
  static GdkPixbuf *dest = NULL;
  guchar *pixels;
  int  pixbuf_channels, pixbuf_rowstride;
  int i, j, k;

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  dest = gdk_pixbuf_get_from_drawable(dest, window->pixmap, gdk_drawable_get_colormap(window->pixmap), x, y, 0, 0, width, height);

  pixbuf_channels = gdk_pixbuf_get_n_channels(dest);
  pixbuf_rowstride = gdk_pixbuf_get_rowstride(dest);
  pixels = gdk_pixbuf_get_pixels(dest);

  /* We can't do a memcpy because apparently gtk sometimes adds extra padding
   pixels at the end of each row, so the rowstride isn't necessarily equal to width * n_channels
   Since we don't want these extra pixels, we do a manual buffer copy */
  for (i = 0; i < width; i++)
  {
    for (j = 0; j < height; j++)
    {
      for (k = 0; k < pixbuf_channels; k++)
      {
        tab[(i + j * width) * pixbuf_channels + k] = pixels[i * pixbuf_channels + j * pixbuf_rowstride + k];
      }
    }
  }

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

void TxLoadWindowImage(TxDonneesFenetre * une_fenetre, char *name)
{
  GError *error = NULL;
  /*TxUpdateDisplay(une_fenetre,local_un_point.x,local_un_point.y,local_un_point.x,local_un_point.y); */
  GdkPixbuf *dest;
#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif
  /*dest=gdk_pixbuf_get_from_drawable    (NULL,
   une_fenetre->pixmap,une_fenetre->gc
   gdk_drawable_get_colormap      ( une_fenetre-> pixmap ),
   0,
   0,
   0,
   0,
   -1,
   -1);
   printf("oooooooooooo\n");
   gdk_pixbuf_save               (dest,
   name,
   "jpeg",&error,"quality", "100", NULL
   );*/
  dest = gdk_pixbuf_new_from_file(name, &error);
  gdk_draw_pixbuf(une_fenetre->pixmap, une_fenetre->gc, dest, 0, 0, 0, 0, -1, -1, GDK_RGB_DITHER_NONE, 0, 0);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif

  /*   gdk_window_invalidate_rect (une_fenetre->pixmap, */
  /* 			      &une_fenetre->update_rect, FALSE); */

  /* pas d'affichage si pas de invalidate... pas efficace ici */
}

/*-----------------------------------------------------------------------*/
/* Prints a line.                                                        */
/*-----------------------------------------------------------------------*/

/* version simple sans changement de parametres d'ecriture: couleur / epaisseur */
void TxDessinerSegmentb(TxDonneesFenetre * une_fenetre, TxPoint origine, TxPoint extremite)
{
  cairo_t *cr;
  cr = une_fenetre->graphic;

  dprints("TxDessinerSegmentb(libgraphique): Printing line from x = %i, y = %i to x = %i, y = %i\n", origine.x, origine.y, extremite.x, extremite.y);
  TxUpdateDisplay(une_fenetre, origine.x, origine.y, extremite.x, extremite.y);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  cairo_move_to(cr, (double) origine.x, (double) origine.y);
  cairo_line_to(cr, (double) extremite.x, (double) extremite.y);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

void TxDessinerSegment_color(TxDonneesFenetre * une_fenetre, TxCouleur *color, TxPoint origine, TxPoint extremite, unsigned int epaisseur, int style)
{
#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  Tx_Set_RGB(une_fenetre, color);
  Tx_Set_Style_Trait(une_fenetre, style, epaisseur);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif

  TxDessinerSegmentb(une_fenetre, origine, extremite);
}

void TxDessinerSegment(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint origine, TxPoint extremite, unsigned int epaisseur)
{
  TxDessinerSegment_color(une_fenetre, &couleurs[numero_couleur], origine, extremite, epaisseur, 1);
}

void TxDessinerSegment_rgb(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint origine, TxPoint extremite, unsigned int epaisseur)
{
  TxDessinerSegment_color(une_fenetre, &rgb_colormap[numero_couleur], origine, extremite, epaisseur, 1);
}

void TxDessinerSegment_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint origine, TxPoint extremite, unsigned int epaisseur)
{
  TxDessinerSegment_color(une_fenetre, &rgb_colormap16M[numero_couleur % NUMBER_OF_16M_COLORS], origine, extremite, epaisseur, 1);
}

void TxDessinerSegmentPointille(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint origine, TxPoint extremite, unsigned int epaisseur)
{
  TxDessinerSegment_color(une_fenetre, &couleurs[numero_couleur], origine, extremite, epaisseur, 0);
}

void TxDessinerSegmentPointille_rgb(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint origine, TxPoint extremite, unsigned int epaisseur)
{
  TxDessinerSegment_color(une_fenetre, &rgb_colormap[numero_couleur], origine, extremite, epaisseur, 0);
}

void TxDessinerSegmentPointille_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint origine, TxPoint extremite, unsigned int epaisseur)
{
  TxDessinerSegment_color(une_fenetre, &rgb_colormap16M[numero_couleur % NUMBER_OF_16M_COLORS], origine, extremite, epaisseur, 0);
}

void TxDessinerSegmentBrise(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint *tab_points, unsigned int nb_points, unsigned int epaisseur)
{
  TX_DRAW_BEGIN
  utx_set_color(une_fenetre->graphic, &couleurs[numero_couleur]);
  Tx_Set_Style_Trait(une_fenetre, TX_STYLE_PLAIN, epaisseur);
  tx_draw_polylines(une_fenetre, tab_points, nb_points);
  TX_DRAW_END
}

void TxDessinerSegmentPointilleBrise(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint *tab_points, unsigned int nb_points, unsigned int epaisseur)
{
  TX_DRAW_BEGIN
  utx_set_color(une_fenetre->graphic, &couleurs[numero_couleur]);
  Tx_Set_Style_Trait(une_fenetre, TX_STYLE_DASH, epaisseur);
  tx_draw_polylines(une_fenetre, tab_points, nb_points);
  TX_DRAW_END
}

void TxDessinerFleche_color(TxDonneesFenetre *window, TxCouleur *color, TxPoint origin, TxPoint destination, double thickness)
{
  TX_DRAW_BEGIN
  utx_set_color(window->graphic, color);
  tx_set_line_thickness(window, thickness);
  tx_draw_arrow(window, origin, destination);
  TX_DRAW_END
}

void TxDessinerFleche(TxDonneesFenetre *window, int color_index, TxPoint origin, TxPoint destination, double thickness)
{
  TxDessinerFleche_color(window, &couleurs[color_index], origin, destination, thickness);
}

void TxDessinerFleche_rgb(TxDonneesFenetre *window, int color_index, TxPoint origin, TxPoint destination, double thickness)
{
  TxDessinerFleche_color(window, &rgb_colormap[color_index % nb_color_max], origin, destination, thickness);
}

void TxDessinerFleche_rgb16M(TxDonneesFenetre *window, int color_index, TxPoint origin, TxPoint destination, double thickness)
{
  TxDessinerFleche_color(window, &rgb_colormap16M[color_index % NUMBER_OF_16M_COLORS], origin, destination, thickness);
}

/*-----------------------------------------------------------------------*/
/* dessine un polygone dont les <nb_points> sommets sont contenus dans le
 tableau <tab_points>. Le polygone est rempli si <plein_vide> est egal a
 TxPlein, dans ce cas il est ferme meme si le dernier point n'est pas egal
 au premier. Si <plein_vide> est egal a TxVide, le polugone n'est pas ferme
 <et forme donc une ligne polygonale non fermee> si le dernier point est
 different du premier. */
/*-----------------------------------------------------------------------*/

void TxDessinerPolygone(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint tab_points[], unsigned int nb_points, unsigned int epaisseur)
{
#ifdef DEBUG
  printf("TxDessinerPolygone\n");
#endif
#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  (void) plein_vide;

  if (!GDK_IS_GC(une_fenetre->gc)) une_fenetre->gc = gdk_gc_new(une_fenetre->window->window);
  gdk_gc_set_rgb_fg_color(une_fenetre->gc, &couleurs[numero_couleur]);
  gdk_gc_set_line_attributes(une_fenetre->gc, epaisseur, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);

  gdk_draw_polygon(une_fenetre->pixmap, une_fenetre->gc, TRUE, tab_points, nb_points);

  /* g_object_unref(G_OBJECT(une_fenetre->gc)); */
#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif

}

/*-----------------------------------------------------------------------*/
/* dessine un cercle ou un disque centre en <centre> et de rayon <rayon> avec
 les couleur et epaisseur specifies. */
/*-----------------------------------------------------------------------*/

void TxDessinerCercle_color(TxDonneesFenetre * une_fenetre, TxCouleur *color, int plein_vide, TxPoint centre, int rayon, int epaisseur)
{
  gboolean fill = TRUE;

  dprints("TxDessinerCercle_color(libgraphique): Printing circle at x = %i, y = %i and radius = %i\n", centre.x, centre.y, rayon);
  TxUpdateDisplay(une_fenetre, centre.x - rayon, centre.y - rayon, centre.x + rayon, centre.y + rayon);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  if (!GDK_IS_GC(une_fenetre->gc))
  {
    une_fenetre->gc = gdk_gc_new(une_fenetre->window->window);
  }

  gdk_gc_set_rgb_fg_color(une_fenetre->gc, color);
  gdk_gc_set_line_attributes(une_fenetre->gc, epaisseur, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);

  if (plein_vide == TxVide)
  {
    fill = FALSE;
  }

  gdk_draw_arc(une_fenetre->pixmap, une_fenetre->gc, fill, centre.x - rayon, centre.y - rayon, rayon * 2, rayon * 2, 0, 360 * 64);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

void TxDessinerCercle(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint centre, int rayon, int epaisseur)
{
  TxDessinerCercle_color(une_fenetre, &couleurs[numero_couleur], plein_vide, centre, rayon, epaisseur);
}

void TxDessinerCercle_rgb(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint centre, int rayon, int epaisseur)
{
  TxDessinerCercle_color(une_fenetre, &rgb_colormap[numero_couleur % nb_color_max], plein_vide, centre, rayon, epaisseur);
}

void TxDessinerCercle_rgb16M(TxDonneesFenetre * une_fenetre, int numero_couleur, int plein_vide, TxPoint centre, int rayon, int epaisseur)
{
  TxDessinerCercle_color(une_fenetre, &rgb_colormap16M[numero_couleur % NUMBER_OF_16M_COLORS], plein_vide, centre, rayon, epaisseur);
}

/* dessine une courbe cairo ou un Z gdk */
void TxDessinerCourbe(TxDonneesFenetre *une_fenetre, TxPoint origine, TxPoint pointa, TxPoint pointb, TxPoint extremite)
{

  cairo_t *cr;
  cr = une_fenetre->graphic;

#ifdef DEBUG
  printf("TxDessinerCourbe \n");
#endif
  TxUpdateDisplay(une_fenetre, origine.x, origine.y, extremite.x, extremite.y);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  /* cairo_set_line_width (cr, (double) epaisseur);*/

  cairo_move_to(cr, (double) origine.x, (double) origine.y);
  cairo_curve_to(cr, (double) pointa.x, (double) pointa.y, (double) pointb.x, (double) pointb.y, (double) extremite.x, (double) extremite.y);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

void TxDessinerCourbe5Points(TxDonneesFenetre *fenetre, TxPoint point1, TxPoint pointa, TxPoint pointb, TxPoint pointc, TxPoint point2)
{

  cairo_t *cr;

  cr = fenetre->graphic;
  cairo_move_to(cr, (double) point1.x, (double) point1.y);
  cairo_curve_to(cr, (double) pointa.x, (double) pointa.y, (double) pointb.x, (double) pointb.y, (double) pointc.x, (double) pointc.y);
  cairo_line_to(cr, (double) point2.x, (double) point2.y);

}

/* dessine une courbe avec un nombre arbitraire de points */
/* avec 2 coudes, on a une droite                         */
void TxDessinerCourbeComplexe(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint *tab_points, unsigned int nb_points, unsigned int epaisseur)
{
  unsigned int i = 0;
  int reste;

  (void) epaisseur;

  TX_DRAW_BEGIN
  utx_set_color(une_fenetre->graphic, &couleurs[numero_couleur]);

  if (nb_points < 2)
  {
    printf("WARNING  TxDessinerCourbeComplexe affichage impossible nb_points=%d<2", nb_points);
    return;
  }
  else if (nb_points == 2)
  {
    tx_draw_line(une_fenetre, tab_points[0], tab_points[1]);
  }
  else if (nb_points == 3)
  {
    tx_draw_curve(une_fenetre, tab_points[0], tab_points[1], tab_points[1], tab_points[2]);
  }
  else
  {
    for (i = 3; i < nb_points; i += 3)
    {
      tx_draw_curve(une_fenetre, tab_points[i - 3], tab_points[i - 2], tab_points[i - 1], tab_points[i]);
    }

    reste = (nb_points - 1) % 3;

    if (reste > 0)
    {
      if (reste == 1)
      {
        tx_draw_line(une_fenetre, tab_points[nb_points - 2], tab_points[nb_points - 1]);
      }
      else if (reste == 2)
      {
        tx_draw_curve(une_fenetre, tab_points[nb_points - 3], tab_points[nb_points - 2], tab_points[nb_points - 2], tab_points[nb_points - 1]);
      }
    }
  }
  TX_DRAW_END
}

void tx_printf(TxGraphic *cr, TxPoint point, const char *format, ...)
{
  char string[STRING_MAX];
  va_list arguments;

  va_start(arguments, format);
  vsprintf(string, "format", arguments);
  TX_DRAW_BEGIN
  utx_print(cr, point, string);
  TX_DRAW_END
  va_end(arguments);
}

void tx_color_printf(TxGraphic *my_graphic, TxPoint point, int color_id, const char *format, ...)
{
  char string[STRING_MAX];
  va_list arguments;

  va_start(arguments, format);
  vsprintf(string, "format", arguments);
  TX_DRAW_BEGIN
  utx_set_color(my_graphic, &couleurs[color_id]);
  utx_print(my_graphic, point, string);
  TX_DRAW_END
  va_end(arguments);
}

void TxEcrireChaine(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint point, const char *une_chaine, char *fonte)
{
  cairo_t *cr;
  cr = une_fenetre->graphic;

  une_fenetre->curseur_courant.x = point.x;
  une_fenetre->curseur_courant.y = point.y;

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  cairo_save(cr);

  if (fonte != NULL && strcmp(fonte, "bold") == 0)
  {
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
  }
  else
  {
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  }

  gdk_cairo_set_source_color(cr, &couleurs[numero_couleur]);
  cairo_set_font_size(cr, 10.0);
  cairo_move_to(cr, (double) point.x, (double) point.y);
  cairo_show_text(cr, une_chaine);
  cairo_restore(cr);

  /* g_object_unref(G_OBJECT(une_fenetre->gc)); */
#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

void TxEcrireChaineSized(TxDonneesFenetre * une_fenetre, int numero_couleur, TxPoint point, char *une_chaine, int size, char *fonte)
{

  /*    cairo_text_extents_t extents;*/
  cairo_t *cr;
  (void) numero_couleur;
  (void) fonte;

  cr = une_fenetre->graphic;

#ifdef DEBUG
  printf("TxEcrireChaine\n");
#endif
  une_fenetre->curseur_courant.x = point.x;
  une_fenetre->curseur_courant.y = point.y;
#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);

  cairo_set_font_size(cr, (float) size);
  /*cairo_text_extents (cr, utf8, &extents);*/
  cairo_move_to(cr, (double) point.x, (double) point.y);
  cairo_show_text(cr, une_chaine);

  /* g_object_unref(G_OBJECT(une_fenetre->gc)); */
#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

/*-----------------------------------------------------------------------*/
/* renvoie la position finale de la derniere chaine ayant ete affichee   */
/* dans cette la TxDonneesFenetre donnee. Si aucune chaine n'a ete       */
/* affichee le point retourne est le point (0,0)                         */
/* (coin superieur gauche de la fenetre)                                 */
/*-----------------------------------------------------------------------*/

TxPoint TxCurseurCourant(TxDonneesFenetre * une_fenetre)
{
  return une_fenetre->curseur_courant;
}

/* dessine une fleche horizontale */
void FlecheHorizontale(TxDonneesFenetre * fenetre, int local_couleur, TxPoint origine, int largeur, int epaisseur)
{
  TxPoint triangle[3], extremite;
  extremite.x = origine.x + largeur;
  extremite.y = origine.y;
  triangle[2].x = extremite.x;
  triangle[2].y = extremite.y;
  if (largeur > 0) extremite.x -= 8;
  else extremite.x += 8;
  TxDessinerSegment(fenetre, local_couleur, origine, extremite, epaisseur);
  triangle[0].y = extremite.y - (3 + epaisseur);
  triangle[1].y = extremite.y + (3 + epaisseur);
  triangle[0].x = extremite.x;
  triangle[1].x = extremite.x;
  TxDessinerPolygone(fenetre, local_couleur, TxPlein, triangle, 3, epaisseur);
}

/* dessine une fleche verticale */
void FlecheVerticale(TxDonneesFenetre * fenetre, int local_couleur, TxPoint origine, int hauteur, int epaisseur)
{
  TxPoint triangle[3], extremite;
  extremite.x = origine.x;
  extremite.y = origine.y + hauteur;
  triangle[2].x = extremite.x;
  triangle[2].y = extremite.y;
  if (hauteur > 0) extremite.y -= 8;
  else extremite.y += 8;
  TxDessinerSegment(fenetre, local_couleur, origine, extremite, epaisseur);
  triangle[0].x = extremite.x - (3 + epaisseur);
  triangle[1].x = extremite.x + (3 + epaisseur);
  triangle[0].y = extremite.y;
  triangle[1].y = extremite.y;
  TxDessinerPolygone(fenetre, local_couleur, TxPlein, triangle, 3, epaisseur);
}

void TracerAxe(TxDonneesFenetre * fenetre, int local_couleur1, int local_couleur2, int local_x1, int local_y1, int local_x2, int local_y2, int maxx, int maxy)
{
  TxPoint or, point;
  int i, pasx;
  int chaine;
  (void) maxy;

  or.x = local_x1;
  or.y = local_y1;
  FlecheVerticale(fenetre, local_couleur1, or, local_y2 - local_y1, 0);
  FlecheHorizontale(fenetre, local_couleur2, or, local_x2 - local_x1, 0);
  pasx = (local_x2 - local_x1) / 5;

  if (local_y2 > local_y1) point.y = local_y1 - 5; /* ecrit en-dessus de l'axe horizontal */
  else point.y = local_y1 + 15; /* ecrit en-dessous de l'axe horizontal */
  for (i = 0; i <= 5; i++)
  {
    point.x = local_x1 + i * pasx;
    chaine = maxx / 10 * i;
    tx_color_printf(fenetre->graphic, point, local_couleur2, "%d", chaine);
  }
}

void affiche_coord(TxDonneesFenetre * fenetre, TxPoint point)
{
  TxPoint local_point2;
  local_point2.x = 10;
  local_point2.y = 0;
  TxDessinerRectangle(fenetre, noir, TxPlein, local_point2, 100, 18, 0);
  local_point2.y = 10;

  tx_color_printf(fenetre->graphic, local_point2, blanc, "%i", point.x);
  local_point2.x = 50;
  tx_color_printf(fenetre->graphic, local_point2, blanc, "%i", point.y);
}

void TxEffacerAireDessin(TxDonneesFenetre * une_fenetre, int couleur_fond)
{
  /* unused var
   gint x;
   gint y;
   gint depth;
   */
  TxPoint point;
#ifdef DEBUG
  printf("TxEffacerAireDessin\n");
#endif
  /* gdt_window_clear(une_fenetre->pixmap ); */

  /*gdk_window_get_geometry (une_fenetre->window->window, &x, &y, &une_fenetre->width, &une_fenetre->height,&depth); */
  /*plutot que de recuperer la geometrie / taille de la fenetre affichee, on utilise la taille de la fenetre stockee en memoire. */
  /* Elle peut etre beaucoup plus grande. */
  point.x = 0;
  point.y = 0;
  /*une_fenetre->width=3000, une_fenetre->height=3000; *//*adhoc il faudrait recuperer la bonne taille !!! cf fonctions de dessin */
  TxDessinerRectangle(une_fenetre, couleur_fond, TxPlein, point, une_fenetre->width, une_fenetre->height, 0);

}

/* insersion de donnees accessibles en entrees sortie dans une IHM de type tableau
 fenetre : correspond a la fenetre a laquelle seront declares les elements
 formulaire: c'est le tableau correspondant permettant de stocker le formulaire (appartient a la fenetre)
 x,y : coordonnes ligne colone du champs a inserer. Le champs comprend 2 parties affichage et entree.
 */

/*void TxInsereChampsdansFormulaire(GtkWidget *fenetre, GtkWidget *formulaire, int x, int y, char nom_champs[], char contenu[],
 GtkWidget **print_widget_entry, GtkWidget **widget_entry, void (*entry_widget_callback)())*/
void TxInsereChampsdansFormulaire(GtkWidget * fenetre, GtkWidget * table, TxChampsFormulaire * champs)
{
  GList *list;

  if (champs == NULL)
  {
    printf("(%s :) erreur TxChampsFormulaire * champs==NULL", __func__);
    exit(EXIT_FAILURE);
  }
  if (table == NULL)
  {
    printf("(%s :) erreur Table==NULL", __func__);
    exit(EXIT_FAILURE);
  }

  champs->fenetre = fenetre;
  champs->table = table;

  /*traitement du label*/
  if (champs->nom_champs != NULL) champs->print_widget_entry = gtk_label_new(champs->nom_champs);
  gtk_widget_ref(champs->print_widget_entry);
  gtk_object_set_data_full(GTK_OBJECT(fenetre), champs->nom_champs, champs->print_widget_entry, (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show(champs->print_widget_entry);
  gtk_table_attach(GTK_TABLE(table), champs->print_widget_entry, champs->x, champs->x + 1, champs->y, champs->y + 1, (GtkAttachOptions) (GTK_FILL), (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment(GTK_MISC(champs->print_widget_entry), 0, 0.5);
  if (champs->combo == (void*) -1) /* FIXME -1 should not be used with a pointer */
  {
    /*ne fait rien car traiter dans interface.c*/
    return;
  }
  /*traitement du combobox*/
  else if (champs->combo != NULL)
  {
    printf("combo pour %s\n", champs->nom_champs);
    champs->combo = gtk_combo_new();
    if (champs->combo == NULL)
    {
      printf("(%s :) erreur : cannot create combo %p", __func__, (gpointer) champs->combo);
      exit(EXIT_FAILURE);
    }
    g_object_ref(champs->combo);

    g_object_set_data_full(G_OBJECT(fenetre), champs->nom_champs, champs->combo, (GtkDestroyNotify) gtk_widget_unref);

    gtk_widget_show(champs->combo);
    gtk_table_attach(GTK_TABLE(table), champs->combo, champs->x + 1, champs->x + 2, champs->y, champs->y + 1, (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), (GtkAttachOptions) (0), 0, 0);

    /*rempli le combobox*/
    gtk_combo_set_popdown_strings(GTK_COMBO(champs->combo), champs->list_items/*items*/);
    list = champs->list_items;
    while (list)
    {
      GtkWidget* li = gtk_list_item_new_with_label((gchar *) list->data);
      gtk_container_add(GTK_CONTAINER(GTK_COMBO(champs->combo)->list), li);
      list = list->next;
    }

    gtk_widget_show(champs->combo);
    /*traitement de la zone de texte du combobox*/
    champs->widget_entry = GTK_COMBO(champs->combo)->entry;
    if (champs->print_widget_entry == NULL)
    {
      printf("(%s :) erreur : cannot create gtk_entry %p", __func__, (gpointer) champs->widget_entry);
      exit(EXIT_FAILURE);
    }
    gtk_widget_ref((GtkWidget *) GTK_ENTRY(GTK_COMBO(champs->combo)->entry));
    gtk_object_set_data_full(GTK_OBJECT(fenetre), champs->nom_champs, GTK_ENTRY(GTK_COMBO(champs->combo)->entry), (GtkDestroyNotify) gtk_widget_unref);
    if (champs->entry_widget_callback != NULL) g_signal_connect(G_OBJECT(GTK_ENTRY(GTK_COMBO(champs->combo)->entry/*champs->widget_entry*/)), champs->event /*"activate"*//*"changed"*/, G_CALLBACK(champs->entry_widget_callback), (gpointer) champs->widget_entry);
    /* gtk_widget_show(GTK_ENTRY(GTK_COMBO (champs->combo)->entry // champs->widget_entry//)); */

    if (champs->list_items != NULL)
    {

      gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(champs->combo)->entry/*champs->widget_entry*/), (gchar *) g_list_first(champs->list_items)->data);

    }
    champs->widget_entry = GTK_COMBO(champs->combo)->entry;
  }
  /*traitement pour le gtk_entry*/
  else
  {
    champs->widget_entry = gtk_entry_new();
    if (champs->print_widget_entry == NULL)
    {
      printf("(%s :) erreur : cannot create gtk_entryl %p", __func__, (gpointer) champs->widget_entry);
      exit(EXIT_FAILURE);
    }
    gtk_widget_ref(champs->widget_entry);
    gtk_object_set_data_full(GTK_OBJECT(fenetre), champs->nom_champs, champs->widget_entry, (GtkDestroyNotify) gtk_widget_unref);
    gtk_entry_set_max_length(GTK_ENTRY(champs->widget_entry), 250);
    if (champs->entry_widget_callback != NULL && strlen(champs->event) > 0) g_signal_connect(G_OBJECT(champs->widget_entry), champs->event, G_CALLBACK(champs->entry_widget_callback), (gpointer) champs->widget_entry);
    gtk_entry_set_activates_default(GTK_ENTRY(champs->widget_entry), TRUE);
    gtk_entry_set_text(GTK_ENTRY(champs->widget_entry), champs->contenu);
    gtk_editable_select_region(GTK_EDITABLE(champs->widget_entry), 0, GTK_ENTRY(champs->widget_entry)->text_length);
    gtk_widget_show(champs->widget_entry);
    gtk_table_attach(GTK_TABLE(table), champs->widget_entry, champs->x + 1, champs->x + 2, champs->y, champs->y + 1, (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), (GtkAttachOptions) (0), 0, 0);
  }

}

void TxUpdateChampsdansFormulaire(TxChampsFormulaire * champs, char *nouveau_contenu, int editable)
{
  float x;

  if (champs == NULL)
  {
    printf("(%s): erreur : champs=NULL \n", __func__);
    exit(EXIT_FAILURE);
  }
  /*    debug_printf("===entre dans (%s)=== champs-nom:%s \n",__func__,champs->nom_champs);*/
  if (champs->combo == NULL)
  {
    if (champs->widget_entry == NULL)
    {
      printf("(%s): erreur : champs->entry =NULL \n", __func__);
      exit(EXIT_FAILURE);
    }
    if (nouveau_contenu != NULL) gtk_entry_set_text(GTK_ENTRY(champs->widget_entry), nouveau_contenu);
    else printf("(%s): Warning: champ: %s nouveau_contenu null!\n ", __func__, champs->nom_champs);
    gtk_editable_select_region(GTK_EDITABLE(champs->widget_entry), 0, GTK_ENTRY(champs->widget_entry)->text_length);
    if (editable == 1)
    {
      gtk_editable_set_editable(GTK_EDITABLE(champs->widget_entry), TRUE);
      gtk_widget_set_sensitive(champs->widget_entry, TRUE);
    }
    else
    {
      gtk_editable_set_editable(GTK_EDITABLE(champs->widget_entry), FALSE);
      gtk_widget_set_sensitive(champs->widget_entry, FALSE);
      sscanf(nouveau_contenu, "%f", &x);
      if (editable == -1) gtk_entry_set_text(GTK_ENTRY(champs->widget_entry), "xxxxxxxxxxxx");
    }

    gtk_widget_show(champs->widget_entry);
  }
  else
  {
    if (nouveau_contenu != NULL) gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(champs->combo)->entry), nouveau_contenu);
    else if (champs->list_items != NULL)
    {
      gtk_combo_set_popdown_strings(GTK_COMBO(champs->combo), champs->list_items);

    }

    gtk_editable_select_region(GTK_EDITABLE(GTK_COMBO(champs->combo)->entry), 0, GTK_ENTRY(GTK_COMBO(champs->combo)->entry)->text_length);
    if (editable == 1)
    {
      gtk_editable_set_editable(GTK_EDITABLE(GTK_COMBO(champs->combo)->entry), TRUE);
      gtk_widget_set_sensitive(GTK_COMBO(champs->combo)->entry, TRUE);
    }
    else
    {
      gtk_editable_set_editable(GTK_EDITABLE(GTK_COMBO(champs->combo)->entry), FALSE);
      gtk_widget_set_sensitive(GTK_COMBO(champs->combo)->entry, FALSE);
      sscanf(nouveau_contenu, "%f", &x);
      if (editable == -1) gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(champs->combo)->entry), "xxxxxxxxxxxx");
    }
    /*gtk_widget_show(GTK_COMBO(champs->combo));*/

    gtk_widget_show(GTK_COMBO(champs->combo)->entry);

  }
  /*  debug_printf("===quitte (%s)===\n",__func__);*/
}

/* reset la position de la zone a afficher: aucune au debut ! */
void TxResetDisplayPosition(TxDonneesFenetre * fenetre)
{
  fenetre->xmin = fenetre->width;
  fenetre->ymin = fenetre->height;
  fenetre->xmax = fenetre->ymax = 0;
}

/*refresh de la zone visible sans flush force*/

void TxDisplay_without_thread(TxDonneesFenetre * fenetre)
{
  GdkRectangle update_rect;

  cairo_t *cr;
  cr = fenetre->graphic;

  update_rect.x = fenetre->xmin;
  update_rect.y = fenetre->ymin;
  update_rect.width = fenetre->xmax - fenetre->xmin;
  update_rect.height = fenetre->ymax - fenetre->ymin;
#ifdef DEBUG
  printf("TxDisplay %d %d %d %d \n", update_rect.x, update_rect.y,
      update_rect.width, update_rect.height);
#endif

  /*  Now invalidate the affected region of the drawing area. */
  gdk_window_invalidate_rect(fenetre->window_pixmap, &update_rect, FALSE);

  cairo_stroke(cr);

  /*remise a zero de la taille et position de la zone a afficher */
  TxResetDisplayPosition(fenetre);
}

void TxDisplay(TxDonneesFenetre * fenetre)
{
#ifdef DEBUG
  printf("TxDisplay\n");
#endif
#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  TxDisplay_without_thread(fenetre);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif

}

/* refresh de la zone specifiee */
void TxFlushDessin(TxDonneesFenetre * fenetre, int x, int y, int largeur, int hauteur)
{
  GdkRectangle update_rect;
  /* unused :
   int *statut;
   */

  cairo_t *cr;
  cr = fenetre->graphic;

#ifdef DEBUG
  printf("TxFlushDessin \n");
#endif
  update_rect.x = x;
  update_rect.y = y;
  update_rect.width = largeur;
  update_rect.height = hauteur;

  /* Now invalidate the affected region of the drawing area. */
  if (GDK_IS_WINDOW(fenetre->window_pixmap))
  {
    gdk_window_invalidate_rect(fenetre->window_pixmap, &update_rect, FALSE);
  }

  cairo_stroke(cr);

}

/* flush hard et long ? */
void TxFlush(TxDonneesFenetre * fenetre)
{
#ifdef DEBUG
  printf("TxFlush\n");
#endif

  TxDisplay(fenetre);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  gtk_widget_queue_draw(fenetre->da);

  /* wait(-1); *//* je suppose qu'il  faut juste attendre un fils un wait null suffit */
  /* wait(NULL);
   gdk_events_pending(); */

  /*une solution qui devrait aller plus vite que gdk_flush, a tester PG. */
  /*GdkDisplay *display = gdk_display_get_default ();
   XFlush (GDK_DISPLAY_XDISPLAY (display); */

  gdk_flush();

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif
}

/* Quitter l'application graphique */
void TxQuitter()
{
  gtk_main_quit();
}

/*--------------------------------------------------------------------------*/
/*   Gestion des VuMetres pour modifier en ligne des variables globales     */
/* Une fois le Vumetre ajoute, la variable golbale associee sera            */
/* automatiquement modifiee.                                                */
/*--------------------------------------------------------------------------*/

/* inscrit la valeur du vumetre qui vient d'etre modifie dans la variable associee*/
/* Fonction passee a la boucle de gestion des evenements par TxAddVuMetre         */
/* Le pointeur sur la variable globale a modifier est dans la structure VuMetre   */

void TxFloatVuMetre_get_value(GtkWidget * widget, TxFloatVuMetre * VuMetre)
{
  *(VuMetre->val) = gtk_range_get_value(VuMetre->gtk_range);
  (void) widget;

#ifdef DEBUG
  printf("%s=%f\n", VuMetre->nom, *(VuMetre->val));
#endif

}

/* inscrit la valeur de la checkbox qui vient d'etre modifiee dans la variable associee*/
/* Fonction passee a la boucle de gestion des evenements par TxAddCheckBox             */
/* Le pointeur sur la variable globale a modifier est dans la structure checkbox       */

void TxCheckBox_get_value(GtkWidget * widget, int * value)
{
  *value = (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) ? 1. : 0.);

#ifdef DEBUG
  printf("checkbox set to %i\n", *value);
#endif

}

void scale_set_default_values(GtkScale * scale)
{
  gtk_range_set_update_policy(GTK_RANGE(scale), GTK_UPDATE_CONTINUOUS);
  gtk_scale_set_digits(scale, 1);
  gtk_scale_set_value_pos(scale, GTK_POS_TOP);
  gtk_scale_set_draw_value(scale, TRUE);
}

typedef struct myStruct {
  int i;
} myStruct;

void add_button(char *name, TxDonneesFenetre * fenetre, void *fct(), int param)
{
  GtkWidget *button;
  GtkWidget *box1, *box2, *box3;
  GtkWidget *label;
  myStruct *p;
  (void) name;

  p = (myStruct *) malloc(sizeof(myStruct)); /*)g_new0 (myStruct, 1); */
  p->i = param;
  printf("ajout d'un bouton add_button %d\n", p->i);
  box1 = fenetre->box1;

  if (box1 == NULL)
  {
    EXIT_ON_ERROR("Ajout du vumetre impossible la box1 n'a pas ete definie pour la fenetre \n");
  }
  box2 = gtk_vbox_new(FALSE, 10);
  gtk_container_set_border_width(GTK_CONTAINER(box2), 10);
  gtk_box_pack_start(GTK_BOX(fenetre->box1), box2, FALSE, TRUE, 0);
  gtk_widget_show(box2);
  /*..... sous boite horizontale .................. */

  label = gtk_label_new("Mafct");
  gtk_box_pack_start(GTK_BOX(box2), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  box3 = gtk_hbox_new(FALSE, 10);
  gtk_container_set_border_width(GTK_CONTAINER(box3), 10);
  gtk_box_pack_start(GTK_BOX(box2), box3, FALSE, TRUE, 0);
  gtk_widget_show(box3);
  button = gtk_button_new_with_label("hel");
  g_signal_connect(GTK_OBJECT(button), "clicked", G_CALLBACK(fct),
  /*fenetre, */(gpointer) p);

  gtk_box_pack_start(GTK_BOX(box3), button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(button);
  gtk_widget_show(button);
}

/************************************************/
/* fenetre de controle des params de simulation */
/* le vumetre est rajoute a la box1             */
/************************************************/
void TxAddVuMetre_opt(TxDonneesFenetre * fenetre, TxFloatVuMetre * VuMetre, float min, float max, float step)
{
  GtkWidget *box1, *box2;
  GtkWidget *label;

  box1 = fenetre->box1;

  if (box1 == NULL)
  {
    EXIT_ON_ERROR("Ajout du vumetre impossible la box1 n'a pas ete definie pour la fenetre \n");
  }

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  /* creation de la boite pour le VuMetre dans la macro boite box1 */
  /* On inscrit d'abord le nom du VuMetre                          */
  box2 = gtk_hbox_new(FALSE, 1);
  gtk_container_set_border_width(GTK_CONTAINER(box2), 0);
  label = gtk_label_new(VuMetre->nom);
  gtk_box_pack_start(GTK_BOX(box2), label, FALSE, FALSE, 0);

  VuMetre->gtk_range = (GtkRange*) gtk_hscale_new_with_range(min, max, step);
  gtk_range_set_value(VuMetre->gtk_range, VuMetre->val[0]);

  g_signal_connect(GTK_OBJECT(VuMetre->gtk_range), "value-changed", G_CALLBACK(TxFloatVuMetre_get_value), VuMetre);
  gtk_box_pack_start(GTK_BOX(box2), (GtkWidget*) VuMetre->gtk_range, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX(box1), box2, FALSE, FALSE, 0);

  gtk_widget_show_all(box1);


#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif

}

/************************************************/
/* bouton de controle des params de simulation  */
/* la checkbox est rajoute a la box1            */
/************************************************/
void TxAddCheckBoxes(TxDonneesFenetre * fenetre, TxCheckBox * checkbox)
{
  int i;
  char box_name[256];
  GtkWidget *box1, *box2;
  GtkWidget *check_button;
  GtkWidget *boxes_label;
  GtkWidget *separator;

  box1 = fenetre->box1;
  if (box1 == NULL)
  {
    printf("Ajout de la checkbox impossible la box1 n'a pas ete definie pour la fenetre \n");
    exit(1);
  }

  /*--------      .    .    .    .     .    .    .    .  -----------*/
  /* ici on cree la checkbox et on la connecte a la structure de definition */
  /* pour pouvoir modifier la variable globale associee dans le callback   */

#ifndef TX_WITHOUT_THREADS
  gdk_threads_enter();
#endif

  box2 = gtk_hbox_new(FALSE, 10);
  gtk_container_set_border_width(GTK_CONTAINER(box2), 10);
  gtk_box_pack_start(GTK_BOX(box1), box2, FALSE, FALSE, 0);
  gtk_widget_show(box2);

  sprintf(box_name, "%s: ", checkbox->nom);
  boxes_label = gtk_label_new(box_name);
  gtk_box_pack_start(GTK_BOX(box2), boxes_label, FALSE, FALSE, 0);
  gtk_widget_show(boxes_label);

  for (i = 0; i < checkbox->nb_boxes; i++)
  {
    sprintf(box_name, "%i", i + 1);
    check_button = gtk_check_button_new_with_label(box_name);
    checkbox->widgets[i] = check_button;
    if (checkbox->val[i] > 0)
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_button), TRUE);
    }
    g_signal_connect(GTK_OBJECT(check_button), "toggled", G_CALLBACK(TxCheckBox_get_value), checkbox->val + i);

    gtk_box_pack_start(GTK_BOX(box2), check_button, FALSE, FALSE, 0);
    gtk_widget_show(check_button);
  }

  /*--------------.   .    .    .    .    .    .    .    . ---------*/
  /* insersion d'un separateur precedent l'ascenseur suivant par ex. */

  separator = gtk_hseparator_new();
  gtk_box_pack_start(GTK_BOX(box1), separator, FALSE, TRUE, 0);
  gtk_widget_show(separator);

#ifndef TX_WITHOUT_THREADS
  gdk_threads_leave();
#endif

}

/************************************************/
/* fenetre de controle des params de simulation */
/* le vumetre est rajoute a la box1             */
/************************************************/
void TxAddVuMetre(TxDonneesFenetre * fenetre, TxFloatVuMetre * VuMetre)
{
  TxAddVuMetre_opt(fenetre, VuMetre, 0.0, 1.0, 0.0001);
}

/* the actual function invoked to paint the canvas
 * widget, this is where most cairo painting functions
 * will go
 */

void init_cairo(TxDonneesFenetre *fenetre)
{
  cairo_t *cr;

  cr = fenetre->graphic;
  if (cr == NULL)
  {

    cr = gdk_cairo_create(fenetre->pixmap);
    fenetre->graphic = cr;
  }
}

/* Redraw the screen from the pixmap */
gboolean fenetre_expose_event(TxDonneesFenetre * fenetre, GtkWidget * widget, GdkEventExpose * event, gpointer data)
{
  (void) data;
  /* We use the "foreground GC" for the widget since it already exists,
   * but honestly any GC would work. The only thing to worry about
   * is whether the GC has an inappropriate clip region set.
   */
  /*
   #ifndef TX_WITHOUT_THREADS
   gdk_threads_enter ();
   #endif
   */
  gdk_draw_drawable(widget->window, widget->style->fg_gc[GTK_WIDGET_STATE(widget)], fenetre->pixmap,
  /* Only copy the area that was exposed. */
  event->area.x, event->area.y, event->area.x, event->area.y, event->area.width, event->area.height);
  /*
   #ifndef TX_WITHOUT_THREADS
   gdk_threads_leave ();
   #endif
   */

  return FALSE;
}

/* Create a new pixmap of the appropriate size to store our drawing - general pour toutes les fenetres */
gboolean fenetre_configure_event(TxDonneesFenetre * fenetre, GtkWidget * widget, GdkEventConfigure * event, gpointer data)
{
  (void) event;
  (void) data;
  /*  if (fenetre->pixmap)
   g_object_unref (G_OBJECT (fenetre->pixmap)); */

  /*
   #ifndef TX_WITHOUT_THREADS
   gdk_threads_enter ();
   #endif
   */
  if (fenetre->pixmap == NULL)
  {
    fenetre->pixmap = gdk_pixmap_new(widget->window, widget->allocation.width, widget->allocation.height, -1);
    fenetre->window_pixmap = widget->window;
    fenetre->widget_pixmap = widget;
    /* Initialize the pixmap to white */
    gdk_draw_rectangle(fenetre->pixmap, widget->style->white_gc, TRUE, 0, 0, widget->allocation.width, widget->allocation.height);

    init_cairo(fenetre);

  }

  /*
   #ifndef TX_WITHOUT_THREADS
   gdk_threads_leave ();
   #endif
   */

  fenetre->xmin = 0;
  fenetre->ymin = 0;
  fenetre->xmax = widget->allocation.width;
  fenetre->ymax = widget->allocation.height;
  /* printf("callback fenetre_configure_event\n"); */
  TxDisplay_without_thread(fenetre);
  /* printf("fin callback\n"); */

  /* We've handled the configure event, no need for further processing. */
  return TRUE;
}


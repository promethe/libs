/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * graphic_tools.c
 *
 *  Created on: 11 mars 2011
 *      Author: Arnaud BLanchard
 */

#include <gtk/gtk.h>
#include <graphic_Tx.h>
#include <math.h>


void utx_set_color(TxGraphic *cr, TxCouleur *color)
{
  cairo_stroke(cr); /* impose l'affichage de ce qui precede */
  gdk_cairo_set_source_color(cr, color);
}

void tx_set_line_thickness(TxDonneesFenetre *tx_window, double thickness)
{
  cairo_set_line_width(tx_window->graphic, thickness);
}

void tx_draw_line(TxDonneesFenetre *tx_window, TxPoint origine, TxPoint extremite)
{
  TxUpdateDisplay(tx_window, origine.x, origine.y, extremite.x, extremite.y);
  cairo_move_to(tx_window->graphic, (double) origine.x, (double) origine.y);
  cairo_line_to(tx_window->graphic, (double) extremite.x, (double) extremite.y);
}

void tx_draw_curve(TxDonneesFenetre *tx_window, TxPoint origine, TxPoint pointa, TxPoint pointb, TxPoint extremite)
{
  TxUpdateDisplay(tx_window, origine.x, origine.y, extremite.x, extremite.y);
  cairo_move_to(tx_window->graphic, (double) origine.x, (double) origine.y);
  cairo_curve_to(tx_window->graphic, (double) pointa.x, (double) pointa.y, (double) pointb.x, (double) pointb.y, (double) extremite.x, (double) extremite.y);
}

void tx_draw_polylines(TxDonneesFenetre *tx_window, TxPoint points[], int number_of_points)
{
  int i;

  cairo_move_to(tx_window->graphic, points[0].x, points[0].y);

  for (i = 1; i < number_of_points; i++)
  {
    TxUpdateDisplay(tx_window, points[0].x, points[0].y, points[i].x, points[i].y);
    cairo_line_to(tx_window->graphic, points[i].x, points[i].y);
  }
}

void tx_draw_arrow(TxDonneesFenetre *tx_window, TxPoint origin, TxPoint destination)
{
  int dy;
  float angle;
  cairo_t *cr = tx_window->graphic;

  TxUpdateDisplay(tx_window, origin.x, origin.y, destination.x, destination.y);

  cairo_move_to(cr, (double) origin.x, (double) origin.y);
  cairo_line_to(cr, (double) destination.x, (double) destination.y);
  cairo_stroke(cr);

  cairo_move_to(cr, (double) destination.x, (double) destination.y);
  if (origin.x == destination.x)
  {
    if (origin.y < destination.y)
    {
      dy = fmaxf(-20., 0.3 * (origin.y - destination.y));
    }
    else
    {
      dy = fminf(20., 0.3 * (destination.y - origin.y));
    }

    cairo_line_to(cr, (double) destination.x + dy * cos(7 * M_PI / 12), (double) destination.y + dy);
    cairo_line_to(cr, (double) destination.x + dy * cos(5 * M_PI / 12), (double) destination.y + dy);
  }
  else
  {
    angle = atan((destination.y - origin.y) / (float) (destination.x - origin.x));

    if (origin.x > destination.x)
    {
      angle += M_PI;
    }

    dy = fminf(20., 0.3 * sqrt((destination.y - origin.y) * (destination.y - origin.y) + (destination.x - origin.x) * (destination.x - origin.x)));

    cairo_line_to(cr, (double) destination.x + dy * cos(11 * M_PI / 12 + angle), (double) destination.y + dy * sin(11 * M_PI / 12 + angle));
    cairo_line_to(cr, (double) destination.x + dy * cos(13 * M_PI / 12 + angle), (double) destination.y + dy * sin(13 * M_PI / 12 + angle));
  }

  cairo_close_path(cr);
  cairo_fill(cr);
}


/* utx for unsafe */
void utx_print(TxGraphic *cr, TxPoint point, const char *string)
{
  cairo_move_to(cr, (double) point.x, (double) point.y);
  cairo_show_text(cr, string);
}

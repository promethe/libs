#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################
####################################################
#script permettant de generer un Makefile pour compiler la library graphique
#v2.0 Maillard M. & Baccon J.C.
#v3.0 Hirel J.
###################################################

####################################################
#definition de $CFLAGS $FLAGS_OPTIM $FLAGS_DEBUG
####################################################
source ../scripts/COMPILE_FLAG


####################################################
#Definition des chemins d'acces, options de compile etc...
####################################################
# Nom du programme
PROG_NAME_SHORT="graphique"
PROG_NAME=lib$PROG_NAME_SHORT

# Initialisation des libs, includes et flags
LIBS="$GTKLIB"
INCLUDES="$GTKINCLUDES -I$SIMULATOR_PATH -I$SIMULATOR_PATH/shared/include/ -I$PWD/include -I. -I$HOME/.local/include"
#CFLAGS definis dans le COMPILE_FLAGS

#############################################################################
#ATTENTION AUX FLAGS DANS CETTE LIB : AVEC -O3 LE CODE EST 3x PLUS LENTS DANS
#CERTAINES APPLIS... NE PAS MODIFIER -O2 NI LES FLAGS -> OPTIMISATIONS RALENTISSENT  M.M. gcc 3.3.x et sans doute d'autres versions...
###########################################################################

# Gestion des parametres passes au Create_Makefile
echo "$*" | grep -q "-enable-debug"
if [ $? -eq 0 ]
then echo "compile $PROG_NAME with debug"
    CFLAGS="$CFLAGS $FLAGS_DEBUG"
else echo "compile $PROG_NAME without debug"
    CFLAGS="$CFLAGS $FLAGS_OPTIM"	
fi

echo "$*" | grep -q "disable-thread"
if [ $? -eq 0 ]
then echo "compile $PROG_NAME with -disable-thread..."	
    CFLAGS="$CFLAGS -DTX_WITHOUT_THREADS"
fi


#Version finale des libs, includes et flags
FINALINCLUDES="$INCLUDES"
FINALLIBS="$LIBS"
FINALCFLAGS="$CFLAGS"

#Les repertoires de destination des fichiers compiles
LIBDIR="$SIMULATOR_PATH/lib/$SYSTEM/$PROG_NAME_SHORT"
OBJPATH="$OBJPATH/$PROG_NAME"
mkdir -p $OBJPATH

#Gestion des fichiers a compiler
SOURCES=`find src/ -depth -type f -name '*.c'`
OBJECTS=""

####################################################
#Creation du Makefile
####################################################

MAKEFILE="Makefile"

#ecrasement du Makefile precedent
echo "" > $MAKEFILE
#regle par defaut
echo "default: $PROG_NAME" >> $MAKEFILE
echo "" >> $MAKEFILE



# creer les regles
#pour chaque  .o
for i in $SOURCES
do
  echo "processing '$i'"
  FICHIER=`basename $i .c`
  CHEMIN=`echo $i | sed -e s@$FICHIER.c@@`
  echo "$OBJPATH/$FICHIER.o:$i" >> $MAKEFILE
  echo -e "\t@echo \"[processing $i...]\"">> $MAKEFILE
  echo -e "\t@(cd $CHEMIN; $CC $FINALCFLAGS  $FINALINCLUDES -c -o $OBJPATH/$FICHIER.o $FICHIER.c)" >> $MAKEFILE
  echo "" >> $MAKEFILE
  OBJECTS="$OBJECTS $OBJPATH/$FICHIER.o"
done

#pour l'edition de liens et le lien sur le binaire
echo "$PROG_NAME: $OBJECTS" >> $MAKEFILE
echo -e "\t@echo \"[making library $PROG_NAME ...]\"" >> $MAKEFILE
echo -e "\t@mkdir -p $LIBDIR" >> $MAKEFILE
echo -e "\t@$AR -rcv $LIBDIR/$PROG_NAME.a $OBJECTS" >> $MAKEFILE
echo "" >> $MAKEFILE

#r�gles additionnelles
echo "clean:" >> $MAKEFILE
echo -e "\trm -f  $OBJPATH/*.o " >> $MAKEFILE
echo "" >> $MAKEFILE

echo "reset:" >> $MAKEFILE
echo -e "\trm -f  $OBJDIR/*.o $LIBDIR/$PROG_NAME.a" >> $MAKEFILE
echo "" >> $MAKEFILE


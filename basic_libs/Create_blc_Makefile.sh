#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################
####################################################
#script permettant de generer un Makefile pour compiler la blc
#v1.0 P. Gaussier
###################################################
 
####################################################
#definition de $CFLAGS $FLAGS_OPTIM $FLAGS_DEBUG
####################################################
source ../scripts/COMPILE_FLAG

####################################################
#Definition des chemins d'acces, options de compile etc...
####################################################

# Nom du programme
PROJECT="blc"
CURRENT_PATH=`pwd -P`
cd ..
SIMULATOR_PATH=$PWD
cd $CURRENT_PATH

# Initialisation des libs, includes et flags
SOURCES=(blc_channel.cpp  blc_mem.cpp  blc_text.cpp  blc_tools.cpp  blc_realtime.cpp blc_image.cpp blc_network.cpp blc_text_graph.cpp blc_text_app.cpp)
SOURCES=${SOURCES[@]/#/blaa/blc/src/} #On ajoute blaa/blc/src/ avant chaque fichier source
ALL_CONFIGURATIONS=(debug release)

LIBS=""
INCLUDES="$GTKINCLUDES -I$SIMULATOR_PATH -I$SIMULATOR_PATH/shared/include/ -I$PWD/include -I$SIMULATOR_PATH/shared/include/Ivy -I$PWD/include/network -I../include"

#CFLAGS definis dans le COMPILE_FLAGS

ARGS=(`getopt --options h --long help, -- "$@"`)
if [ $? -ne 0 ]
then 
	echo "Use $0 -h to get some help." > /dev/stderr
	exit 2;
fi

for CONFIGURATION in ${ALL_CONFIGURATIONS[@]}
do
LIB_NAME=lib${PROJECT}

# Gestion des parametres passes au Create_Makefile
if [ $CONFIGURATION == "debug" ]
then echo "compile enable-debug..."
    CFLAGS="$CFLAGS $FLAGS_DEBUG"
     POSTFIX="_debug"   
else echo "compile disable-debug..."
    CFLAGS="$CFLAGS $FLAGS_OPTIM"
    POSTFIX=""
fi

LIB_NAME="${LIB_NAME}${POSTFIX}"

#Version finale des libs, includes et flags
FINALINCLUDES="$INCLUDES"
FINALLIBS="$LIBS "
FINALCFLAGS="$CFLAGS"

#Les repertoires de destination des fichiers compiles
OBJDIR="$OBJPATH/$LIB_NAME"
LIBDIR="$SIMULATOR_PATH/lib/$SYSTEM/blc"

#Gestion des fichiers a compiler
OBJECTS=""

####################################################
#Creation du Makefile
####################################################

MAKEFILE="Makefile.$LIB_NAME"

#ecrasement du Makefile precedent

#regle par defaut
echo "default: $LIB_NAME $LIB_NAME.$DYNAMIC_LIBRARY_SUFFIXE" > $MAKEFILE
echo "" >> $MAKEFILE
echo -e "include ../scripts/variables.mk\n" >> $MAKEFILE
echo "$OBJDIR:">> $MAKEFILE
echo -e "\tmkdir -p \$@" >> $MAKEFILE

#creer les regles
#pour chaque  .o
for i in $SOURCES
do
  echo "processing '$i'"
  FICHIER=`basename $i .cpp`
  CHEMIN=`echo $i | sed -e s@$FICHIER.cpp@@`
  echo "$OBJDIR/$FICHIER.o:$i | $OBJDIR " >> $MAKEFILE
  echo -e "\t@echo \"[processing $i...]\"">>$MAKEFILE
echo -e "\t@(cd $CHEMIN; $CC $FINALCFLAGS $FINALINCLUDES -fPIC -c -o $OBJDIR/$FICHIER.o $FICHIER.cpp -DSVN_REVISION='\$(shell svnversion -n .)' )" >> $MAKEFILE
  echo "" >> $MAKEFILE
  OBJECTS="$OBJECTS $OBJDIR/$FICHIER.o"
done

#pour l'edition de liens et le lien sur le binaire
echo "$LIB_NAME: $OBJECTS" >> $MAKEFILE
echo -e "\t@echo \"[making Promethe blc library...]\"" >> $MAKEFILE
echo -e "\t@mkdir -p $LIBDIR" >> $MAKEFILE
echo -e "\t@$AR -rcv $LIBDIR/$LIB_NAME.a $OBJECTS" >> $MAKEFILE
echo "" >> $MAKEFILE

echo "$LIB_NAME.$DYNAMIC_LIBRARY_SUFFIXE: $OBJECTS " >> $MAKEFILE
echo -e "\t$CC $CFLAGS_DYNAMIC_LIBRARY $OBJECTS -o $LIBDIR/$LIB_NAME.$DYNAMIC_LIBRARY_SUFFIXE $LIBS -lstdc++ -lrt" >> $MAKEFILE
echo -e	"\tcp -f $LIBDIR/$LIB_NAME.$DYNAMIC_LIBRARY_SUFFIXE $DIR_LIB_LETO_PROM/$LIB_NAME.$DYNAMIC_LIBRARY_SUFFIXE" >> $MAKEFILE
echo "" >> $MAKEFILE

#regles additionnelles
echo "clean:" >> $MAKEFILE
echo -e "\trm -f  $OBJDIR/*.o" >> $MAKEFILE
echo "" >> $MAKEFILE

echo "reset:clean" >> $MAKEFILE
echo -e "\trm -f  $OBJDIR/*.o $LIBDIR/$LIB_NAME.a" >> $MAKEFILE
echo "" >> $MAKEFILE

done


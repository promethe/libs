#!/bin/sh
# Basic Libraries And Applications (blaa)
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

#Default values

install_prefix="~/.local"
current_dir=`pwd`
parent_dir=`dirname $current_dir`
build_dir="$parent_dir/`basename $current_dir`_build"

usage()
{
    echo "Usage: install [options] project_directory"
    echo "    -b directory            :Directory to create the build. (default: $build_dir)"
    echo "    -h                      :Display this help."
    echo "    -i directory            :Directory to install the binaries. Default: $install_prefix"
    echo
}

set -- $(getopt b:hi: "$@")
while [ $# -gt 0 ]
do
    case "$1" in
    (-b) build_dir="$2"; shift;;
    (-h) usage;exit 0;;
    (-i) install_prefix="$2"; shift;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*)  break;;
    esac
    shift
done


if (($# == 0 ))
then usage; exit 1;
else
echo
echo " Install $1"
echo "=========================="

mkdir -p $build_dir/${1}_build && cd $build_dir/${1}_build && cmake -q $current_dir/$1 -DCMAKE_INSTALL_PREFIX=$install_prefix -DCMAKE_BUILD_TYPE="Release" && make && make install

if [ $? -ne 0 ]
then 
echo -e "\nFail installing $1\n"
else 
echo -e "\nSuccess installing $1 in $install_prefix .\n"
fi

cd $current_path
fi


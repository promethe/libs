#!/bin/sh

install_prefix="~/.local"
current_dir=`pwd`
parent_dir=`dirname $current_dir`
project_dir="$current_dir/$1"


build_dir="$parent_dir/`basename $current_dir`_build"/$1
mkdir -p $build_dir && cd $build_dir && cmake $project_dir -DCMAKE_INSTALL_PREFIX=$install_prefix -DCMAKE_BUILD_TYPE="Release" && make doc

if [ $? ] ; then
if type xdg-open 2>/dev/null; then xdg-open "$build_dir/html/index.html";
elif type open 2>/dev/null; then open "$build_dir/html/index.html";
else echo "Open documentation in: $build_dir/html/index.html"; fi
else
echo "Failing building the documentation"
fi





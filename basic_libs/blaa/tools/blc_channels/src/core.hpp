//
//  tools.hpp
//  blc_channels
//
//  Created by Arnaud Blanchard on 24/09/2015.
//
//

#ifndef core_hpp
#define core_hpp

#include "blc_channel.h"

extern int channels_nb;
extern blc_channel_info *channels_infos;
extern blc_channel **channels_addresses;

blc_channel *get_channel_by_name_or_id(char const *name);
void remove_channel(char const *name);
void remove_semaphore(char const *name);
void display_content(char const *name);
void display_channels();

#endif

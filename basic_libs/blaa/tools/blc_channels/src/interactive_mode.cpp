//
//  interactive_mode.cpp
//  blc_channels
//
//  Created by Arnaud Blanchard on 24/09/2015.
//
//

#include "blc.h"
#include "core.hpp"
#include "interactive_mode.hpp"

#include <ctype.h> //tolower
#include <sys/mman.h> // shm_...x
#include <errno.h> //errno
#include <unistd.h> //close
#include <fcntl.h> // O_... constants

void command_callback(char const *argument, void *pointer)
{
    int  cancel=0;
    char tmp_buffer[NAME_MAX];
    size_t size;
    char *end, action = *(char*)pointer;
    blc_channel *channel;
    char tmp_sem_abp[4], sem_abp[4]="---";
    blc_channel_info info;
    int fd;
    switch (action)
    {
        case 'a':case 'b':case 'p':case 'A':case 'B':case 'P':
            channel = get_channel_by_name_or_id(argument);
            if (channel)
            {
                if (strchr(channel->sem_abp, tolower(action)) == NULL) printf("The channel '%s' does not have '%c' semaphore.", argument,  tolower(action));
                else switch (action)
                {
                    case 'a':SYSTEM_ERROR_CHECK(sem_post(channel->sem_ack), -1, "");break;
                    case 'A':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_ack), -1, "");break;
                    case 'b':SYSTEM_ERROR_CHECK(sem_post(channel->sem_block), -1, "");break;
                    case 'B':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_block), -1, "");break;
                    case 'p':SYSTEM_ERROR_CHECK(sem_post(channel->sem_protect), -1, "");break;
                    case 'P':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_protect), -1, "");break;
                }
            }
            else printf("unknown blc_channel '%s'", argument);
            break;
        case 'c':
            if (argument[0] != '/') printf("The channel name must start with '/'");
            else do
            {
                if (blc_channel_get_info_with_name(&info, argument) !=-1)
                {
                    color_printf(BLC_RED, "The blc_channel '%s' already exist.",info.name);
                    printf("\nDo you want to recreate it ? (y/n)");
                    do {
                        fgets(tmp_buffer, NAME_MAX, stdin);
                        if (strcmp(tmp_buffer, "y\n")==0)
                        {
                            remove_channel(argument);
                            break;
                        }
                        else cancel=1;
                    }while(strcmp(tmp_buffer, "n\n") != 0);
                }
                
                fd=shm_open(argument, O_RDWR, S_IRWXU);
                if (fd != -1) {color_printf(BLC_RED, "A shared memory of name '%s' already exists.\nDo you want to remove it before creating a new blc_channel ? (y/n)", argument);
                    do {
                        fgets(tmp_buffer, NAME_MAX, stdin);
                        if (strcmp(tmp_buffer, "y\n")==0)
                        {
                            shm_unlink(argument);
                            break;
                        }
                        cancel=1;
                    }while(strcmp(tmp_buffer, "n\n") != 0);
                }
                if (cancel) break;
                
                printf("Size of blc channel '%s' ('c' to cancel)?", argument);
                fgets(tmp_buffer, NAME_MAX, stdin);
                size = strtol(tmp_buffer, &end, 10);
                if (end != tmp_buffer)
                {
                    printf("abp (ack, block, protect) ?");
                    scanf("%3[apb]", tmp_sem_abp);
                    if (strchr(tmp_sem_abp, 'a')) sem_abp[0]='a';
                    if (strchr(tmp_sem_abp, 'b')) sem_abp[1]='b';
                    if (strchr(tmp_sem_abp, 'p')) sem_abp[2]='p';
                    
                    channel = new blc_channel(argument, size, sem_abp);
                    APPEND_ITEM(&channels_addresses, &channels_nb, &channel);
                    break;
                }
            }
            while(tmp_buffer[0] != 'c');
            break;
        case 'd':display_content(argument);
            break;
        case 'l':display_channels();
            break;
        case 'q':exit(0);
            break;
        case 'u':remove_channel(argument);
            break;
        case 'U':remove_semaphore(argument);
            break;
            
    }
}

void main_interactive_mode(const char *start_filter_name)
{
    int i, fd;
    
    blc_command_add("c", command_callback, "name", "create a new blc_channel", (void*)"c");
//    blc_command_add("d", command_callback, "channel", "display the content", (void*)"d");
    blc_command_add("l", command_callback, NULL, "list the blc_channels", (void*)"l");
    blc_command_add("u", command_callback, "channel", "unlink a channel", (void*)"u");
    blc_command_add("U", command_callback, "name", "unlink a semaphore", (void*)"U");
    blc_command_add("q", command_callback, NULL, "quit", (void*)"q");
    blc_command_add("a", command_callback, "channel", "send ack (sem_post)", (void*)"a");
    blc_command_add("A", command_callback, "channel", "wait for ack (sem_wait)", (void*)"A");
    blc_command_add("b", command_callback, "channel", "unblock the channel (sem_post)", (void*)"b");
    blc_command_add("B", command_callback, "channel", "block a channel (sem_wait)", (void*)"B");
    blc_command_add("p", command_callback, "channel", "unprotect a channel (sem_post)", (void*)"p");
    blc_command_add("P", command_callback, "channel", "protect a channel (sem_wait)", (void*)"P");
    
    channels_nb=blc_channel_get_all_infos(&channels_infos, start_filter_name);

    channels_addresses=MANY_ALLOCATIONS(channels_nb, blc_channel*);
    FOR(i, channels_nb)
    {
        
        fd = shm_open(channels_infos[i].name, O_RDWR, S_IRWXU);
        
        if (fd ==-1) {
            color_printf(BLC_RED, "Openning shared memory '%s'\n%s.\n", channels_infos[i].name, strerror(errno));
            //On le supprime de la liste
            channels_infos[i] = channels_infos[channels_nb-1];
            channels_nb--;
            i--;
            continue;
        }
        else close(fd);
        channels_addresses[i] = open_blc_channel(channels_infos[i].name);
    }

    while (1)
    {
        display_channels();
        blc_command_display_help();
        blc_command_interpret();
        printf("\n");
    }
}
#include "core.hpp"
#include "interactive_mode.hpp"
#include "blc.h"
#include <sys/stat.h> // mode S_... constants
#include <sys/mman.h>
#include <unistd.h>


int main(int argc, char **argv)
{
    char const *interactive_mode=NULL, *help=NULL;
    char const *start_filter_name = "";
    
    terminal_ansi_detect();
    
    program_option_add(&interactive_mode, 'h', "help", 0, "Display this help");
    program_option_interpret(&argc, &argv);
    
    if (argc == 2) start_filter_name = argv[1];
    else if (argc > 2) EXIT_ON_ERROR("You can have only one argument, which is the optional start name filter. You have %d arguments", argc-1);
    
    if (help)
    {
        program_option_display_help();
        exit(0);
    }
    else {
        underline_printf('=', "%s", argv[0]);
        main_interactive_mode(start_filter_name);
    }
    return 0;
}


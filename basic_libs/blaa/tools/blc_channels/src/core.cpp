//
//  tools.cpp
//  blc_channels
//
//  Created by Arnaud Blanchard on 24/09/2015.
//
//
#include "core.hpp"


#include "blc.h"
#include <errno.h>
#include <sys/mman.h>

blc_channel_info *channels_infos=NULL;
int channels_info_nb=0, channels_nb=0;
blc_channel **channels_addresses=NULL;




/**
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
blc_channel *get_channel_by_name_or_id(char const *name)
{
    int i, id;
    char *end;
    
    if (name[0]=='/')
    {
        FOR(i, channels_nb) if (strcmp(channels_addresses[i]->name, name)) return channels_addresses[i];
    }
    else
    {
        id = strtol(name, &end, 10);
        if (end!=name && end[0]==0)
        {
            FOR(i, channels_nb) if (channels_addresses[i]->id == id) return channels_addresses[i];
        }else color_printf(BLC_RED, "You have to give either an id or a channel name stating by '/' instead of '%s'.\n", name);
    }
    return NULL;
}

void display_content(char const *name)
{
    blc_channel *channel;
    int dim=0;
    
    channel= get_channel_by_name_or_id(name);
    if(channel->type == STRING_TO_UINT32("FL32"))
    {
        while(dim!=channel->dims_nb)
        {
            
            //    fprintf(stdout, "%6f", );
        }
    }
    else EXIT_ON_ERROR("You cannot display %.4s", channel->type);
}

void remove_channel(char const *name)
{
    blc_channel *channel;
    char buffer[NAME_MAX+1];
    int i;
    
    channel = get_channel_by_name_or_id(name);
    if (channel == NULL)
    {
        if(name[0]=='/')
        {
            do
            {
                color_printf(BLC_RED, "'%s' is not a known channel.\nTry to unlink the shared memory file anyway (y/n).\n", name);
                fgets(buffer, NAME_MAX, stdin);
                
                if (strcmp(buffer, "y\n")==0)
                {
                    if (shm_unlink(name) == -1)
                    {
                        if (errno == ENOENT) printf("The shared memory '%s' does not exist.\n", name);
                        else printf("Unlinking '%s'.\nSystem error:%s", name, strerror(errno));
                    }
                    printf("\nSuccess unlinking shared memory '%s'\n\n", name);
                    break;
                }
            }while(strcmp(buffer, "n\n") != 0);
        }
    }
    else
    {
        FOR(i, channels_nb) if (channels_addresses[i] == channel) break;
        channels_addresses[i]->unlink();
        channels_nb--;
        channels_addresses[i] = channels_addresses[channels_nb];
        MANY_REALLOCATIONS(&channels_addresses, channels_nb);
    }
}

void remove_semaphore(char const *name)
{
    
    int ret;
    
    ret=sem_unlink(name);
    
    if (ret==-1)
    {
        if (errno==ENOENT) color_printf(BLC_RED, "The semaphore '%s' does not exist/n", name);
        else EXIT_ON_SYSTEM_ERROR("Unlinking semaphore '%s'.", name);
    }
    else color_printf(BLC_GREEN, "The semaphore '%s' has been unlinked.\n", name);
}


void display_channels()
{
    char sem_status[4]="---";
    int i, width;
    blc_channel *channel;
    
    underline_printf('-', "%6s | sem | type |format| %-32s | %-32s | %-32s ", "id", "dims", "parameter", "name");
    FOR(i, channels_nb)
    {
        channel = channels_addresses[i];
        
        strcpy(sem_status, channel->sem_abp);
        if ((sem_status[0]=='a') && (sem_is_locked(channel->sem_ack))) sem_status[0]='A';
        if ((sem_status[1]=='b') && (sem_is_locked(channel->sem_block))) sem_status[1]='B';
        if ((sem_status[2]=='p') && (sem_is_locked(channel->sem_protect))) sem_status[2]='P';
        printf("%6d | %3s | %.4s | %.4s | ", channel->id, sem_status, UINT32_TO_STRING(channel->type),  UINT32_TO_STRING(channel->format));
        width=channel->fprint_dims(stdout);
        printf("%*c", 32-width, ' ');
        printf(" | %-32s | %-32s\n", channel->parameter, channel->name);
    }
    printf("\n");
}
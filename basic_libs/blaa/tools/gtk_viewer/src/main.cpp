#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
#include <jpeglib.h>
#include <sys/mman.h>
#include <errno.h> //errno

#include "blc.h"


const char *channel_name;
GtkWidget *window;

static uchar *RGB3_from_YUYV = NULL;
pthread_t init_table_thread;
float value_min=0, value_max=1;

void* create_RGB3_from_YUYV(void *widget)
{
    int Y, Cb, Cr;
    int i, j;
    float G_tmp;
    static uchar  R[256], B;
    struct timeval time = {0,0};
    (void)widget;
    
    
    if (RGB3_from_YUYV == NULL)
    {
        RGB3_from_YUYV= MANY_ALLOCATIONS(256*256*256*3, uchar);
        
        diff_us_time(&time);
        
        FOR_INV(Y, 256)
        {
            FOR_INV(j,256) R[j]= CLIP_UCHAR(Y+1.13983*(j-128)); //It does not depend on Cb
            FOR_INV(Cb, 256)
            {
                B =  CLIP_UCHAR(Y+2.03211*(Cb-128)); // It does not depend on Cr
                G_tmp = - 0.58060*(Cb-128);
                
                FOR_INV(Cr, 256)
                {
                    i = 3 * (Y + (Cb << 8) + (Cr << 16));
                    
                    // Version Wikipedia
                    RGB3_from_YUYV[i] = R[Cr];
                    RGB3_from_YUYV[i + 1] = CLIP_UCHAR(Y-0.39465*(Cr-128) + G_tmp);
                    RGB3_from_YUYV[i + 2] = B;
                }
            }
        }
        printf("time %lfms\n", diff_us_time(&time)/1000.);
    }
    return NULL;
}

/** Gtk cannot display a black and white image, therefore we convert it before updating it*/
gboolean update_Y800_image(GtkImage *image, GdkFrameClock *frame_clock, blc_channel *channel)
{
    GdkPixbuf *pixbuf;
    int i, offset;
    uchar *pixels;
    (void) frame_clock;
    
    pixbuf = gtk_image_get_pixbuf(image);
    pixels = gdk_pixbuf_get_pixels(pixbuf);
    
    offset = (channel->size - 1) * 3;
    
    // Conversion GREY -> RGB3
    FOR_INV(i, channel->size)
    {
        memset(&pixels[offset], channel->data[i], 3);
        offset -= 3;
    }
    gtk_image_set_from_pixbuf(image, pixbuf);
    
    return G_SOURCE_CONTINUE;
}

gboolean update_RGB3_image(GtkImage *image, GdkFrameClock *frame_clock, GdkPixbuf *pixbuf)
{
    (void) frame_clock;
    gtk_image_set_from_pixbuf(image, pixbuf);
    return G_SOURCE_CONTINUE;
}

void jpeg_error(j_common_ptr cinfo, int msg_level)
{
    (void) msg_level;
    cinfo->err->num_warnings++;
}
gboolean update_JPEG_image(GtkImage *image, GdkFrameClock *frame_clock, blc_channel *channel)
{
    
    JSAMPROW row_pt[1];
    GdkPixbuf *pixbuf;
    int row_stride;
    uchar *pixels;
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;
    (void) frame_clock;
    
    pixbuf = gtk_image_get_pixbuf(image);
    pixels = gdk_pixbuf_get_pixels(pixbuf);
    
    cinfo.err = jpeg_std_error(&jerr);
    cinfo.err->emit_message = jpeg_error;
    jpeg_create_decompress(&cinfo);
    jpeg_mem_src(&cinfo, (uchar*) channel->data, channel->size);
    jpeg_read_header(&cinfo, TRUE);
    jpeg_start_decompress(&cinfo);
    row_stride = cinfo.output_width * cinfo.output_components;
    row_pt[0] = pixels;
    
    while (cinfo.output_scanline < cinfo.output_height)
    {
        jpeg_read_scanlines(&cinfo, row_pt, 1);
        row_pt[0] += row_stride;
    }
    
    if (cinfo.err->num_warnings != 0)
    {
        PRINT_WARNING("Drop image : %s", cinfo.err->jpeg_message_table[cinfo.err->last_jpeg_message]);
        cinfo.err->num_warnings = 0;
    }
    gtk_image_set_from_pixbuf(image, pixbuf);
    
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    
    return G_SOURCE_CONTINUE;
}

// 4:2:2
gboolean update_YUYV_image(GtkImage *image, GdkFrameClock *frame_clock, blc_channel *channel)
{
    GdkPixbuf *pixbuf;
    int i, j;
    uchar *data = (uchar*) channel->data;
    uchar *pixels, *tmp_pixels;
    int Y, Cb = 128, Cr = 128;
    
    (void) frame_clock;
    
    pixbuf = gtk_image_get_pixbuf(image);
    pixels = gdk_pixbuf_get_pixels(pixbuf);
    tmp_pixels = pixels;
    
    i = 0;
    while (i != (int) channel->size)
    {
        Y = data[i++];
        Cb = data[i++];
        j = Y + (Cb << 8) + (Cr << 16);
        memcpy(tmp_pixels, RGB3_from_YUYV + j * 3, 3);
        
        Y = data[i++];
        Cr = data[i++];
        j = Y + (Cb << 8) + (Cr << 16);
        memcpy(tmp_pixels + 3, RGB3_from_YUYV + j * 3, 3);
        tmp_pixels+=6;
    }
    
    gtk_image_set_from_pixbuf(image, pixbuf);
    
    return G_SOURCE_CONTINUE;
}

// 4:2:2
gboolean update_yuv2_image(GtkImage *image, GdkFrameClock *frame_clock, blc_channel *channel)
{
    GdkPixbuf *pixbuf;
    int i, j;
    uchar *data = (uchar*) channel->data;
    uchar *pixels, *tmp_pixels;
    int Y, Cb = 128, Cr = 128;
    
    (void) frame_clock;
    
    pixbuf = gtk_image_get_pixbuf(image);
    pixels = gdk_pixbuf_get_pixels(pixbuf);
    tmp_pixels = pixels;
    
    i = 0;
    while (i != (int) channel->size)
    {
        Cb = data[i++];
        Y = data[i++];
        
        j = Y + (Cb << 8) + (Cr << 16);
        memcpy(tmp_pixels, RGB3_from_YUYV + j * 3, 3);
        
        Cr = data[i++];
        Y = data[i++];
        
        j = Y + (Cb << 8) + (Cr << 16);
        memcpy(tmp_pixels + 3, RGB3_from_YUYV + j * 3, 3);
        tmp_pixels+=6;
    }
    
    gtk_image_set_from_pixbuf(image, pixbuf);
    
    return G_SOURCE_CONTINUE;
}

void getting_range(GtkRange *range, float *value)
{
    *value = gtk_range_get_value(range);
}

void getting_spin_value(GtkSpinButton *button, float *value)
{
    *value = gtk_spin_button_get_value(button);
}

void getting_check_value(GtkToggleButton *button, float *value)
{
    if (gtk_toggle_button_get_active(button)) *value = 1.0;
    else *value = 0.0;
}

gboolean update_range(GtkRange *range, GdkFrameClock *frame_clock, float *value)
{
    (void) frame_clock;
    
    gtk_range_set_value(range, *value);
    return G_SOURCE_CONTINUE;
}

gboolean update_spin_value(GtkSpinButton *button, GdkFrameClock *frame_clock, float *value)
{
    (void) frame_clock;
    
    gtk_spin_button_set_value(button, *value);
    
    return G_SOURCE_CONTINUE;
}

gboolean update_check_value(GtkToggleButton *button, GdkFrameClock *frame_clock, float *value)
{
    (void) frame_clock;
    gtk_toggle_button_set_active(button, (*value > 0.5));
    return G_SOURCE_CONTINUE;
}

GtkWidget *create_float_display(blc_channel *channel)
{
    GtkWidget *widget = NULL, *grid;
    uchar *image_buffer;
    char label_text[NAME_MAX + 1];
    GtkWidget *display;
    int steps, width, height, dim, h, i, j;
    float *values;
    
    SPRINTF(label_text, "%s %.4s", channel->name, UINT32_TO_STRING(channel->format));
    display = gtk_frame_new(label_text);
    
    if (channel->dims_nb==1)
    {
        steps=1;
        width=channel->lengths[0];
        height=1;
    }
    if (channel->dims_nb==2)
    {
        steps=1;
        width=channel->lengths[0];
        height=channel->lengths[1];
    }
    else if (channel->dims_nb==3)
    {
        steps=channel->lengths[0];
        width=channel->lengths[1];
        height=channel->lengths[2];
    };
    
    display = gtk_frame_new(channel->name);
    grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(display), grid);
    
    values=(float*)channel->data;
    
    if (channel->format==STRING_TO_UINT32("TBOX") || channel->format==STRING_TO_UINT32("NDEF"))  FOR(j, height) FOR(i, width) FOR(h, steps)
        {
            widget = gtk_spin_button_new_with_range(value_min, value_max, 0.001);
            gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_spin_value, &values[j*width*steps+i*steps+h], NULL);
            g_signal_connect(G_OBJECT(widget), "value-changed", G_CALLBACK(getting_spin_value), &values[j*width*steps+i*steps+h]);
            gtk_widget_set_vexpand(widget, TRUE);
            gtk_widget_set_hexpand(widget, TRUE);
            gtk_grid_attach(GTK_GRID(grid), widget, i+h, j, 1, 1);
    }
    else if (channel->format==STRING_TO_UINT32("VMET"))  FOR(j, height) FOR(i, width) FOR(h, steps)
        {
            widget = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, value_min, value_max, 0.001);
            gtk_range_set_inverted(GTK_RANGE(widget), TRUE);
            gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_range, &values[j*width*steps+i*steps+h], NULL);
            g_signal_connect(G_OBJECT(widget), "value-changed", G_CALLBACK(getting_range), &values[j*width*steps+i*steps+h]);
            gtk_widget_set_vexpand(widget, TRUE);
            gtk_widget_set_hexpand(widget, TRUE);
            gtk_grid_attach(GTK_GRID(grid), widget, i+h, j, 1, 1);
        }
    else if (channel->format==STRING_TO_UINT32("CBOX"))  FOR(j, height) FOR(i, width) FOR(h, steps)
    {
        widget = gtk_check_button_new();
        gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_check_value, &values[j*width*steps+i*steps+h], NULL);
        g_signal_connect(G_OBJECT(widget), "toggled", G_CALLBACK(getting_check_value), &values[j*width*steps+i*steps+h]);
        gtk_widget_set_vexpand(widget, TRUE);
        gtk_widget_set_hexpand(widget, TRUE);
        gtk_grid_attach(GTK_GRID(grid), widget, i+h, j, 1, 1);
    }
    else
    {
        snprintf(label_text, NAME_MAX, "Unknown format ('%.4s') for float values.", UINT32_TO_STRING(channel->format));
        widget = gtk_label_new(label_text);
        gtk_container_add(GTK_CONTAINER(grid), widget);
    }
    return display;
}




GtkWidget *create_byte_display(blc_channel *channel)
{
    GdkPixbuf *pixbuf;
    GtkWidget *widget = NULL;
    uchar *image_buffer;
    char label_text[NAME_MAX + 1];
    GtkWidget *display;
    int width, height;
    
    SPRINTF(label_text, "%s %.4s", channel->name, UINT32_TO_STRING(channel->format));
    display = gtk_frame_new(label_text);
    
    if (channel->format == STRING_TO_UINT32("Y800") )
    {
        width=channel->lengths[0];
        height=channel->lengths[1];
        image_buffer = MANY_ALLOCATIONS(width*height*3, uchar);
        pixbuf = gdk_pixbuf_new_from_data( image_buffer, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
        widget = gtk_image_new_from_pixbuf(pixbuf);
        gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_Y800_image, channel, NULL);
    }
    else if (channel->format == STRING_TO_UINT32("RGB3") )
    {
        width=channel->lengths[1];
        height=channel->lengths[2];
        pixbuf = gdk_pixbuf_new_from_data((uchar*)channel->data, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
        widget = gtk_image_new_from_pixbuf(pixbuf);
        gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_RGB3_image, pixbuf, NULL);
    }
    else if (channel->format == STRING_TO_UINT32("JPEG") )
    {
        width=channel->lengths[1];
        height=channel->lengths[2];
        image_buffer = MANY_ALLOCATIONS(width*height*3, uchar);
        pixbuf = gdk_pixbuf_new_from_data( image_buffer, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
        widget = gtk_image_new_from_pixbuf(pixbuf);
        gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_JPEG_image, channel, NULL);
    }
    else if (channel->format == STRING_TO_UINT32("YUYV"))
    {
        width=channel->lengths[1];
        height=channel->lengths[2];
        image_buffer = MANY_ALLOCATIONS(width*height * 3, uchar); //In order to be sure that the rowstride will be 'gui.width * 3'
        pixbuf = gdk_pixbuf_new_from_data(image_buffer, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
        widget = gtk_image_new_from_pixbuf(pixbuf);
        
        pthread_create(&init_table_thread, NULL, create_RGB3_from_YUYV, widget);
        gtk_widget_add_tick_callback(GTK_WIDGET(widget), (GtkTickCallback) update_YUYV_image, channel, NULL);
        
    }
    else if (channel->format == STRING_TO_UINT32("yuv2"))
    {
        width=channel->lengths[1];
        height=channel->lengths[2];
        image_buffer = MANY_ALLOCATIONS(width*height * 3, uchar); //In order to be sure that the rowstride will be 'gui.width * 3'
        pixbuf = gdk_pixbuf_new_from_data(image_buffer, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
        widget = gtk_image_new_from_pixbuf(pixbuf);
        
        pthread_create(&init_table_thread, NULL, create_RGB3_from_YUYV, widget);
        gtk_widget_add_tick_callback(GTK_WIDGET(widget), (GtkTickCallback) update_yuv2_image, channel, NULL);
        
        
    }
    else widget = gtk_label_new("Unknown format image");
    
    
    gtk_widget_set_size_request(GTK_WIDGET(display), 32, 32);
    gtk_container_add(GTK_CONTAINER(display), widget);
    return display;
}



void activate_cb(GApplication *app)
{
    blc_channel_info channel_info;
    blc_channel *channel;
    GtkWidget *display=NULL;
    window=gtk_application_window_new(GTK_APPLICATION(app));
    
    if (blc_channel_get_info_with_name(&channel_info, channel_name)==-1) EXIT_ON_ERROR("The channel '%s' does not exist.", channel_name);
    
    channel = new blc_channel(channel_name);
    
    if (channel->type==STRING_TO_UINT32("UIN8"))    display=create_byte_display(channel);
    else if (channel->type==STRING_TO_UINT32("FL32")) display=create_float_display(channel);
    else EXIT_ON_ERROR("Type '%.4s' not managed.", UINT32_TO_STRING(channel->type));
    
    gtk_container_add(GTK_CONTAINER(window), display);
    gtk_widget_show_all(window);
}


/** Classical GTK application.
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int main(int argc, char *argv[])
{
    GtkApplication *app;
    int status=0;
    char const *help=NULL;
    
    terminal_ansi_detect();
    program_option_add(&help, 'h', "help", 0, "display this help.");
    // program_option_add(&input, 'i', "input", 1, "channel to display.");
    program_option_interpret(&argc, &argv);
    
    
    if (help) program_option_display_help();
    else{
        
        gtk_disable_setlocale();
        gtk_init(&argc, &argv);
        
        if (argc == 2)
        {
            channel_name = argv[1];
            argc = 1; // To avoid g_application to try to interpret it.
        }
        else EXIT_ON_ERROR("You need to give a channel name.");
        
        app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
        g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
        status = g_application_run(G_APPLICATION(app), argc, argv);
        g_object_unref(app);
    }
    
    return (status);
}


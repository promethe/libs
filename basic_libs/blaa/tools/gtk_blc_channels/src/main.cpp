#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
#include <jpeglib.h>
#include <sys/mman.h>
#include <errno.h> //errno
#include <libgen.h> //dirname


#include "blc.h"


enum
{
    NAME_COLUMN, ACK_COLUMN, ACK_VALUE_COLUMN, ACK_INCONSISTENT_COLUMN, BLOCK_COLUMN,  BLOCK_VALUE_COLUMN, BLOCK_INCONSISTENT_COLUMN, PROTECT_COLUMN,  PROTECT_VALUE_COLUMN, PROTECT_INCONSISTENT_COLUMN, TYPE_COLUMN,  FORMAT_COLUMN,  DIMS_COLUMN,  POINTER_COLUMN, COLUMNS_NB
};

struct group
{
    char *name;
    GtkTreeIter iter;
};

char const *start_filter_name = "";


GtkWidget *statusbar;
GtkWidget *window, *tree;
blc_channel_info *channels_infos=NULL;
blc_channel **channels;
int channels_nb,  statusbar_context_id;

int refresh_id=0;
struct group *groups=NULL;
int groups_nb=0;

GtkToolItem *remove_tool_item, *refresh_tool_item;
GtkTreeStore *store;

char const *dir_name;



int sems_set_inconsitent(void *user_data)
{
    GtkTreeIter *iter = (GtkTreeIter*)user_data;
    
    gtk_tree_store_set(GTK_TREE_STORE(store),  iter,
                       ACK_INCONSISTENT_COLUMN, 1,
                       BLOCK_INCONSISTENT_COLUMN, 1,
                       PROTECT_INCONSISTENT_COLUMN, 1, -1);
    free(iter);
    return G_SOURCE_REMOVE;
}


int refresh_each_sem (GtkTreeModel *model,  GtkTreePath *path, GtkTreeIter *iter,   gpointer user_data)
{
    blc_channel *channel;
//    GtkTreeIter *iter_copy = ALLOCATION(GtkTreeIter);
    (void) path;
    (void)user_data;
    (void)model;

 //   *iter_copy=*iter;
    gtk_tree_model_get(model,  iter, POINTER_COLUMN, &channel, -1);

    if (channel->sem_ack) gtk_tree_store_set(GTK_TREE_STORE(store),  iter, ACK_VALUE_COLUMN, sem_is_locked(channel->sem_ack), ACK_INCONSISTENT_COLUMN, 0, -1);
    if (channel->sem_block)gtk_tree_store_set(GTK_TREE_STORE(store),  iter, BLOCK_VALUE_COLUMN, sem_is_locked(channel->sem_block), BLOCK_INCONSISTENT_COLUMN, 0, -1);
    if (channel->sem_protect)gtk_tree_store_set(GTK_TREE_STORE(store),  iter, PROTECT_VALUE_COLUMN, sem_is_locked(channel->sem_protect), PROTECT_INCONSISTENT_COLUMN, 0, -1);

 //  g_timeout_add_seconds(1, sems_set_inconsitent,iter_copy);
    return FALSE;
}

void refresh_cb(GtkWidget *widget, gpointer user_data)
{
    gtk_tree_model_foreach(GTK_TREE_MODEL(store), refresh_each_sem, user_data);
}

void ack_sem_cb (GtkCellRendererToggle *cell_renderer, gchar *path_string, gpointer user_data)
{
    blc_channel *channel;
    GtkTreeIter *iter = ALLOCATION(GtkTreeIter); //free by inconsistent
    (void)user_data;
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(store),  iter,  path_string);
    gtk_tree_model_get(GTK_TREE_MODEL(store),  iter, POINTER_COLUMN, &channel, -1);
    
    if (gtk_cell_renderer_toggle_get_active( cell_renderer)) sem_post(channel->sem_block);
    else sem_trywait(channel->sem_ack);
    
    refresh_each_sem(GTK_TREE_MODEL(store), NULL, iter, channel);

}

void block_sem_cb (GtkCellRendererToggle *cell_renderer, gchar *path_string, gpointer tree_model)
{
    blc_channel *channel;
    GtkTreeIter *iter = ALLOCATION(GtkTreeIter); //free by inconsistent
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(tree_model),  iter,  path_string);
    gtk_tree_model_get(GTK_TREE_MODEL(tree_model),  iter, POINTER_COLUMN, &channel, -1);
    
    if (gtk_cell_renderer_toggle_get_active( cell_renderer)) sem_post(channel->sem_block);
    else sem_trywait(channel->sem_block);
    
    gtk_tree_store_set(GTK_TREE_STORE(tree_model),  iter, BLOCK_VALUE_COLUMN, sem_is_locked(channel->sem_block), BLOCK_INCONSISTENT_COLUMN, 0, -1);
 //   g_timeout_add_seconds(1, set_inconsitent_block, iter);
}

void protect_sem_cb (GtkCellRendererToggle *cell_renderer, gchar *path_string, gpointer tree_model)
{
    blc_channel *channel;
    GtkTreeIter *iter = ALLOCATION(GtkTreeIter); //free by inconsistent
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(tree_model),  iter,  path_string);
    gtk_tree_model_get(GTK_TREE_MODEL(tree_model),  iter, POINTER_COLUMN, &channel, -1);
    
    if (gtk_cell_renderer_toggle_get_active( cell_renderer)) sem_post(channel->sem_protect);
    else sem_trywait(channel->sem_protect);
    
    gtk_tree_store_set(GTK_TREE_STORE(tree_model),  iter, PROTECT_VALUE_COLUMN, sem_is_locked(channel->sem_protect),PROTECT_INCONSISTENT_COLUMN,0,  -1);
 //   g_timeout_add_seconds(1, set_inconsitent_protect, iter);

}

int reload_channels(void *user_data)
{
    char tmp_dims_text[NAME_MAX];
    
    blc_channel_info *info;
    blc_channel *channel;
    int i, fd;
    int ack_value, block_value, protect_value;
    char *type;
    char *format;
    char *dims_text;
    int dims_text_size;
    GtkWidget *dialog;
    struct group *group;
    struct group tmp_group;
    char *variable_name = NULL;
    int group_name_len;
    GtkTreeIter iter, iter_parent, *parent_pt;
    (void)user_data;
    
    channels_nb=0;

    FREE(channels_infos);
    FREE(channels);
    gtk_tree_store_clear(store);

    channels_nb = blc_channel_get_all_infos(&channels_infos, start_filter_name);
    channels = MANY_ALLOCATIONS(channels_nb, blc_channel*);
    
    FOR (i, channels_nb)
    {
        info=&channels_infos[i];
        fd = shm_open(info->name, O_RDWR, S_IRWXU);
        
        if (fd ==-1) {
            dialog = (GtkWidget*)gtk_message_dialog_new (GTK_WINDOW(window), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "Trying to open shared memory '%s'.\nSystem error '%s'.", info->name,  g_strerror (errno));
            gtk_dialog_run (GTK_DIALOG (dialog));
            gtk_widget_destroy (dialog);
            //On le supprime de la liste
            channels_infos[i] = channels_infos[channels_nb-1];
            channels_nb--;
            i--;
            continue;
        }
        else close(fd);
        
        channel=new blc_channel(info->name);
        
        if (channel->sem_ack) ack_value=sem_is_locked(channel->sem_ack);
        if (channel->sem_block) block_value=sem_is_locked(channel->sem_block);
        if (channel->sem_protect) protect_value=sem_is_locked(channel->sem_protect);
        type=MANY_ALLOCATIONS(5, char);
        strncpy(type, UINT32_TO_STRING(channel->type), 4);
        type[4]='\0';
        format=MANY_ALLOCATIONS(5, char);
        strncpy(format, UINT32_TO_STRING(channel->format), 4);
        format[4]='\0';
        
        
        dims_text_size=channel->sprint_dims(tmp_dims_text, sizeof(tmp_dims_text));
        dims_text=MANY_ALLOCATIONS(dims_text_size+1, char);
        strcpy(dims_text, tmp_dims_text);
        
        
        variable_name = strchr(channel->name, '.');
        if (variable_name==NULL)
        {
            variable_name=channel->name; //There is no group
            parent_pt=NULL;
        }
        else
        {
            group_name_len=variable_name-channel->name;
            if (strncmp(channel->name, start_filter_name, group_name_len) == 0) parent_pt=NULL;
            else
            {
                FOR_EACH_INV(group, groups, groups_nb) if (strncmp(group->name, channel->name , group_name_len)==0) break;
                
                if (group==groups-1)
                {
                    tmp_group.name=MANY_ALLOCATIONS(group_name_len+1, char);
                    strncpy(tmp_group.name, channel->name, group_name_len );
                    tmp_group.name[group_name_len]='\0';
                    
                    gtk_tree_store_insert_with_values(store, &iter_parent, NULL, -1,
                                                      NAME_COLUMN, tmp_group.name, -1);
                    tmp_group.iter = iter_parent;
                    APPEND_ITEM(&groups, &groups_nb, &tmp_group);
                    group=&groups[groups_nb-1];
                }
                parent_pt=&group->iter;
            }
            variable_name++; // We start after the dot
        }
        
        gtk_tree_store_insert_with_values(store, &iter, parent_pt, -1,
                                          NAME_COLUMN, variable_name,
                                          ACK_COLUMN, channel->sem_ack, ACK_VALUE_COLUMN, ack_value,
                                          BLOCK_COLUMN, channel->sem_block, BLOCK_VALUE_COLUMN, block_value,
                                          PROTECT_COLUMN, channel->sem_protect, PROTECT_VALUE_COLUMN, protect_value,
                                          TYPE_COLUMN, type,
                                          FORMAT_COLUMN, format,
                                          DIMS_COLUMN, dims_text,
                                          POINTER_COLUMN, channel,
                                          -1);
        channels[i]=channel;
    }
    return G_SOURCE_REMOVE;
}

void unlink_channel_cb(GtkWidget *widget, gpointer user_data)
{
    (void)widget;
    blc_channel *channel = (blc_channel*)user_data;
    channel->unlink();
}

void action_channel_cb(GtkWidget *widget, gpointer user_data)
{
    char path[PATH_MAX+1];
    
    SPRINTF(path, "%s/gtk_viewer", dir_name);

    pid_t pid;
    blc_channel *channel = (blc_channel*)user_data;

    (void)widget;
    pid = fork();
    
    if (pid==0)
    {
        SYSTEM_ERROR_CHECK(execlp(path, path, channel->name, NULL), -1, "Executing '%s'", path);
    }
}

int tree_button_press_cb (GtkWidget *tree_view, GdkEventButton *event, gpointer user_data)
{
    GtkWidget *unlink_button, *action_button;
    GtkWidget *menu;
    GtkTreePath *path;
    GtkTreeIter iter;
    blc_channel *channel;
    (void)user_data;
    
    
    if (event->button==GDK_BUTTON_SECONDARY)
    {
        menu = gtk_menu_new ();
        unlink_button = gtk_menu_item_new_with_label("unlink");
        gtk_container_add(GTK_CONTAINER(menu),  unlink_button);
        gtk_container_add(GTK_CONTAINER(menu),  gtk_separator_menu_item_new());
        action_button = gtk_menu_item_new_with_label("gtk_viewer");
        gtk_container_add(GTK_CONTAINER(menu),action_button);

        gtk_widget_show_all(menu);
        
        gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, GDK_BUTTON_PRIMARY, event->time);
        gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(tree), event->x, event->y, &path, NULL, NULL, NULL);
     //   tree_model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree_view));
        gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(store),  &iter,  gtk_tree_path_to_string(path));
        gtk_tree_model_get(GTK_TREE_MODEL(store),   &iter, POINTER_COLUMN, &channel, -1);
        g_signal_connect(G_OBJECT(unlink_button), "activate", G_CALLBACK(unlink_channel_cb), channel);
        g_signal_connect(G_OBJECT(action_button), "activate", G_CALLBACK(action_channel_cb), channel);

        return true;
    }
    return false;
}

void *check_blc_channel_change(void *user_data)
{
    sem_t *sems[2], sem1 , *sem2;
    int i=0;
    char label[NAME_MAX+1];
    (void)user_data;

    SYSTEM_ERROR_CHECK(sems[0] = sem_open("blc_channels_sem1", O_CREAT | O_RDWR, S_IRWXU, 0), NULL, "Creating blc_channels_sem1");
    SYSTEM_ERROR_CHECK(sems[1] = sem_open("blc_channels_sem2", O_CREAT | O_RDWR, S_IRWXU, 0), NULL, "Creating blc_channels_sem2");

    while(1)
    {
        sem_wait(sems[i]);
        while(sem_trywait(sems[1-i]) == 0); //We are sure to be locked before continuing
        sem_post(sems[i]);
        gdk_threads_add_idle(reload_channels, NULL);
        i=1-i;
        refresh_id++;
    }
    return NULL;
}

/** Look for the blc_channels and then create the graphical interface */
static void activate(GtkApplication *app)
{
    GtkWidget *layout, *grid, *refresh_button;
   GtkWidget *toolbar;
 //   GtkBin *scrolled_window;
    char title[NAME_MAX + 1];
    GtkCellRenderer *renderer;
    pthread_t thread;
    
    window = gtk_application_window_new(app);
    snprintf(title, NAME_MAX, "blc_channels %s", start_filter_name);
    gtk_window_set_title(GTK_WINDOW(window), title);
    
    statusbar=gtk_statusbar_new();

    statusbar_context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "default");
    layout = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
    grid = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);
    
    refresh_button=GTK_WIDGET(gtk_tool_button_new(gtk_image_new_from_icon_name("view-refresh", GTK_ICON_SIZE_BUTTON), "refresh"));
    
    toolbar = gtk_toolbar_new();
    gtk_container_add(GTK_CONTAINER(toolbar), refresh_button);
    
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);
    gtk_container_add(GTK_CONTAINER(layout), toolbar);
    gtk_container_add(GTK_CONTAINER(layout), grid);
    
    store = gtk_tree_store_new (COLUMNS_NB,  G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING/*, G_TYPE_UINT64*/, G_TYPE_POINTER);
  
    /* initialized with default attributes */
/*    SYSTEM_SUCCESS_CHECK(pthread_attr_init(&pthread_attr), 0, NULL);
    SYSTEM_SUCCESS_CHECK(pthread_attr_setdetachstate(&pthread_attr,PTHREAD_CREATE_DETACHED), 0, NULL);
*/    SYSTEM_SUCCESS_CHECK(pthread_create(&thread, NULL,  check_blc_channel_change, NULL), 0, "Creating thread");
//    SYSTEM_SUCCESS_CHECK(pthread_attr_destroy(&pthread_attr), 0, NULL);

    reload_channels(NULL);
    
    tree= gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "name", renderer, "text", NAME_COLUMN, NULL);

    renderer = gtk_cell_renderer_toggle_new();
     gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "ack", renderer, "visible", ACK_COLUMN, "active", ACK_VALUE_COLUMN, "inconsistent", ACK_INCONSISTENT_COLUMN, NULL);
    g_signal_connect(G_OBJECT(renderer), "toggled", G_CALLBACK(ack_sem_cb), store);
    
    renderer = gtk_cell_renderer_toggle_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "block", renderer, "visible", BLOCK_COLUMN, "active", BLOCK_VALUE_COLUMN, "inconsistent", BLOCK_INCONSISTENT_COLUMN , NULL);
    g_signal_connect(G_OBJECT(renderer), "toggled", G_CALLBACK(block_sem_cb), store);
    
    renderer = gtk_cell_renderer_toggle_new();
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW (tree), -1, "protect", renderer, "visible", PROTECT_COLUMN, "active", PROTECT_VALUE_COLUMN, "inconsistent", PROTECT_INCONSISTENT_COLUMN, NULL);
    g_signal_connect(G_OBJECT(renderer), "toggled", G_CALLBACK(protect_sem_cb), store);

    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1,"type", renderer, "text", TYPE_COLUMN, NULL);
    
    renderer = gtk_cell_renderer_text_new ();
     gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "format", renderer, "text", FORMAT_COLUMN, NULL);
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "dims", renderer, "text", DIMS_COLUMN, NULL);
    
    g_signal_connect(G_OBJECT(tree), "button-press-event", G_CALLBACK(tree_button_press_cb), NULL);
    g_signal_connect(G_OBJECT(refresh_button), "clicked", G_CALLBACK(refresh_cb), NULL);

    
    gtk_container_add(GTK_CONTAINER(grid), tree);
    gtk_container_add(GTK_CONTAINER(layout), statusbar);
    gtk_container_add(GTK_CONTAINER(window), layout);


    gtk_widget_show_all(window);
}

/** Classical GTK application.
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int main(int argc, char **argv)
{
    GtkApplication *app;
    int status;
    
    gtk_disable_setlocale();
    gtk_init(&argc, &argv);
    
    if (argc == 2)
    {
        start_filter_name = argv[1];
        argc = 1; // To avoid g_application to try to interpret it.
    }
    dir_name=dirname(strdup(argv[0]));
    
    app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    return (status);
}


#!/bin/sh

current_dir=`pwd`
parent_dir=`dirname $current_dir`

./compile.sh $1 "Debug"
success=$?
cd $current_dir
if [ ! $success ];
then
echo "fail compiling '$1'"
exit 1
fi

bin_dir="$parent_dir/`basename $current_dir`_build/bin/"

program=`basename $1`
shift      #remove $1
echo
echo "execute:"
echo "cd $bin_dir"
echo "./$program $*"
cd $bin_dir && nemiver ./$program $*
cd $current_dir




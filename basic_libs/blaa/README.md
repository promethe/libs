Basic Libraries And Applications (blaa)
======================================


    Copyright : ETIS - ENSEA, University of Cergy-Pontoise, CNRS (2011-2015)  
    Author : Arnaud Blanchard  
    Licence :  CeCILL v2.1

Basic libraries
---------------

- blclib : Basic Library for C/C++. Very generic tools.
- blv4l2 : Basic Library for video capture on Linux. Need video for linux libv4l2.
- blQTKit : Basic Library for video on MAC. Need Quicktime libQTKit.
- ...

Basic applications
------------------

- epimethe : display interface to read or write on share memory provided by blc_channel of blclib.

Installation
============

Use `./install <project>` (blc, blc_examples, blv4l2, ...)

By default the binaries will be in `~/.local/bin` `~/.local/lib` and the includes in `~/.local/include`


Developpement
=============

You can use the same .h files and library binary for C and C++, but some functionnalities may differ (functions with structure in parameter instead of methods, variable on parent instead of heritage, ...).
In examples/ you have tiny applications (blc_write, blc_reader) using shared memory and a tool (blc_channels_info) listing shared memories availables (not on Mac).

With an IDE
-----------

If you want to edit the code, you can use:

    eclipse_create_project <project>
    xcode_project <project>

With unix Makefile
------------------
  
    cmake <project> [-DCMAKE_INSTALL_PREFIX=~/.local]
    make
    [sudo] make install

The Makefile in `../blaa_build` is not human readable but use `make help` to see the existing targets. The important settings are in CMakeCache.txt


Documentation
=============

To build and launch the documentatoin do:

    ./doc.sh <project>

The built documentation will be in `../blaa_build/<project>/html/index.html`
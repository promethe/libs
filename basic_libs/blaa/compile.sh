#!/bin/sh

install_prefix="~/.local"
current_dir=`pwd`
parent_dir=`dirname $current_dir`
project_dir="$current_dir/$1"
build_dir="$parent_dir/`basename $current_dir`_build"/$1

if [ "$#" = "2" ];then
build_type="$2"
else
build_type="Release"
fi

mkdir -p $build_dir && cd $build_dir &&
cmake $project_dir -DCMAKE_INSTALL_PREFIX=$install_prefix -DCMAKE_BUILD_TYPE=$build_type
make



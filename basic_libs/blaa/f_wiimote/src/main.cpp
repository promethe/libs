//test sur la récupération des données de Promethe

/* Modified Arnaud Blanchard
 * 16/10/206
 */

#include <stdio.h>
#include "../wiiuse-master/src/wiiuse.h"
#include "blc.h"
#include <string.h>

#define BUTTONS_NB sizeof(buttons)/sizeof(struct wiimote_button)

struct wiimote_button {
   int id;
   char const *name;
};

struct wiimote_button buttons[] =
   {
      { WIIMOTE_BUTTON_A, "A" },
      { WIIMOTE_BUTTON_B, "B" },
      { WIIMOTE_BUTTON_UP, "Up" },
      { WIIMOTE_BUTTON_DOWN, "Down" },
      { WIIMOTE_BUTTON_LEFT, "Left" },
      { WIIMOTE_BUTTON_RIGHT, "Right" },
      { WIIMOTE_BUTTON_PLUS, "Plus" }, //6
      { WIIMOTE_BUTTON_MINUS, "Minus" }, //7
      { WIIMOTE_BUTTON_HOME, "Home" }, //8
      { WIIMOTE_BUTTON_ONE, "One" },
      { WIIMOTE_BUTTON_TWO, "Two" } };

void update_button(wiimote *wiimote, blc_channel *channel, wiimote_button *button)
{
   if (IS_JUST_PRESSED(wiimote, button->id))
   {
      *(float*) channel->data = 1;
      printf("Button %s pressed\n", button->name);
   }
   if (IS_RELEASED(wiimote, button->id))
   {
      *(float*) channel->data = 0;
      printf("Button %s released\n", button->name);
   }
//   SYSTEM_ERROR_CHECK(sem_post(channel->sem_block), -1, "unblocking button %s.", button->name); Pb if  it has already been openned withouf block
}

int main()
{
   char channel_name[NAME_MAX + 1];
   blc_channel *button_channels[BUTTONS_NB];
   blc_channel *channel_rotation = NULL;
   blc_channel *channel_translation = NULL;
   int i, run;
   wiimote** wiimotes;

   wiimotes = wiiuse_init(1);
   if (!wiiuse_find(wiimotes, 1, 9000)) EXIT_ON_ERROR("No wiimote found.");

   while (!(wiiuse_connect(wiimotes, 1)))
   {
   }

   wiiuse_set_leds(wiimotes[0], WIIMOTE_LED_1);
   wiiuse_rumble(wiimotes[0], 1);
   wiiuse_rumble(wiimotes[0], 0);
   wiiuse_set_ir(wiimotes[0], 1);
   wiiuse_motion_sensing(wiimotes[0], 1);

   run = 1;

   //We allocate the channels once we know it works
   FOR(i, BUTTONS_NB)
   {
      SPRINTF(channel_name, "/wii.%s", buttons[i].name);
      button_channels[i] = create_or_open_blc_channel(channel_name, "--p", STRING_TO_UINT32("FL32"), STRING_TO_UINT32("NDEF"), NULL, 2, 1, 1);
   }
   channel_translation = create_or_open_blc_channel("/wii.translation", "--p", STRING_TO_UINT32("FL32"), STRING_TO_UINT32("NDEF"), NULL, 2, 3, 1);
   channel_rotation = create_or_open_blc_channel("/wii.rotation", "--p", STRING_TO_UINT32("FL32"), STRING_TO_UINT32("NDEF"), NULL, 2, 3, 1);

   while (run)
   {
      if (wiiuse_poll(wiimotes, 1))
      {
         FOR(i, BUTTONS_NB)
         {
            update_button(wiimotes[0], button_channels[i], &buttons[i]);
         }

         if ((IS_PRESSED(wiimotes[0], WIIMOTE_BUTTON_MINUS)) && (IS_PRESSED(wiimotes[0], WIIMOTE_BUTTON_PLUS)) && (IS_PRESSED(wiimotes[0], WIIMOTE_BUTTON_B)))
         {
            //Not elegant but simple 6, 7, 8
            *(float*) button_channels[6]->data = 0;
            *(float*) button_channels[7]->data = 0;
            *(float*) button_channels[8]->data = 0;
            printf("Wiimote stopped by minus/plus/B pressed.\n");
            run = 0;
         }

         channel_rotation->protect(1); // We check nobody use it when we write
         memcpy(channel_rotation->data, &wiimotes[0]->orient, channel_rotation->size); //roll, pitch, yaw
         channel_rotation->protect(0);
         //We signal we have updated the channel
         //     SYSTEM_ERROR_CHECK(sem_post(channel_rotation->sem_block), -1, "unblocking button %s."); Pb if  it has already been openned withouf block

         channel_translation->protect(1); // We check nobody use it when we write
         memcpy(channel_translation->data, &wiimotes[0]->gforce, channel_translation->size); //x, y, z
         channel_translation->protect(0);
         //We signal we have updated the channel
         //     SYSTEM_ERROR_CHECK(sem_post(channel_translation->sem_block), -1, "unblocking button translation."); Pb if  it has already been openned withouf block
      }

   }
   wiiuse_disconnect(wiimotes[0]);
   return 0;
}


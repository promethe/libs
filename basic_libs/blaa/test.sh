#!/bin/sh

current_dir=`pwd`
parent_dir=`dirname $current_dir`

./compile.sh $1 >&2
fail=$?
cd $current_dir

if [ $fail != 0 ];
then
echo "fail compiling '$1'"
exit 1
fi

bin_dir="$parent_dir/`basename $current_dir`_build/bin/"

program=`basename $1`
shift      #remove $1
echo
echo "execute:" >&2
echo "cd $bin_dir" >&2
echo "./$program $*" >&2
echo 
cd $bin_dir && ./$program $*
cd $current_dir




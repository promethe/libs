/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)

 Author: Aranud Blanchard

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/**
 *
 *  @date Oct 10, 2014
 *  @author Arnaud Blanchard
 @defgroup blc_channel channels
 @{
 */

#ifndef BLC_CHANNEL_H
#define BLC_CHANNEL_H

#include "blc_network.h"
#include "blc_tools.h"
#include "blc_mem.h"
#include <netinet/in.h>
#include <semaphore.h>

#define SEM_NAME_LEN 31 // At least on OSX
#define BLC_PARAMETER_MAX 31
#define BLC_DIMS_MAX 8

typedef struct blc_channel_info
{
    int id;
    char name[NAME_MAX+1];
    char sem_abp[4];
    int lengths[BLC_DIMS_MAX];
    int dims_nb;
    char parameter[BLC_PARAMETER_MAX+1];
    uint32_t type, format;    
#ifdef __cplusplus
    blc_channel_info();
    
    void fscan_info(FILE *file);
    int sprint_dims(char *string, int size);
    int fprint_dims(FILE *file);
    void fprint_info(FILE *file);
    int get_type_size();
    size_t get_minimum_size();
#endif
}blc_channel_info;

typedef struct blc_channel
#ifdef __cplusplus
:blc_mem, blc_channel_info {
    
	///Create or open a channel in shared memory.
    blc_channel(char const *name);
    ///Channel with only one dimension
    blc_channel(char const *name, int length, char const *sem_abp="---", uint32_t type=0, uint32_t format=0, char const *parameter=NULL);

    blc_channel(char const *name, char const *new_sem_abp, uint32_t type, uint32_t format, char const *new_parameter, int dims_nb, int length0, ...); // Protected, Block, Ack

    blc_channel(char const *name, char const *sem_abp, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length0, va_list arguments);

    blc_channel(char const *name, size_t size, char const *new_sem_abp, const char *hostname);
    ///Remove the channel. This free the memory of this process and close the file localy. It does not remove the shared memory file on the system.
   ~blc_channel();
    
    void vcreate(char const *sem_abp, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, va_list arguments);
    void create(char const *sem_abp, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, ...);
    void open();
    void open_or_create(char const *name);
    
    void protect(int value=1);
    void set_name(char const *name);
    void unlink(); //Remove the memory from the system unless another process retain it.
    void send();
    
#else
      { blc_mem mem;
        struct blc_channel_info info;// Not beautiful but makes it easy to convert C++ heritage of "class"  in C struct inclusion.
#endif
   int fd, socket_fd;
   sem_t  *sem_ack,  *sem_block, *sem_protect;
   uint32_t index; //type: INT8, UIN8, IN16, UI16, IN32, UI32, IN64, UI64, FL32, FL64, IMGE;
   struct sockaddr_in target;

}blc_channel;


    extern int blc_channel_port;

void start_channel_server(int port);
    
START_EXTERN_C
    
int sem_is_locked(sem_t *sem);

blc_channel *create_blc_channel(const char *name, char const *new_sem_abp, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length0, ...);

blc_channel *open_blc_channel(const char *name);
    
blc_channel *create_or_open_blc_channel(const char *name, char const *sem_abp, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length0, ...);

void destroy_blc_channel(blc_channel *channel);

/** Allocate and give the array of existing shared memory files on the system.*/
int blc_channel_get_all_infos(struct blc_channel_info **channels_infos, char const *start_name_filter);

///Retrieve the informations about a channel if it is known and return the id. Otherwise, return -1
int blc_channel_get_info_with_name(blc_channel_info *info, char const *name);
    
/** Remove the channels
 * \param channels Address of a pointer to an array containing a list of channel. This array may have been created by blc_channel_get_availables.
 * \param channels_nb number of channels in the array.*/
void destroy_channels(blc_channel ***channels, int channels_nb);

/** Detruit la memoire partagée : indispensable de pouvoir supprimer la memoire partagée 
 * dans certain contexte d'exécution
*/
int blc_channel_unlink( blc_channel * channel);

void blc_channel_protect(blc_channel *channel, int value);

END_EXTERN_C
///@}
    
#endif

/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author: Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/*
 The usage formating follows the guidelines in http://docopt.org
 */


#ifndef BLC_TEXTAPP_H
#define BLC_TEXTAPP_H

#include "blc_tools.h"

START_EXTERN_C

///Add a command in the list of possible command lines from the user.
void blc_command_add(const char *command_name, void (*callback)(char const *argument, void *user_data), const char *prompt, const char *help, void *user_data);
///Wait a input from the user (this id blocking) and interprets it depending on the blc_command list.
void blc_command_interpret();
///Like blc_command_interpret but does not block if there is nothing to read (return 0 in this case).
int blc_command_try_to_interpret();
///Start a thread listinning to the user entry without blocking.
void blc_command_interpret_tread();
///Display the list of blc_command with help.
void blc_command_display_help();


///Allocate and fill a command line with the array of argument. The array must be terminated by a NULL argument and must the returned command line me be freed after use.
///It does not handle spaces in argument !!!
char *create_command_line_from_argv(char const *const *argv);
///Allocate and fill an array of argument by spiting the command_line. The array is terminated by a NULL argument and it must be freed after used.
///It does not handle spaces in argument !!!
char * const*create_argv_from_command_line(char const *command_line);


///Set the desciption of the program used for help. A copy of desciption is done (strdup).
void program_set_description(char const *description);
/// Add parameter required or not after the program name
void program_add_parameter(char const *name, int required_nb, char const *help);
///Add a possible option to the program. The argument following the option, if any, it will be put in result otherwise "1" will be put in result.
void program_option_add( char const**result, char letter, char const *long_option, char const *parameter, const char *help);
///Interpret the program arguments and update the results values as it should.
void program_option_interpret(int *argc, char **argv[]);
///Do program_option_interpret but print the name of the program (argv[0]) underlined with '='.
void program_option_interpret_and_print_title(int *argc, char ***argv);
///Display the different possible argument for the  program.
void program_option_display_help();



END_EXTERN_C
#endif
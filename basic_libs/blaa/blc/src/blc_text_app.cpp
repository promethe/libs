/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#include "blc_text_app.h"

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <termios.h>
#include <sys/select.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h> //errno and EINT
#include <libgen.h>

#include "blc.h"

typedef struct
{
    const char *name;
    void (*callback)(char const *argument, void* user_data);
    const char *prompt;
    const char *help;
    void *user_data;
}blc_command;

// This is a bit redundant with struct option but has we need to send an array of struct option to getopt_long, we need two independant arrays: one with struct option and one with type_program_option.
typedef struct {
    char const **result;
    char letter;
    char const *name;
    char const *parameter;
    char const *help;
}type_program_option;

struct program_parameter{
    char const *name;
    int required_nb;
    char const *help;
};

char const *program_description=NULL;

blc_command *blc_commands = NULL;
int blc_commands_nb = 0;

type_program_option *program_options=NULL;
int program_options_nb=0;

struct program_parameter *program_parameters=NULL;
int program_parameters_nb;

START_EXTERN_C
void blc_command_add(const char *command_name, void (* callback)(char const *argument, void*), const char *prompt, const char *help, void *user_data)
{
    blc_command tmp_command;
    
    tmp_command.name = command_name;
    tmp_command.callback = callback;
    tmp_command.prompt = prompt;
    tmp_command.help = help;
    tmp_command.user_data = user_data;
    
    APPEND_ITEM(&blc_commands, &blc_commands_nb, &tmp_command);
}

/**Wait an input in the terminal. It match the blc_commands, if possible it executes it and return the pointer result..
 * If a command is recognise but a parameter is missing (promt not NULL), asks for more input.
 * */

void blc_command_interpret()
{
    blc_mem line, parameter;
    size_t line_capability=0;
    ssize_t tmp_size;
    const blc_command *command;
    int name_size;
    
    do
    {
        tmp_size = getline(&line.data, &line_capability, stdin);
    }while ((tmp_size == -1) && (errno == EINTR));
    
    if( tmp_size==-1) EXIT_ON_SYSTEM_ERROR("getline");
    line.size=tmp_size-1;
    line.data[line.size]=0; // Removing the last return char (\n)
    
    FOR_EACH_INV(command, blc_commands, blc_commands_nb)
    {
        name_size = strlen(command->name);
        if (strncmp(command->name, line.data, name_size) == 0)
        {
            parameter.data = line.data + name_size;
            parameter.size = line.size - name_size;
            
            if (parameter.size == 0) //No text after the command
            {
                parameter.data = NULL;
                if (command->prompt != NULL) //A text was expected
                {
                    printf("%s:\n", command->prompt);
                    parameter.size = getline(&parameter.data, &line_capability, stdin)-1;
                    parameter.data[parameter.size]=0;
                }
                command->callback(parameter.data, command->user_data);
                FREE(parameter.data);
                line_capability=0;
                break;
            }
            else
            {
                if (command->prompt == NULL) continue; // If we do not wait for parameter, the command must be exact.
                else command->callback(parameter.data, command->user_data);
                break;
            }
        }
    }
    if (command < blc_commands) printf("Unknown command in: %s\n", line.data);
}

int blc_command_try_to_interpret()
{
    fd_set rfds;
    struct timeval time_val={0,0};
    int retval;
    
    /* Surveiller stdin (fd 0) en attente d'entrées */
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    
    SYSTEM_ERROR_CHECK(retval = select(1, &rfds, NULL, NULL, &time_val), -1, NULL);
    
    if (retval) blc_command_interpret();
    else return 0;
    return 1;
}

void blc_command_display_help()
{
    const blc_command *command;
    size_t command_length_max = 0;
    size_t command_length=0;
    
    FOR_EACH(command, blc_commands, blc_commands_nb)
    {
        command_length = strlen(command->name);
        if (command->prompt) command_length+=strlen(command->prompt)+2;
        command_length_max = MAX(command_length, command_length_max);
    }
    
    FOR_EACH(command, blc_commands, blc_commands_nb)
    {
        if (command->prompt) printf("%s<%s>%*c:%s\n", command->name, command->prompt, (int)(command_length_max - strlen(command->name)-strlen(command->prompt)-1), ' ', command->help);
        else    printf("%-*s :%s\n", (int)command_length_max, command->name, command->help);
    }
}

void *blc_command_interpret_loop(void *pointer)
{
    (void)pointer;
    
    while(1)
    {
        blc_command_interpret();
    }
    
    return NULL;
}

void blc_command_interpret_tread()
{
    pthread_t thread;
    
    SYSTEM_ERROR_CHECK(pthread_create(&thread, NULL,  blc_command_interpret_loop, NULL), -1, NULL);
    
}



char *create_command_line_from_argv(char const *const *argv){
    int command_line_size, i;
    char *command_line, *pos;
    
    command_line_size=0;
    for(i=0; argv[i] != NULL; i++) command_line_size+=strlen(argv[i])+1; // + 1 for the space
    
    command_line=MANY_ALLOCATIONS(command_line_size+1, char);
    pos=command_line;
    for(i=0; argv[i] != NULL; i++)
        pos+=sprintf(pos, "%s ", argv[i]);
    return command_line;
}

char * const*create_argv_from_command_line(char const *command_line)
{
    char * const*argv=NULL;
    char *arg_pt;
    char const *pos;
    char tmp_arg[NAME_MAX+1];
    int length, argc=0;
    
    pos=command_line;
    while(sscanf(pos, SCAN_CONV(NAME_MAX, "s")"%n", tmp_arg, &length)==1)
    {
        arg_pt = strdup(tmp_arg);
        APPEND_ITEM(&argv, &argc, &arg_pt);
        pos+=length;
    }
    arg_pt=NULL;
    APPEND_ITEM(&argv, &argc, &arg_pt);
    return argv;
}


void program_set_description(char const *description)
{
    program_description=strdup(description);
}

void program_add_parameter( char const *name, int required_nb, char const *help)
{
    struct program_parameter parameter;
    
    parameter.name=name;
    parameter.required_nb=required_nb;
    parameter.help=help;
    APPEND_ITEM(&program_parameters, &program_parameters_nb, &parameter);
}

void program_option_add( char const**result, char letter, char const *long_option, char const *parameter, const char *help)
{
    type_program_option  tmp_program_option;
    
    tmp_program_option.result =  result;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = parameter;
    tmp_program_option.help = help;
    APPEND_ITEM(&program_options, &program_options_nb, &tmp_program_option);
}

void program_option_interpret(int *argc, char **argv[])
{
    char *optstring;
    int ret, option_index;
    int optstring_size=0;
    int options_nb=0;
    struct option *long_options = NULL, tmp_option;
    type_program_option *program_option;
    
    blc_program_name = basename(*argv[0]);

    optstring = MANY_ALLOCATIONS(program_options_nb*2+1, char);
    
    FOR_EACH_INV(program_option, program_options, program_options_nb)
    {
        optstring[optstring_size++]=program_option->letter;
        tmp_option.name = program_option->name;
        if (program_option->parameter) tmp_option.has_arg=required_argument;
        else tmp_option.has_arg=no_argument;
        if (tmp_option.has_arg !=0) optstring[optstring_size++]=':';
        tmp_option.flag = NULL;
        tmp_option.val=program_option->letter;
        APPEND_ITEM(&long_options, &options_nb, &tmp_option);
    }
    /* We stop the string and option long_options array by NULL values */
    optstring[optstring_size++]='\0';
    CLEAR(tmp_option);
    APPEND_ITEM(&long_options, &options_nb, &tmp_option);
    
    while ((ret = getopt_long(*argc, *argv, optstring, long_options, &option_index)) != -1)
    {
        if (ret=='?')
        {
            color_fprintf( BLC_RED, stderr, "The possibilities are:\n");
            program_option_display_help();
            EXIT_ON_ERROR("Invalid program argument");
        }
        
        FOR_EACH_INV(program_option, program_options, program_options_nb)
        {
            if (program_option->letter == ret) //TODO see what to do with special char ? et :
            {
                if (optarg == NULL) *program_option->result="1";
                else *program_option->result = optarg;                
            }
        }
    }
    
     //minus one because we keep the command name in argv[0]
    *argc -= (optind-1);
    *argv += (optind-1);
    FREE(long_options);
}

void program_option_interpret_and_print_title(int *argc, char ***argv)
{
    fprintf(stderr, "\n");
    
    program_option_interpret(argc, argv);
    underline_printf('=', blc_program_name);
    fprintf(stderr, "\n");
}
                     
void program_option_display_help()
{
    struct program_parameter *parameter;
    int i, option_length_max, tmp_length;
    type_program_option *program_option;
    
    //conventions want the help on stderr (avoid to interfere with normal output)
    fprintf(stderr, "\nUsage: %s", blc_program_name);
    option_length_max=0;
    FOR_EACH(parameter, program_parameters, program_parameters_nb)
    {
        if (parameter->required_nb==0) fprintf(stderr, " [%s]", parameter->name);
        else if (parameter->required_nb==-1) fprintf(stderr, " %s ...", parameter->name);
        else if (parameter->required_nb==1) fprintf(stderr, " %s", parameter->name);
        else FOR(i, parameter->required_nb) fprintf(stderr, " %s%d", parameter->name, i+1);
        
        option_length_max=MAX(option_length_max, (int)strlen(parameter->name));

    }
    FOR_EACH(program_option, program_options, program_options_nb)
    {
        fprintf(stderr, " [-%c", program_option->letter);
        tmp_length=3;
        if (program_option->parameter){
            fprintf(stderr, " %s", program_option->parameter);
            tmp_length+=strlen(program_option->parameter);
        }
        fprintf(stderr, "]");
        option_length_max=MAX(option_length_max, (int)strlen(program_option->name)+tmp_length);
    }
    fprintf(stderr, "\n");
    
    option_length_max+=10;
    if (program_parameters_nb) fprintf(stderr, "Arguments:\n");
    FOR_EACH(parameter, program_parameters, program_parameters_nb)
    {
        fprintf(stderr, "  %-*s:%s\n", option_length_max-2, parameter->name, parameter->help);
    }
    
    fprintf(stderr, "Options:\n");
    FOR_EACH(program_option, program_options, program_options_nb)
    {
        tmp_length=fprintf(stderr, "  -%c", program_option->letter);
        if (program_option->name) tmp_length+=fprintf(stderr, ", --%s", program_option->name);
        if (program_option->parameter) tmp_length+=fprintf(stderr, " %s", program_option->parameter);
        fprintf(stderr, "%*c", option_length_max-tmp_length, ' ');
        fprintf(stderr, ":%s\n", program_option->help);
    }
    if (program_description) fprintf(stderr, "Description:\n  %s\n", program_description);
}
END_EXTERN_C

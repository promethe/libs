/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)

 Author: A. Blanchard

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

//
//  Created by Arnaud Blanchard on 17/06/2014.
//
//

#include "blc_mem.h"
#include "blc.h"

blc_mem::blc_mem(size_t new_size)
      : data(NULL), size(0)
{
   if (new_size != 0) allocate(new_size);
}

void blc_mem::allocate(size_t new_size)
{
   if (new_size != size || data == NULL)  // Changement de taille ou l'allocation n'a encore jamais été faite
   {
      free(data);
      data = MANY_ALLOCATIONS(new_size, char);
      size = new_size;
   }
}

void blc_mem::allocate_min(size_t new_size)
{
    if (new_size > size)
    {
        free(data);
        data = MANY_ALLOCATIONS(new_size, char);
        size = new_size;
    }
}

void blc_mem::reallocate(size_t new_size)
{
   MANY_REALLOCATIONS(&data, new_size);
   size = new_size;
}

void blc_mem::replace(char const*new_data, size_t new_size)
{
   allocate(new_size);
   memcpy(data, new_data, new_size);
}

void blc_mem::append(char const*new_data, size_t new_size)
{
   size_t previous_size = size;
   reallocate(size + new_size);
   memcpy(&data[previous_size], new_data, new_size);
}

void blc_mem::reset(int value)
{
   memset(data, value, size);
}


/* C wrapper */
extern "C" {
void blc_mem_allocate(blc_mem *mem, size_t size)
{
   mem->allocate(size);
}

void blc_mem_reallocate(blc_mem *mem, size_t new_size)
{
   mem->reallocate(new_size);
}

void blc_mem_replace(blc_mem *mem, const char *data, size_t size)
{
   mem->replace(data, size);
}

void blc_mem_append(blc_mem *mem, const char *data, size_t size)
{
   mem->append(data, size);
}
}

